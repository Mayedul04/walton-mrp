//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WMRP_DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class GovernmentHolidayTable
    {
        public long Id { get; set; }
        public string GovernmentHoliday { get; set; }
        public Nullable<System.DateTime> HolidayDate { get; set; }
    }
}
