//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WMRP_DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProductionLog
    {
        public ProductionLog()
        {
            this.ProductionWastages = new HashSet<ProductionWastage>();
        }
    
        public long ID { get; set; }
        public Nullable<long> ManufactureOrderID { get; set; }
        public Nullable<long> ModelProcessID { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Remark { get; set; }
        public Nullable<long> AddedBy { get; set; }
        public Nullable<System.DateTime> AddedDate { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<int> Status { get; set; }
    
        public virtual ManufactureOrder ManufactureOrder { get; set; }
        public virtual ModelProcess ModelProcess { get; set; }
        public virtual ICollection<ProductionWastage> ProductionWastages { get; set; }
    }
}
