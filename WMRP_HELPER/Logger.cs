﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMRP.Helper
{
    public static class Logger
    {
        public static void SaveLogger(String lines, string FunctionName)
        {
            // Write the string to a file.append mode is enabled so that the log
            // lines get appended to  test.txt than wiping content and writing the log
            var Text = "Time: " + DateTime.Now + System.Environment.NewLine + " ||FunctionName: " + FunctionName + System.Environment.NewLine + lines;
            var filePath = AppDomain.CurrentDomain.BaseDirectory + "bin\\ErrorFile.txt";
            System.IO.StreamWriter file = new System.IO.StreamWriter(filePath, true);
            file.WriteLine(Text);
            file.Close();
        }
    }
}