﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMRP.Helper
{
    public class OracleDatabaseConnection
    {
        public static OracleConnection GetOldConnection()
        {
           
            const string connectionString = "Data Source=(DESCRIPTION="
                                       + "(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.100.181)(PORT=1521))"
                                       + "(CONNECT_DATA=(SERVICE_NAME=PROD)));"
                                       + "User Id=APPSRBG;Password=appsrbg;";
            var oldConnection = new OracleConnection(connectionString);
            return oldConnection;
        }
    }
}