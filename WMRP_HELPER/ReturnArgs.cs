﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMRP.Helper
{
    public class ReturnArgs
    {
        public ResponseMessage Response { get; set; }
        public string View { get; set; }
    }
}