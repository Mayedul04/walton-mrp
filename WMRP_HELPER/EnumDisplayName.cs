﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ComponentModel.DataAnnotations;

namespace WMRP.HELPER
{
   public static class EnumDisplayName
    {
       public static string GetDisplayName(Enum enumValue)
       {
           if (enumValue != null)
           {
               var temp = enumValue.GetType().GetMember(enumValue.ToString())
                         .First();
               if (temp.GetCustomAttribute<DisplayAttribute>() != null)
                   return temp.GetCustomAttribute<DisplayAttribute>().Name;
               else
                   return temp.Name;
           }
           return "";
       }
    }
}
