﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

//using Microsoft.Ajax.Utilities;
using WMRP_DAL;

namespace WMRP.Helper
{
    public class UserAuthorization:AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            try
            {
                var user = HttpContext.Current.Session["user"] as User;
                var controller = httpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
                var action = httpContext.Request.RequestContext.RouteData.Values["action"].ToString();
                if (user == null)
                {
                    if (action.ToLower() == "kpihome" && controller.ToLower() == "home") return true;
                    return false;
                }
                //return true;
                if (user.Role=="SA")
                    return true;
                if (new HttpRequestWrapper(System.Web.HttpContext.Current.Request).IsAjaxRequest())
                {
                    return true;
                }
                
                if (action.ToLower() == "login" && controller.ToLower() == "account") return true;
                if (action.ToLower() == "logoff" && controller.ToLower() == "account") return true;
                if (action.ToLower() == "authfailed" && controller.ToLower() == "account") return true;
                if (action.ToLower() == "index" && controller.ToLower() == "home") return true;
                if (action.ToLower() == "registration" && controller.ToLower() == "company") return true;
                if (action.ToLower() == "addcompany" && controller.ToLower() == "company") return true;
                if (httpContext.Request.RequestContext.HttpContext.Session == null ||
                    httpContext.Request.RequestContext.HttpContext.Session["permissions"] == null) return false;
                var filterContext = new AuthorizationContext();

                //if(filterContext.IsChildAction)
                var permissions = (List<Permission>)httpContext.Request.RequestContext.HttpContext.Session["permissions"];
                return Authorize(permissions, controller, action);
            }
            catch (Exception ex)
            {
                Logger.SaveLogger(ex.Message, "AuthorizeCore");
                throw ex;
            }
            return false;
        }

        private static bool Authorize(List<Permission> permissions, string controller, string action)
        {
            if (!permissions.Any()) return false;
            var index = permissions.FindIndex(i => i.ControllerName.ToLower() == controller.ToLower() && i.ActionName.ToLower() == action.ToLower());
            return index >= 0;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            try
            {
                var actionName = filterContext.ActionDescriptor.ActionName.ToLower();
                var controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower();
                if (actionName == "LogOff" && controllerName == "Account")
                {
                    filterContext.Result = new RedirectResult("~/Account/LogOff");
                }
                else if (actionName == "kpihome" && controllerName == "home")
                {
                    filterContext.Result = new RedirectResult("~/Home/KpiHome");
                }

                else if (actionName == "authfailed" && controllerName == "Account")
                {
                    filterContext.Result = new RedirectResult("~/Account/AuthFailed");
                }
                else if (actionName == "lostpassword" && controllerName == "login")
                {
                    filterContext.Result = new RedirectResult("~/Account/LostPassword");
                }
                else
                {
                    if (filterContext.RequestContext.HttpContext.Session == null) return;
                    var logInInfo = (User)filterContext.RequestContext.HttpContext.Session["user"];
                    //if (filterContext.HttpContext.Request.IsAjaxRequest())
                    //{
                    //    var response = new ReturnArgs { };
                    //    if (logInInfo == null)
                    //    {
                    //        response.Response = new ResponseMessage
                    //        {
                    //            MessageType = (int)MesssageType.UnAuthorized,
                    //            Message = "You are not logged in"
                    //        };
                    //        filterContext.Result = logInInfo == null ? new RedirectResult("~/Account/Login") : new RedirectResult("~/Account/AuthFailed");
                    //    }
                    //    else
                    //    {
                    //        //response.Response = new ResponseMessage
                    //        //{
                    //        //    MessageType = (int)MesssageType.UnAuthorized,
                    //        //    Message = "You don not have the permission for this operation."
                    //        //};
                    //        //filterContext.Result = new JsonResult
                    //        //{
                    //        //    Data = response,
                    //        //    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    //        //}; ;
                    //        base.HandleUnauthorizedRequest(filterContext);
                    //    }
                    //}
                    //else
                    //    filterContext.Result = logInInfo == null ? new RedirectResult("~/Account/Login") : new RedirectResult("~/Account/AuthFailed");
                    var httpContext = filterContext.HttpContext;
                    var request = httpContext.Request;
                    var response = httpContext.Response;
                    if (request.IsAjaxRequest())
                    {
                        
                        response.StatusCode = (int)HttpStatusCode.Accepted;
                        response.SuppressFormsAuthenticationRedirect = true;
                        response.End();

                        filterContext.Result = new JsonResult
                        {
                            Data = response,
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet
                        };
                        filterContext.Result = new RedirectResult(filterContext.HttpContext.Request.Url.AbsoluteUri);
                    }
                    else
                        base.HandleUnauthorizedRequest(filterContext);
                }
            }
            catch (Exception ex)
            {
                Logger.SaveLogger(ex.Message, "AuthorizeCore");
                throw ex;
            }
        }
    }
}