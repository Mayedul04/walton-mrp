﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMRP
{
   public static class  GetMaxValue
    {
       public static DateTime GetMaxDate(List<DateTime> dates)
       {
          DateTime maxdate = dates.Max<DateTime>();
          return maxdate;
       }
    }
}
