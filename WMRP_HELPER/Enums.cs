﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WMRP.Helper
{
    public enum HeaderTypes { Type1 = 1, Type2 = 2, Type3 = 3, Type4 = 4, Type5 = 5 }
    public enum MesssageType { Success = 1, Failed, Error, UnAuthorized }
    public enum OrderStatus { Drafted = 0, Submitted = 1, Checked=2, Approved=3 }
    public enum ProcessLevel { [Display(Name = "Model Level")]ML = 1,[Display(Name = "Parts Level")] PL = 2, }
    public enum PartsType { [Display(Name = "Raw Material")]RM = 1, [Display(Name = "Semi Knock-Down")] SKD = 2 }
    public enum StockType { [Display(Name = "Raw Material")]RM = 1, [Display(Name = "Work in Progress")] WIP = 2, [Display(Name = "Semi Finished Good")] SFG = 3 }
    public enum CapacityType { [Display(Name = "pcs/hr")]pcs = 1, [Display(Name = "days/order")] days = 2, }
}