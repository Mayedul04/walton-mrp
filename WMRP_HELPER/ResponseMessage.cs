﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMRP.Helper
{
    public enum MessageType { Success=1, Failed=0}
    public class ResponseMessage
    {
        public MessageType MessageType { get; set; }
        public string Message { get; set; }
        public object ReturnValue { get; set; }
        public object ReturnObject { get; set; }
        public object ReturnId { get; set; }
    }
}