﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMRP.Models.Helper_Class
{
    public class Result
    {

        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}