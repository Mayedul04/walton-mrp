﻿using System.Collections.Generic;
using WMRP_DAL;
using WMRP.Infrastructures.IRepositories;

namespace WMRP.Infrastructure.IRepositories
{
    public interface IUserRepository:IRepository<User>
    {
        List<User> GetUsersByInchargeId(string employeeId);
    }
}