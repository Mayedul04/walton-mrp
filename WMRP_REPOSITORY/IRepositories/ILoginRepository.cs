﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMRP_DAL;
using WMRP.Infrastructures.IRepositories;

namespace WMRP.Infrastructure.IRepositories
{
    public interface ILoginRepository:IRepository<User>
    {
        User GetLogin(string userName, string password);
    }
}
