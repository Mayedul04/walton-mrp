﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WMRP_DAL;
using WMRP.Infrastructures.IRepositories;

namespace WMRP.Infrastructure.IRepositories
{
    public interface IRoleRepository:IRepository<Role>
    {
    }
}