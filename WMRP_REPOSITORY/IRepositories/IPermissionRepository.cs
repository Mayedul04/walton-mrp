﻿using WMRP_DAL;
using WMRP.Infrastructures.IRepositories;

namespace WMRP.Infrastructure.IRepositories
{
    public interface IPermissionRepository:IRepository<Permission>
    {
    }
}
