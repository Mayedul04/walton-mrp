﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMRP.Infrastructures.IRepositories;
using WMRP_DAL;

namespace WMRP.IRepositories
{
    public interface IStockRepository : IRepository<BOMStock>
    {
        BOMStock GetBomStockByPartID(long id);
    }
}
