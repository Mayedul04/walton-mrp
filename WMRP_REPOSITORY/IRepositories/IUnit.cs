﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMRP.Infrastructures.IRepositories
{
    public interface IUnit
    {
        IRepository<TEntity> Repository<TEntity>() where TEntity : class;
        void Commit();
        void Dispose();
    }
}
