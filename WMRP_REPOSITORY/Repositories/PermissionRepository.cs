﻿using WMRP_DAL;
using WMRP.Infrastructure.IRepositories;
using WMRP.Infrastructures.Repositories;

namespace WMRP.Infrastructure.Repositories
{
    public class PermissionRepository:Repository<Permission>, IPermissionRepository
    {
        private readonly WMRPEntities _context;
        public PermissionRepository(WMRPEntities context)
            : base(context)
        {
            _context = context;
        }
    }
}