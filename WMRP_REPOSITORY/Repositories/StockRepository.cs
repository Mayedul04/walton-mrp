﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMRP.Infrastructures.Repositories;
using WMRP_DAL;
using WMRP.IRepositories;

namespace WMRP.Repositories
{
    public class StockRepository : Repository<BOMStock>,IStockRepository
    {
        private readonly WMRPEntities _context;

        public StockRepository(WMRPEntities context)
            : base(context)
        {
            _context = context;
        }

        public BOMStock GetBomStockByPartID(long id)
        {
            var stock = _context.BOMStocks.FirstOrDefault(x => x.PartsID == id);

            return stock;
        }
    }
}
