﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMRP.Infrastructures.IRepositories;
using WMRP_DAL;


namespace WMRP.Infrastructures.Repositories
{
    public class CellPhoneUnit : ICellPhoneUnit
    {
        private readonly CellPhoneProjectEntities _context;

        private bool _disposed;

        public CellPhoneUnit(CellPhoneProjectEntities context)
        {
            _context = context;

        }

        public ICellPhoneProjectRepository<TEntity> Repository<TEntity>() where TEntity : class
        {
            ICellPhoneProjectRepository<TEntity> repository = new CellPhoneProjectRepository<TEntity>(_context);
            return repository;
        }

        public void Commit()
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    _context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
    }
}
