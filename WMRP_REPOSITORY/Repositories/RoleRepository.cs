﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WMRP_DAL;
using WMRP.Infrastructure.IRepositories;
using WMRP.Infrastructures.Repositories;

namespace WMRP.Infrastructure.Repositories
{
    public class RoleRepository:Repository<Role>,IRoleRepository
    {
        private readonly WMRPEntities _context;

        public RoleRepository(WMRPEntities context)
            : base(context)
        {
            _context = context;
        }
    }
}