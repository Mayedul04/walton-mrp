﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WMRP_DAL;
using WMRP.Infrastructure.IRepositories;
using WMRP.Infrastructures.Repositories;

namespace WMRP.Infrastructure.Repositories
{
    public class LoginRepository:Repository<User>,ILoginRepository
    {
        private readonly WMRPEntities _context;

        public LoginRepository(WMRPEntities context)
            : base(context)
        {
            _context = context;
        }

        public User GetLogin(string userName, string password)
        {
            User user = _context.Users.FirstOrDefault(i => i.UserName == userName && i.Password == password);
            return user;
        }
    }
}