﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WMRP_DAL;
using WMRP.Infrastructure.IRepositories;
using WMRP.Infrastructures.Repositories;
using System.Data.Entity;

namespace WMRP.Infrastructure.Repositories
{
    public class UserRepository: Repository<User>,IUserRepository
    {
        private readonly WMRPEntities _context;
       
        public UserRepository(WMRPEntities context)
            : base(context)
        {
            _context = context;
        }

        public List<User> GetUsersByInchargeId(string employeeId)
        {
            var model = _context.Users.Where(x => x.InchargeEmployeeId == employeeId).ToList();
            return model;
        }
    }
}