﻿using System;
using WMRP_DAL;
using WMRP.Infrastructure.IRepositories;
using WMRP.Infrastructure.Repositories;
using WMRP.Infrastructures.IRepositories;
using WMRP.IRepositories;
using WMRP.Repositories;
using System.Data.Entity;


namespace WMRP.Infrastructures.Repositories
{
    public class Unit : IUnit 
    {
        private readonly WMRPEntities _context;
       
        private bool _disposed;

        public IUserRepository UserRepository { get; private set; }
        public IRoleRepository RoleRepository { get;private set; }
        
        public ILoginRepository LoginRepository { get; private set; }
        public IPermissionRepository PermissionRepository { get; private set; }

        public IStockRepository StockRepository { get; private set; }

        
        public Unit(WMRPEntities context)
        {
            _context = context;

            UserRepository = new UserRepository(_context);
            RoleRepository = new RoleRepository(_context);

            LoginRepository = new LoginRepository(_context);
            PermissionRepository = new PermissionRepository(_context);

            StockRepository = new StockRepository(_context);

        }

        public IRepository<TEntity> Repository<TEntity>() where TEntity : class
        {
            IRepository<TEntity> repository = new Repository<TEntity>(_context);
            return repository;
        }

        public void Commit()
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    _context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
    }
}