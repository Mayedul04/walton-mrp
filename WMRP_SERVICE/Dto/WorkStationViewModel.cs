﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMRP.Service.Dto
{
    public class WorkStationViewModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public Nullable<bool> IsMachine { get; set; }
        public Nullable<bool> Status { get; set; }
        public List<MOrderRoutingViewModel> MOrderRoutings { get; set; }
        public List<WorkStationDetailViewModel> WorkStationDetails { get; set; }
    }
}
