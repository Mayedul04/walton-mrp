﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMRP.Service.Dto
{
   public class MOrderModelViewModel
    {
        public long ID { get; set; }
        public Nullable<long> PackageID { get; set; }
        public long ProductModelID { get; set; }
        public long ProductId { get; set; }
        public long MOId { get; set; }
        public bool Selected { get; set; }

        public string ManufactureOrderName { get; set; }
        public string PackageConfigurationName { get; set; }
        public string ProductModelName { get; set; }
    }
}
