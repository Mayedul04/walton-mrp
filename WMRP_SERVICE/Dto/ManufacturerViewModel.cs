﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMRP.Service;

namespace WMRP.Service.Dto
{
   public class ManufacturerViewModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public Nullable<int> AvgShippingTime { get; set; }
        public Nullable<int> MOQ { get; set; }
        public Nullable<int> MPQ { get; set; }
        public string Comment { get; set; }
        public Nullable<long> AddedBy { get; set; }
        public Nullable<System.DateTime> AddedDate { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public string Statsus { get; set; }

        public List<ProductPartViewModel> ProductParts { get; set; }

    }
}
