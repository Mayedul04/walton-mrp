﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMRP.Service.Dto;

namespace WMRP.Service.Dto
{
  public  class ProductModelViewModel
    {
        public long ID { get; set; }
        public string ModelName { get; set; }
        public Nullable<long> ProductGroupId { get; set; }
        public Nullable<long> ProductId { get; set; }
        public string ProductName { get; set; }
        public Nullable<long> AddedBy { get; set; }
        public Nullable<System.DateTime> AddedDate { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> Status { get; set; }

        //ignored in auto mapper
        public List<ManufactureOrderViewModel> ManufactureOrders { get; set; }
        public List<PackageDetailViewModel> PackageDetails { get; set; }
        public List<ProductPartViewModel> ProductParts { get; set; }
        public List<ModelProcessViewModel> ModelProcesses { get; set; }
    }
}
