﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMRP.Service.Dto
{
   public class PackageConfigurationViewModel
    {
        public long ID { get; set; }
        public string PackageName { get; set; }
        public Nullable<long> AddedBy { get; set; }
        public Nullable<System.DateTime> AddedDate { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> Status { get; set; }

        public List<MOrderModelViewModel> MOrderModels { get; set; }
        public List<PackageDetailViewModel> PackageDetails { get; set; }
    }
}
