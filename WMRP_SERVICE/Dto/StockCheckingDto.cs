﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMRP.Service.Dto
{
   public class StockCheckingDto
    {
        public long ProductPartId { get; set; }
        public long CurrentStock { get; set; }
        public bool Available { get; set; }
    }
}
