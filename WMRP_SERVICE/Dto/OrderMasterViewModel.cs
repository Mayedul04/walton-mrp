﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMRP.Service.Dto
{
  public  class OrderMasterViewModel
    {
        public long ID { get; set; }
        public string Title { get; set; }
        public string ReponsiblePerson { get; set; }
        public Nullable<long> RequisitionInitiatedBy { get; set; }
        public string Description { get; set; }
        public string OrderNumber { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<long> AddedBy { get; set; }
        public Nullable<System.DateTime> AddedDate { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }

        public List<OrderDetailViewModel> OrderDetails { get; set; }
    }
}
