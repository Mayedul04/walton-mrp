﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WMRP.Service.Dto
{
    public class MOrderBOMViewModel
    {
        public long ID { get; set; }
        public long MOId { get; set; }
        public long PartsID { get; set; }
        public string PartsName { get; set; }
        public long? ParentID { get; set; }
        public string ParentName { get; set; }
        public string PartsCode { get; set; }
        public string Dimension { get; set; }
        public int RequiredQty { get; set; }
        public Nullable<int> DraftedQty { get; set; }
        public Nullable<int> SubmittedQty { get; set; }
        public Nullable<long> ManfacturerId { get; set; }
        public string ManufacturerName { get; set; }
        public Nullable<System.DateTime> ExpectedProductionStartDate { get; set; }
        public string ManufactureOrderTitle { get; set; }
        public long StockAvailability { get; set; }
        public bool Available { get; set; }
        public string ExpectedProductionStartDateText { get; set; }

    }
}
