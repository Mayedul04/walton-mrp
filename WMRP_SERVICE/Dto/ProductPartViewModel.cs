﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMRP_DAL;

namespace WMRP.Service.Dto
{
    public class ProductPartViewModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string ChineseName { get; set; }
        public string YwxPartNo { get; set; }
        public string ManufacturePartNo { get; set; }
        public Nullable<int> PartsType { get; set; }
        public string PartsTypeName { get; set; }
        public Nullable<long> ParentID { get; set; }
        public string PartsParentName { get; set; }
        public Nullable<long> ProductModelID { get; set; }
        public string ProductModelName { get; set; }
        public Nullable<int> RequiredQty { get; set; }
        public string Description { get; set; }
        public string Reference { get; set; }
        public Nullable<long> ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }

        public Nullable<long> MOQ { get; set; }
        public Nullable<long> MPQ { get; set; }

        public Nullable<long> AddedBy { get; set; }
        public Nullable<System.DateTime> AddedDate { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> Status { get; set; }
        public List<BOMStockViewModel> BOMStocks { get; set; }
        public List<MOrderBOMViewModel> MOrderBOMs { get; set; }
        public List<ModelProcessViewModel> ModelProcesses { get; set; }
        public List<ProductPartViewModel> ProductParts1 { get; set; }

        public long? InventoryItemId { get; set; }
        public string UOM { get; set; }
        
    }
}
