﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMRP.Service.Dto
{
    public class ManufactureOrderViewModel
    {
        public long ID { get; set; } 
        public string Title { get; set; }
        public string BDColor { get; set; }
        public Nullable<long> ManufactureType { get; set; }
        public string ManufactureTypeName { get; set; }
        public Nullable<long> PackageID { get; set; }
        public string PackageName { get; set; }
        public Nullable<long> ProductModelID { get; set; }
        public string ProductModelName { get; set; }

        public Nullable<long> ProductID { get; set; }
        public long ExpectedProductionVolume { get; set; }

        public string ExpectedStartDateText { get; set; }
        public string ExpectedEndDateText { get; set; }
        public Nullable<long> AddedBy { get; set; }
        public Nullable<System.DateTime> AddedDate { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public System.DateTime CalculatedStartDate { get; set; }
        public string CalculatedStartDateText { get; set; }
        public System.DateTime CalculatedCompletionDate { get; set; }
        public string CalculatedCompletionDateText { get; set; }


        public System.DateTime? ExpectedStartDate { get; set; }
        public System.DateTime? ExpectedEndDate { get; set; }


        public Nullable<long> DraftedBy { get; set; }
        public Nullable<System.DateTime> DraftedDate { get; set; }
        public Nullable<long> SubmittedBy { get; set; }
        public Nullable<System.DateTime> SubmittedDate { get; set; }
        public Nullable<long> CheckedBy { get; set; }
        public Nullable<System.DateTime> CheckedDate { get; set; }
        public Nullable<long> ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApproveDate { get; set; }

        public string ColorCode { get; set; }

        public List<MOrderBOMViewModel> MOrderBOMs { get; set; }
        public List<MOrderModelViewModel> MOrderModels { get; set; }
        public List<MOrderRoutingViewModel> MOrderRoutings { get; set; }
        public List<ProductionLogViewModel> ProductionLogs { get; set; }


    }
}
