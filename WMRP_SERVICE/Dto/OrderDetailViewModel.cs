﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMRP.Service.Dto
{
   public class OrderDetailViewModel
    {
        public long ID { get; set; }
        public string ItemName { get; set; }
        public Nullable<long> OrderMasterID { get; set; }
        public string SupplierName { get; set; }
        public string Quantity { get; set; }
        public Nullable<long> ComponentID { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
        public Nullable<System.DateTime> InspectionDate { get; set; }
        public Nullable<System.DateTime> WareHouseReceiveDate { get; set; }
        public Nullable<System.DateTime> Comments { get; set; }
        public Nullable<long> AddedBy { get; set; }
        public Nullable<System.DateTime> AddedDate { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<int> Status { get; set; }

        public string OrderMaster { get; set; }
    }
}
