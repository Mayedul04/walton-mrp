﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMRP.Service.Dto
{
    public class MOrderRoutingViewModel
    {
        public long ID { get; set; }
        public long MOId { get; set; }
        public long ProcessId { get; set; }
        public string ProcessName { get; set; }
        public long ProcessType { get; set; }
        public Nullable<double> ExpectedFinishingTime { get; set; }
        public Nullable<long> WorkStationId { get; set; }
        public string WorkStationName { get; set; }
        public int Capacity { get; set; }
        public Nullable<int> CapacityType { get; set; }
        public string CapacityTypeName { get; set; }
        public int Sequence { get; set; }
        public Nullable<double> ThresholdTime { get; set; }
        public Nullable<int> NoOfChangeOver { get; set; }
        public int OldModelChangeOvertime { get; set; }
        public int NewModelChangeOvertime { get; set; }
        public Nullable<System.DateTime> ExpectedStartDate { get; set; }
        public Nullable<System.DateTime> ExpectedEndDate { get; set; }
        public string ExpectedStartDateText { get; set; }
        public string ExpectedEndDateText { get; set; }
        public string ManufactureOrderTitle { get; set; }
        public bool Selected { get; set; }
    }
}
