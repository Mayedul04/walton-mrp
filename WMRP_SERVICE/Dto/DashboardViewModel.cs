﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMRP.Service.Dto
{
    public class DashboardViewModel
    {
        public long? DraftCount { get; set; }
        public long? SubmitCount { get; set; }
        public long? CheckeCount { get; set; }
        public long? ApprovedCount { get; set; }
        public string WorkStationName { get; set; }
        public long WorkStationId { get; set; }
        public long WorkStationTotalCapacity { get; set; }

        public long PartsId { get; set; }

        public long Skd { get; set; }
        public long Ckd { get; set; }
        public string ModelName { get; set; }
        public int? PartsType { get; set; }
        public long? StockAmount { get; set; }
        public DateTime? AddedDate { get; set; }
        public string FormatedAddedDate { get; set; }

    }
}
