﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMRP.Service.Dto
{
   public class BOMStockViewModel
    {
        public long ID { get; set; }
        public Nullable<long> PartsID { get; set; }
        public Nullable<int> StockType { get; set; }
        public Nullable<long> CurrentStock { get; set; }
        public string ProductPartName { get; set; }
        
    }
}
