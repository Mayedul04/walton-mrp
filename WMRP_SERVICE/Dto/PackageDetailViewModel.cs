﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMRP.Service.Dto
{
   public class PackageDetailViewModel
    {
        public long ID { get; set; }
        public Nullable<long> PackageId { get; set; }
        public Nullable<long> ProductModelID { get; set; }
        public Nullable<long> ProductID { get; set; }
        public Nullable<int> Quantity { get; set; }
        public List<ProductModelViewModel> Models { get; set; }
        public string PackageConfigurationName { get; set; }
        public string ProductModelName { get; set; }
        public string ProductName { get; set; }
    }
}
