﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMRP.Service.Dto
{
   public class ProductionWastageViewModel
    {
        public long ID { get; set; }
        public Nullable<long> ProductionLogID { get; set; }
        public string ProductionLogName { get; set; }
        public Nullable<long> PartsID { get; set; }
        public Nullable<long> WastageAmount { get; set; }
        public Nullable<double> WastagePercentage { get; set; }

    }
}
