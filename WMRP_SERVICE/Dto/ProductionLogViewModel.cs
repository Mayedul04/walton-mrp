﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMRP.Service.Dto
{
   public class ProductionLogViewModel
    {
        public long ID { get; set; }
        public Nullable<long> ManufactureOrderID { get; set; }
        public Nullable<long> ProcessID { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Remark { get; set; }
        public Nullable<long> AddedBy { get; set; }
        public Nullable<System.DateTime> AddedDate { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<int> Status { get; set; }

        public string ManufactureOrderName { get; set; }
        public string ProcessName { get; set; }
        public List<ProductionWastageViewModel> ProductionWastages { get; set; }
    }
}
