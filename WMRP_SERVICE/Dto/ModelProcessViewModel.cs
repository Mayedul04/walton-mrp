﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMRP.Service.Dto
{
    public class ModelProcessViewModel
    {
        public long ID { get; set; }
        public Nullable<long> ProcessID { get; set; }
        public string ProcessName { get; set; }
        public Nullable<long> ProductModelID { get; set; }
        public string ProductModelName { get; set; }
        public Nullable<long> PartsID { get; set; }
        public string ProductPartName { get; set; }
        public Nullable<int> Sequence { get; set; }
        public Nullable<bool> IsMainProcess { get; set; }
        public Nullable<int> LeadTime { get; set; }
        
        public Nullable<long> AddedBy { get; set; }
        public Nullable<System.DateTime> AddedDate { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> Status { get; set; }


        public List<ProductionLogViewModel> ProductionLogs { get; set; }
        public List<WorkStationDetailViewModel> WorkStationDetails { get; set; }
    }
}
