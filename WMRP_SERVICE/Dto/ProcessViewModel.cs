﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace WMRP.Service.Dto
{
  public class ProcessViewModel
    {

        public long ID { get; set; }
        public string Name { get; set; }
        public Nullable<int> ProcessLevel { get; set; }
        public string ProcessLevelName { get; set; }
        public Nullable<bool> Status { get; set; }

        public List<ModelProcessViewModel> ModelProcesses { get; set; }
        
    }
}
