﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMRP.Service
{
    public class CommercialReportViewModel
    {
        public long ID { get; set; }
        public string Name { get; set; }

        public Nullable<long> ProductModelID { get; set; }
        public string ProductModelName { get; set; }

        public Nullable<long> ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }

        public int PurchaseAmount { get; set; }
    }
}
