﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMRP.Service.Dto
{
  public  class BOMStockLogViewModel
    {
        public long ID { get; set; }
        public Nullable<long> StockID { get; set; }
        public Nullable<long> Amount { get; set; }
        public Nullable<long> AddedBy { get; set; }
        public Nullable<System.DateTime> AddedDate { get; set; }
        public string BOMStockTitle { get; set; }
    }
}
