﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WMRP.Service;

namespace WMRP.Service.Dto
{
    public class ProductViewModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public Nullable<long> GroupId { get; set; }
        public string ProductGroupName { get; set; }
        public Nullable<bool> IsMainProductOfGroup { get; set; }
        public Nullable<int> LeadTimeWithMain { get; set; }
        public Nullable<long> AddedBy { get; set; }
        public Nullable<System.DateTime> AddedDate { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> Status { get; set; }
        public  List<ProductModelViewModel> ProductModels { get; set; }
    }
}