﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMRP.Service.Dto
{
  public  class EnumDto
    {
      public int ID { get; set; }
      public string Name { get; set; }
    }
}
