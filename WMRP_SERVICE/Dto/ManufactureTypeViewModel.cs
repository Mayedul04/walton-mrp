﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMRP.Service.Dto
{
  public  class ManufactureTypeViewModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public List<ManufactureOrderViewModel> ManufactureOrders { get; set; }
    }
}
