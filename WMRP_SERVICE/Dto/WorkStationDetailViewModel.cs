﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMRP_DAL;

namespace WMRP.Service.Dto
{
  public  class WorkStationDetailViewModel
    {
        public long ID { get; set; }
        public long WorkStationID { get; set; }
        public string WorkSatationName { get; set; }
        public Nullable<int> OldModelChangeOvertime { get; set; }
        public Nullable<int> NewModelChangeOvertime { get; set; }
        public Nullable<int> CapacityPerHour { get; set; }
        public Nullable<long> ModelProcessID { get; set; }
        public string ModelProcessName { get; set; }
        public Nullable<long> AddedBy { get; set; }
        public Nullable<System.DateTime> AddedDate { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> Status { get; set; }

        public  string WorkSatationCapacityType { get; set; }
       
    }
}
