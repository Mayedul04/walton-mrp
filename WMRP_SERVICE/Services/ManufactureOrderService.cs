using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using WMRP.Infrastructures.Repositories;
using WMRP.Service.Dto;
using WMRP_DAL;
using AutoMapper;
using WMRP.Service.IServices;
using WMRP.Helper;
using WMRP;
using WMRP.HELPER;
using System.Globalization;
using Oracle.ManagedDataAccess.Client;
using System.Data;


namespace WMRP.Service.Services
{
    public class ManufactureOrderService : IManufactureOrderService
    {
        private readonly WMRPEntities _context;
        private readonly CellPhoneProjectEntities _ccontext;
        private readonly Unit _unit;
        private readonly CellPhoneUnit _cunit;


        public ManufactureOrderService()
        {
            _context = new WMRPEntities();
            _unit = new Unit(_context);
            _ccontext = new CellPhoneProjectEntities();
            _cunit = new CellPhoneUnit(_ccontext);

        }
        //private 

        public ManufactureOrderViewModel GetManufactureOrder(long? id)
        {
            var data = _unit.Repository<ManufactureOrder>().Get((long)id);
            return Mapper.Map<ManufactureOrderViewModel>(data);
        }
        public List<ManufactureOrderViewModel> GetAllOrders()
        {
            List<ManufactureOrderViewModel> orderList = new List<ManufactureOrderViewModel>();
            var data = _unit.Repository<ManufactureOrder>().GetAll();
            orderList = Mapper.Map<List<ManufactureOrderViewModel>>(data);
            return orderList;
        }

        public List<DashboardViewModel> GetWorkstationCapacity()
        {
            List<DashboardViewModel> data = (from wd in _unit.Repository<WorkStationDetail>().GetAll().Where(x => x.WorkSatation.CapacityType == 1).OrderBy(y => y.ModelProcess.ProcessID)

                                             select new
                                             {
                                                 wd.WorkSatation.Name,
                                                 wd.CapacityPerHour,
                                                 wd.WorkStationID

                                             }
                                                 into temp
                                                 group temp by temp.WorkStationID
                                                     into g
                                                     let obj = g.FirstOrDefault()
                                                     where obj != null
                                                     select new DashboardViewModel
                                                     {
                                                         WorkStationId = g.Key,
                                                         WorkStationName = obj.Name,
                                                         WorkStationTotalCapacity = (long)g.Average(i => i.CapacityPerHour),
                                                     }).ToList();


            return data;
        }

        public List<DashboardViewModel> GetModelWisePartsType()
        {

            List<DashboardViewModel> data = (from productPart in _unit.Repository<ProductPart>().GetAll()
                                             join productModel in _unit.Repository<ProductModel>().GetAll() on productPart.ProductModelID equals productModel.ID

                                             select new
                                             {
                                                 productModel.ModelName,
                                                 productPart.PartsType
                                             }
                                                 into temp
                                                 group temp by temp.ModelName
                                                     into g
                                                     let obj = g.FirstOrDefault()
                                                     where obj != null
                                                     select new DashboardViewModel
                                                     {
                                                         ModelName = g.Key,
                                                         Ckd = g.Count(i => i.PartsType == 1),
                                                         Skd = g.Count(i => i.PartsType == null)
                                                     }).ToList();

            return data;
        }

        public List<DashboardViewModel> GetAllModelBomStock()
        {
            List<DashboardViewModel> data = (from bomStockLog in _unit.Repository<BOMStockLog>().GetAll()
                                             join productPart in _unit.Repository<ProductPart>().GetAll() on bomStockLog.PartsID equals productPart.ID

                                             select new
                                             {
                                                 productPart.ProductModel.ModelName,
                                                 bomStockLog.Amount,
                                                 bomStockLog.AddedDate
                                             }
                                                 into temp
                                                 group temp by new { temp.ModelName, temp.AddedDate }
                                                     into g
                                                     let obj = g.FirstOrDefault()
                                                     where obj != null
                                                     select new DashboardViewModel
                                                     {
                                                         ModelName = g.Key.ModelName,
                                                         //AddedDate = g.Key.AddedDate,
                                                         FormatedAddedDate = DateTime.Parse(g.Key.AddedDate.ToString()).ToShortDateString(),
                                                         StockAmount = g.Sum(i => i.Amount)

                                                     }).ToList();
            return data;
        }

        public DashboardViewModel GetAllTypeOfStatus()
        {
            var orderList = GetAllOrders();

            //var draftList = new List<long>();
            //var submitList = new List<long>();
            //var checkList = new List<long>();
            //var approveList = new List<long>();

            var draftCount = 0;
            var submitCount = 0;
            var checkListCount = 0;
            var approveCount = 0;

            foreach (var item in orderList)
            {

                if (item.DraftedBy != null)
                {
                    draftCount++;
                }
                else if (item.SubmittedBy != null)
                {
                    submitCount++;
                }
                else if (item.CheckedBy != null)
                {
                    checkListCount++;
                }
                else if (item.ApprovedBy != null)
                {
                    approveCount++;
                }

            }
            DashboardViewModel viewModel = new DashboardViewModel();

            viewModel.DraftCount = draftCount;
            viewModel.SubmitCount = submitCount;
            viewModel.CheckeCount = checkListCount;
            viewModel.ApprovedCount = approveCount;

            //var statusList = new List<long>();

            //statusList.Add(draftCount);
            //statusList.Add(submitCount);
            //statusList.Add(checkListCount);
            //statusList.Add(approveCount);

            return viewModel;
        }
        //public List<ManufactureOrderViewModel> GetAllOrders(int status)

        public List<ManufactureOrderViewModel> GetAllOrders(int? status)
        {
            var data = new List<ManufactureOrder>();
            List<ManufactureOrderViewModel> orderList = new List<ManufactureOrderViewModel>();
            if (status != null)
                data = _unit.Repository<ManufactureOrder>().GetAll().Where(x => x.Status == status).ToList();
            else
                data = _unit.Repository<ManufactureOrder>().GetAll().ToList();
            //var productmodels = _unit.Repository<ProductModel>().GetAll();
            orderList = Mapper.Map<List<ManufactureOrderViewModel>>(data);
            //orderList = (from or in data
            //             //join pr in productmodels
            //             //on or.ProductModelID equals pr.ID

            //             select new ManufactureOrderViewModel
            //             {
            //                 ID=or.ID,
            //                 Title=or.Title,
            //                 //ProductModelID=or.ProductModelID,
            //                 //ProductModelName=pr.ModelName,
            //                 ExpectedProductionVolume=(long)or.ExpectedProductionVolume,
            //                 ExpectedStartDateText= DateTime.Parse(or.ExpectedStartDate.ToString()).ToShortDateString(),
            //                 ExpectedEndDateText = DateTime.Parse(or.ExpectedEndDate.ToString()).ToShortDateString(),
            //                 CalculatedCompletionDateText = DateTime.Parse(or.CalculatedCompletionDate.ToString()).ToShortDateString(),
            //                 StatusName = EnumDisplayName.GetDisplayName((OrderStatus)or.Status)
            //             }).ToList();

            return orderList;

        }
        public ResponseMessage AddMOrder(ManufactureOrderViewModel viewmodel)
        {
            var response = new ResponseMessage();
            //var user = HttpContext.Current.Session["user"] as User;
            var entity = new ManufactureOrder();
            var productboms = new List<MOrderBOM>();
            var routings = new List<MOrderRouting>();
            var momodels = new List<MOrderModel>();

            var bom = new MOrderBOM();
            var rout = new MOrderRouting();
            var momodel = new MOrderModel();

            if (viewmodel.MOrderBOMs != null && viewmodel.MOrderBOMs.Count > 0)
            {
                foreach (var item in viewmodel.MOrderBOMs)
                {
                    if (item.ID > 0 && item.ID != null)
                    {
                        bom = new MOrderBOM();
                        bom = _unit.Repository<MOrderBOM>().Get(item.ID);

                        if (viewmodel.Status == (int)OrderStatus.Drafted)
                        {
                            item.DraftedQty = (int)(item.RequiredQty);
                            item.SubmittedQty = 0;
                        }

                        else
                        {
                            item.DraftedQty = 0;
                            item.SubmittedQty = (int)(item.RequiredQty);
                        }

                        Mapper.Map(item, bom);
                        DateTime tempDate = DateTime.Today;
                        DateTime.TryParseExact(item.ExpectedProductionStartDateText, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out tempDate);
                        bom.ExpectedProductionStartDate = tempDate;

                        _unit.Repository<MOrderBOM>().Update(bom);
                        productboms.Add(bom);
                    }
                    else
                    {
                        bom = Mapper.Map<MOrderBOM>(item);
                        if (viewmodel.Status == (int)OrderStatus.Drafted)
                        {
                            item.DraftedQty = (int)(item.RequiredQty * viewmodel.ExpectedProductionVolume);
                            item.SubmittedQty = 0;
                        }

                        else
                        {
                            item.DraftedQty = 0;
                            item.SubmittedQty = (int)(item.RequiredQty * viewmodel.ExpectedProductionVolume);
                        }
                        DateTime tempDate = DateTime.Today;
                        DateTime.TryParseExact(item.ExpectedProductionStartDateText, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out tempDate);
                        bom.ExpectedProductionStartDate = tempDate;
                        _unit.Repository<MOrderBOM>().Add(bom);
                        productboms.Add(bom);
                    }

                }
            }


            if (viewmodel.MOrderRoutings != null && viewmodel.MOrderRoutings.Count > 0)
            {
                foreach (var item in viewmodel.MOrderRoutings)
                {
                    DateTime startDate = DateTime.Today;
                    DateTime.TryParseExact(item.ExpectedStartDateText, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate);
                    item.ExpectedStartDate = startDate;
                    DateTime endDate = DateTime.Today;
                    DateTime.TryParseExact(item.ExpectedEndDateText, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out endDate);
                    item.ExpectedEndDate = endDate;
                    if (item.ID > 0 && item.ID != null)
                    {
                        rout = new MOrderRouting();
                        rout = _unit.Repository<MOrderRouting>().Get(item.ID);
                        Mapper.Map(item, rout);
                        _unit.Repository<MOrderRouting>().Update(rout);
                        routings.Add(rout);
                    }
                    else
                    {
                        rout = Mapper.Map<MOrderRouting>(item);
                        _unit.Repository<MOrderRouting>().Add(rout);
                        routings.Add(rout);
                    }

                }
            }

            if (viewmodel.MOrderModels != null && viewmodel.MOrderModels.Count > 0)
            {
                foreach (var item in viewmodel.MOrderModels)
                {
                    if (item.ID > 0 && item.ID != null)
                    {
                        momodel = new MOrderModel();
                        momodel = _unit.Repository<MOrderModel>().Get(item.ID);
                        Mapper.Map(item, momodel);
                        _unit.Repository<MOrderModel>().Update(momodel);
                        momodels.Add(momodel);
                    }
                    else
                    {
                        momodel = Mapper.Map<MOrderModel>(item);
                        _unit.Repository<MOrderModel>().Add(momodel);
                        momodels.Add(momodel);
                    }

                }
            }

            try
            {
                DateTime tempDate = DateTime.Today;
                DateTime.TryParseExact(viewmodel.ExpectedStartDateText, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out tempDate);
                viewmodel.ExpectedStartDate = tempDate;
                DateTime endDate = DateTime.Today;
                DateTime.TryParseExact(viewmodel.ExpectedEndDateText, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out endDate);
                viewmodel.ExpectedEndDate = endDate;

                DateTime tempCompletionDate = DateTime.Today;
                DateTime.TryParseExact(viewmodel.CalculatedCompletionDateText, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out tempCompletionDate);
                viewmodel.CalculatedCompletionDate = tempCompletionDate;

                DateTime tempCalculatedStartDate = DateTime.Today;
                DateTime.TryParseExact(viewmodel.CalculatedStartDateText, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out tempCalculatedStartDate);
                viewmodel.CalculatedStartDate = tempCalculatedStartDate;

                if (viewmodel.ID != null && viewmodel.ID > 0)
                {
                    entity = _unit.Repository<ManufactureOrder>().Get(viewmodel.ID);
                    Mapper.Map(viewmodel, entity);

                    if (viewmodel.Status == (int)OrderStatus.Approved)
                    {
                        entity.ApprovedBy = 1;
                        entity.ApproveDate = DateTime.Now;

                    }
                    else if (viewmodel.Status == (int)OrderStatus.Submitted)
                    {
                        entity.SubmittedBy = 1;
                        entity.SubmittedDate = DateTime.Now;
                    }
                    else if (viewmodel.Status == (int)OrderStatus.Checked)
                    {
                        entity.CheckedBy = 1;
                        entity.CheckedDate = DateTime.Now;
                    }
                    else
                    {
                        entity.DraftedBy = 1;
                        entity.DraftedDate = DateTime.Now;
                    }
                    entity.MOrderModels = momodels;
                    entity.MOrderBOMs = productboms;
                    entity.MOrderRoutings = routings;
                    _unit.Repository<ManufactureOrder>().Update(entity);
                    _unit.Commit();
                    response.ReturnId = entity.ID;
                    response.Message = "Manufacture Order Have been Updated Successfully";
                    response.MessageType = MessageType.Success;
                    return response;
                }

                else
                {
                    entity = Mapper.Map<ManufactureOrder>(viewmodel);
                    entity.AddedBy = 1;
                    entity.AddedDate = DateTime.Now;

                    entity.DraftedBy = 1;
                    entity.DraftedDate = DateTime.Now;
                    entity.MOrderBOMs = productboms;
                    entity.MOrderRoutings = routings;
                    entity.MOrderModels = momodels;
                    _unit.Repository<ManufactureOrder>().Add(entity);
                    _unit.Commit();
                    response.ReturnId = entity.ID;
                    response.Message = "Manufacture Order Have been Added Successfully";
                    response.MessageType = MessageType.Success;
                    return response;
                }
            }
            catch (Exception ex)
            {
                if (viewmodel.ID > 0)
                    response.ReturnId = viewmodel.ID;
                else
                    response.ReturnId = 0;
                response.Message = "Failed" + ex.Message;
                response.MessageType = MessageType.Failed;
                return response;
            }
        }

        public DateTime GetNextAvailableDateAfterHoliday(List<DateTime> holidays, DateTime datetocheck)
        {
            DateTime availdate = datetocheck;
            if (holidays.Contains(datetocheck))
            {
                availdate = datetocheck.AddDays(+1);
                GetNextAvailableDateAfterHoliday(holidays, availdate);
            }
            return availdate;
        }
        public List<MOrderRoutingViewModel> GetProductRoutings(long mid, DateTime sdate)
        {
            var holidays = _cunit.Repository<GovernmentHolidayTable>().GetAll().Select(x => (DateTime)x.HolidayDate).ToList();
            var processlist = _unit.Repository<ModelProcess>().GetAll().Where(x => x.ProductModelID == mid).OrderBy(x => x.Sequence).ToList();
            var warkstations = _unit.Repository<WorkStationDetail>().GetAll().Where(x => x.Status == true).ToList();
            var data = (from pr in processlist
                        join wr in warkstations
                        on pr.ID equals wr.ModelProcessID
                        select new MOrderRoutingViewModel
                        {
                            ProcessId = pr.ID,
                            ProcessName = pr.Process.Name,
                            ProcessType = pr.Process.ID,   //The Basic Process ID. ex: SMT, Assembly
                            WorkStationName = wr.WorkSatation.Name,
                            WorkStationId = wr.WorkStationID,
                            Capacity = (int)wr.CapacityPerHour,
                            CapacityType = (int)wr.WorkSatation.CapacityType,
                            CapacityTypeName = EnumDisplayName.GetDisplayName((CapacityType)wr.WorkSatation.CapacityType),
                            ExpectedFinishingTime = 0,
                            Sequence = (int)pr.Sequence,
                            ThresholdTime = 0,
                            OldModelChangeOvertime = (int)wr.OldModelChangeOvertime,
                            NewModelChangeOvertime = (int)wr.NewModelChangeOvertime,
                            ExpectedStartDateText = GetNextAvailableDateAfterHoliday(holidays, sdate).ToString("dd/MM/yyyy"),
                            ExpectedStartDate = GetNextAvailableDateAfterHoliday(holidays, sdate),
                            //ExpectedStartDateText = (pr.Process.ID == 12 ? GetNextAvailableDateAfterHoliday(holidays, sdate) : GetNextAvailableDateAfterHoliday(holidays, sdate).AddDays(+(int)pr.LeadTime)).ToString("dd/MM/yyyy"),
                            ExpectedEndDateText = DateTime.Today.AddDays(+1).ToString("dd/MM/yyyy"),
                            ExpectedEndDate = DateTime.Today.AddDays(+1),
                            Selected = false
                        }).ToList();

            return data;

        }

        public List<MOrderBOMViewModel> GetMorderBOM(long mid, int qty, DateTime expstartdate, long? moid)
        {
            List<MOrderBOMViewModel> finaldata = new List<MOrderBOMViewModel>();
            if (moid != null && moid > 0)
            {
                var morderboms = _unit.Repository<MOrderBOM>().GetAll().Where(x => x.MOId == moid).ToList();
                var stocks = GetCurrentStock((long)moid);
                if (morderboms.Count > 0)
                {
                    finaldata = (from pr in morderboms
                                 join st in stocks
                                 on pr.ID equals st.ProductPartId
                                 into temp
                                 from stock in temp.DefaultIfEmpty()
                                 select new MOrderBOMViewModel
                                 {
                                     ID = pr.ID,
                                     PartsID = pr.PartsID,
                                     PartsName = pr.ProductPart.Name,
                                     PartsCode = pr.ProductPart.YwxPartNo,
                                     Dimension = pr.ProductPart.Description,
                                     ParentID = pr.ProductPart.ParentID,
                                     ParentName = (pr.ProductPart.ParentID > 0 ? pr.ProductPart.ProductPart1.Name : ""),
                                     RequiredQty = (int)pr.DraftedQty,
                                     DraftedQty = (int)pr.DraftedQty,
                                     SubmittedQty = (int)pr.SubmittedQty,
                                     ManfacturerId = pr.ManfacturerId,
                                     ManufacturerName = (pr.ManfacturerId != null ? pr.ProductPart.Manufacturer.Name : ""),
                                     StockAvailability = (stock != null ? (long)stock.CurrentStock : 0),
                                     Available = (stock != null ? ((long)stock.CurrentStock > (int)pr.DraftedQty * qty ? true : false) : false),
                                     ExpectedProductionStartDate = pr.ExpectedProductionStartDate,
                                     ExpectedProductionStartDateText = DateTime.Parse(pr.ExpectedProductionStartDate.ToString()).ToString("dd/MM/yyyy")
                                 }).ToList();
                }

                else
                {
                    finaldata = GetProductBOM(mid, qty, expstartdate);
                }
            }
            else
            {
                finaldata = GetProductBOM(mid, qty, expstartdate);
            }
            return finaldata;
        }

        public ManufactureOrderViewModel GetManufacturOrder(long? id)
        {
            ManufactureOrderViewModel morder = new ManufactureOrderViewModel();
            List<MOrderBOMViewModel> boms = new List<MOrderBOMViewModel>();
            //ManufactureOrderViewModel morder = new ManufactureOrderViewModel();
            //ManufactureOrderViewModel morder = new ManufactureOrderViewModel();

            var data = _unit.Repository<ManufactureOrder>().Get((long)id);
            if (data != null)
            {
                morder = Mapper.Map<ManufactureOrderViewModel>(data);
                var ordermodels = Mapper.Map<List<MOrderModelViewModel>>(morder.MOrderModels.Where(x => x.Selected == true));
                morder.MOrderModels = ordermodels;
                // var parts = _unit.Repository<ProductPart>().GetAll().Where(x=>x.ProductModelID=morder.pr);
                //var boms = Mapper.Map<List<MOrderBOMViewModel>>(morder.MOrderBOMs);
                //var boms = (from or in morder.MOrderBOMs
                //            join pr in parts
                //            on or.PartsID equals pr.ID
                //            select new MOrderBOMViewModel
                //            {
                //                PartsID = pr.ID,
                //                PartsName = pr.Name,
                //                PartsCode = pr.ComponentCode,
                //                Dimension = pr.Dimention,
                //                RequiredQty = (int)pr.RequiredQty,
                //                DraftedQty = (int)or.DraftedQty,
                //                SubmittedQty = (int)or.SubmittedQty,
                //                ManfacturerId = pr.Manufacturer.ID,
                //                ManufacturerName = pr.Manufacturer.Name,
                //                StockAvailability = (int)pr.StockAvailability,
                //                ExpectedProductionStartDate = or.ExpectedProductionStartDate,
                //                ExpectedProductionStartDateText = or.ExpectedProductionStartDateText
                //            }).ToList();
                //morder.MOrderBOMs = boms;
                //var routings = Mapper.Map<List<MOrderRoutingViewModel>>(morder.MOrderRoutings);
                //var routings = (from pr in morder.MOrderRoutings
                //            select new MOrderRoutingViewModel
                //            {
                //                ProcessId = pr.ID,
                //                ProcessName = pr.ProcessName,
                //                WorkStationName = pr.WorkStationName,
                //                WorkStationId = pr.WorkStationId,
                //                Capacity = (int)pr.Capacity,
                //                ExpectedFinishingTime = 0,
                //                Sequence = (int)pr.Sequence,
                //                OldModelChangeOvertime = (int)pr.OldModelChangeOvertime,
                //                NewModelChangeOvertime = (int)pr.NewModelChangeOvertime,
                //                ExpectedStartDateText = pr.ExpectedStartDateText,
                //                ExpectedEndDateText = pr.ExpectedEndDateText,
                //                Status = 0
                //            }).ToList();
                //morder.MOrderRoutings = routings;

                return morder;
            }
            else
                return null;
        }

        private List<MOrderBOMViewModel> GetProductBOM(long modelid, int qty, DateTime expstartdate)
        {
            var parts = _unit.Repository<ProductPart>().GetAll().Where(x => x.ProductModelID == modelid).ToList();
            var stocks = GetCurrentStock(modelid);
            var data = (from pr in parts
                        join st in stocks
                        on pr.ID equals st.ProductPartId
                        into temp
                        from stock in temp.DefaultIfEmpty()
                        select new MOrderBOMViewModel
                        {
                            PartsID = pr.ID,
                            PartsName = pr.Name,
                            PartsCode = pr.YwxPartNo,
                            Dimension = pr.Description,
                            ParentID = pr.ParentID,
                            ParentName = (pr.ParentID > 0 ? pr.ProductPart1.Name : ""),
                            RequiredQty = (int)pr.RequiredQty * qty,
                            DraftedQty = (int)pr.RequiredQty * qty,
                            SubmittedQty = (int)pr.RequiredQty * qty,
                            ManfacturerId = pr.ManufacturerId,
                            ManufacturerName = (pr.ManufacturerId > 0 ? pr.Manufacturer.Name : ""),
                            StockAvailability = (stock != null ? (long)stock.CurrentStock : 0),
                            Available = (stock != null ? ((long)stock.CurrentStock > (int)pr.RequiredQty * qty ? true : false) : false),
                            ExpectedProductionStartDate = ((int)pr.RequiredQty * qty > (long)500 ? expstartdate.AddMonths(+(pr.Manufacturer != null ? (int)pr.Manufacturer.AvgShippingTime : 2)) : expstartdate),
                            ExpectedProductionStartDateText = ((int)pr.RequiredQty * qty > (long)500 ? expstartdate.AddMonths(+(pr.Manufacturer != null ? (int)pr.Manufacturer.AvgShippingTime : 2)).ToString("dd/MM/yyyy") : expstartdate.ToString("dd/MM/yyyy"))

                        }).ToList();
            return data;
        }
        public List<StockCheckingDto> GetCurrentStock(long modelid)
        {
            var response = new ResponseMessage();
            StockCheckingDto currentstockdto = new StockCheckingDto();
            List<StockCheckingDto> currentstocklist = new List<StockCheckingDto>();
            var parts = _unit.Repository<ProductPart>().GetAll().Where(x => x.ProductModelID == modelid && x.InventoryItemId > 0).ToList();
            foreach (var part in parts)
            {
                currentstockdto = new StockCheckingDto();
                var connection = OracleDatabaseConnection.GetOldConnection();
                string stockQuery = "SELECT SUBINVENTORY_CODE,  SUM (MOQ.TRANSACTION_QUANTITY) ON_HAND FROM  APPS.MTL_ONHAND_QUANTITIES MOQ" +
                                     " WHERE    MOQ.INVENTORY_ITEM_ID=" + part.InventoryItemId + "  AND MOQ.ORGANIZATION_ID = 646 GROUP BY SUBINVENTORY_CODE";
                OracleDataReader oracleDataReader = null;
                OracleCommand oracleCommand = new OracleCommand(stockQuery, connection) { CommandType = CommandType.Text };

                try
                {
                    connection.Open();
                    oracleDataReader = oracleCommand.ExecuteReader();
                    if (oracleDataReader.HasRows)
                    {
                        while (oracleDataReader.Read())
                        {
                            currentstockdto.CurrentStock = oracleDataReader.IsDBNull(1) ? 0 : oracleDataReader.GetInt64(1);
                            currentstockdto.ProductPartId = part.ID;
                            currentstockdto.Available = false;
                        }
                    }
                    else
                    {
                        currentstockdto.CurrentStock = 0;
                        currentstockdto.ProductPartId = part.ID;
                        currentstockdto.Available = false;
                    }
                    oracleDataReader.Close();
                    connection.Close();
                    oracleCommand.Dispose();
                    currentstocklist.Add(currentstockdto);
                }
                catch (Exception e)
                {
                    response.Message = "Failed" + e.Message;
                    response.MessageType = MessageType.Failed;
                }
            }
            return currentstocklist;
        }

    }
}
