﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMRP.Helper;
using WMRP.Infrastructures.Repositories;
using WMRP_DAL;
using WMRP.Service.IServices;
using WMRP.Service.Dto;

namespace WMRP.Service.Services
{
    public class WorkStationService : IWorkStationService
    {
        private readonly WMRPEntities _context;
        private readonly Unit _unit;

        public WorkStationService()
        {
            _context = new WMRPEntities();
            _unit = new Unit(_context);
        }

        #region for WorkStation

        public List<WorkStationViewModel> GetAllWorkStation()
        {
            List<WorkStationViewModel> workstationList = new List<WorkStationViewModel>();

            var workstations = _unit.Repository<WorkSatation>().GetAll();
            workstationList = Mapper.Map<List<WorkStationViewModel>>(workstations);

            return workstationList;
        }

        public ResponseMessage AddWorkStationDetails(List<WorkStationDetailViewModel> viewmodels)
        {
            var response = new ResponseMessage();
            //var user = HttpContext.Current.Session["user"] as User;
            var entity = new WorkStationDetail();
            var entities = new List<WorkStationDetail>();
            foreach (var item in viewmodels)
            {
                if (item.ID != null && item.ID > 0)
                {
                    entity = new WorkStationDetail();
                    Mapper.Map(item, entity);
                    entity.UpdatedBy = 1;
                    entity.UpdatedDate = DateTime.Now;
                    _unit.Repository<WorkStationDetail>().Update(entity);
                    entities.Add(entity);
                }

                else
                {
                    entity = Mapper.Map<WorkStationDetail>(item);
                    entity.AddedBy = 1;
                    entity.AddedDate = DateTime.Now;
                    entity.Status = true;
                    _unit.Repository<WorkStationDetail>().Add(entity);
                    entities.Add(entity);
                }

            }

            try
            {
                _unit.Commit();
                response.Message = "Workstation Details Have been Saved Successfully";
                response.MessageType = MessageType.Success;
            }
            catch (Exception ex)
            {
                response.Message = "Failed" + ex.Message;
                response.MessageType = MessageType.Failed;
            }
            return response;
        }
        public List<WorkStationDetailViewModel> GetModelStationProcess(long modelid)
        {
            var modelprocess = _unit.Repository<ModelProcess>().GetAll().Where(x=>x.ProductModelID==modelid  && x.Status==true).OrderBy(y=>y.Sequence).ToList();
            var workstations = _unit.Repository<WorkSatation>().GetAll().Where(x => x.Status == true).ToList();

            var data = (from mp in modelprocess
                        join ws in workstations
                        on mp.ProcessID equals ws.ProcessID
                        select new WorkStationDetailViewModel
                        {
                            WorkStationID=ws.ID,
                            WorkSatationName=ws.Name,
                            OldModelChangeOvertime=0,
                            NewModelChangeOvertime=0,
                            CapacityPerHour=0,
                            ModelProcessID=mp.ID,
                            ModelProcessName=mp.Process.Name,
                            Status=false
                        }).ToList();

            return data;
        }

        public List<WorkStationDetailViewModel> GetAllWorkStationDetails(long modelid)
        {
            List<WorkStationDetailViewModel> workstationList = new List<WorkStationDetailViewModel>();

            var workstations = _unit.Repository<WorkStationDetail>().GetAll().Where(x=>x.ModelProcess.ProductModelID==modelid).OrderBy(y=>y.ModelProcess.Sequence).ToList();
            //var data = (from wd in workstations
            //            join W in _unit.Repository<WorkSatation>().GetAll() on wd.WorkSatation.    
                            
            //                )
            if (workstations.Count > 0)
                workstationList = Mapper.Map<List<WorkStationDetailViewModel>>(workstations);
            else
                workstationList = GetModelStationProcess(modelid);

            return workstationList;
        }

        public ResponseMessage AddWorkStation(WorkStationViewModel workStataionViewModel)
        {
            var response = new ResponseMessage();
            //var user = HttpContext.Current.Session["user"] as User;
            var workStation = new WorkSatation();
            try
            {
                if (workStataionViewModel.ID != null && workStataionViewModel.ID > 0)
                {

                    Mapper.Map(workStataionViewModel, workStation);
                    _unit.Repository<WorkSatation>().Update(workStation);
                    _unit.Commit();

                    response.Message = "WorkStation Has Been Updated Successfully";
                    response.MessageType = MessageType.Success;

                    return response;
                }

                else
                {
                    workStation = Mapper.Map<WorkSatation>(workStataionViewModel);
                    workStation.Status = true;

                    _unit.Repository<WorkSatation>().Add(workStation);
                    _unit.Commit();

                    response.Message = "WorkStation Has Been Added Successfully";
                    response.MessageType = MessageType.Success;
                    return response;
                }


            }
            catch (Exception ex)
            {
                response.Message = "Failed" + ex.Message;
                response.MessageType = MessageType.Failed;
                return response;
            }
        }

        public WorkStationViewModel GetWorkStation(long? id)
        {
            var workStation = _unit.Repository<WorkSatation>().Get((long)id);

            return Mapper.Map<WorkStationViewModel>(workStation);
        }

        public ResponseMessage DeleteWorkStation(long id)
        {
            var response = new ResponseMessage();
            try
            {
                if (id != null && id > 0)
                {
                    var WorkStation = _unit.Repository<WorkSatation>().Get(id);
                    WorkStation.Status = false;
                    _unit.Repository<WorkSatation>().Update(WorkStation);
                    _unit.Commit();

                    response.Message = "WorkStation Has been Deleted Successfully";
                    response.MessageType = MessageType.Success;
                    return response;
                }
                else
                {
                    response.Message = "Invalid Id";
                    response.MessageType = MessageType.Failed;

                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Message = "Failed" + ex.Message;
                response.MessageType = MessageType.Failed;
                return response;
            }
        }



        #endregion
    }
}
