﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMRP.Infrastructures.Repositories;
using WMRP.Service.Dto;
using WMRP_DAL;
using AutoMapper;
using WMRP.Service.IServices;
using WMRP.Helper;


namespace WMRP.Service.Services
{
    public class MRPProcessService : IProcessService
    {
        private readonly WMRPEntities _context;
        private readonly Unit _unit;

        public MRPProcessService()
        {
            _context = new WMRPEntities();
            _unit = new Unit(_context);
        }
        public ModelProcessViewModel GetProcess(long? id)
        {
            var data = _unit.Repository<ModelProcess>().Get((long)id);
            return Mapper.Map<ModelProcessViewModel>(data);
        }
        public List<ModelProcessViewModel> GetAllProcess(long? modelid)
        {
            List<ModelProcessViewModel> processList = new List<ModelProcessViewModel>();
            var data = _unit.Repository<ModelProcess>().GetAll().Where(x=>x.Status==true);
            if(modelid>0)
                data = data.Where(x=>x.ProductModelID==modelid).ToList();
            processList = Mapper.Map<List<ModelProcessViewModel>>(data);
            return processList.OrderBy(o=>o.Sequence).ToList();
        }

        public List<ProcessViewModel> GetAllBasicProcess()
        {
            List<ProcessViewModel> processList = new List<ProcessViewModel>();
            var data = _unit.Repository<Process>().GetAll();
            processList = Mapper.Map<List<ProcessViewModel>>(data);
            return processList;

        }

        public ResponseMessage AddProcess(List<ModelProcessViewModel> viewmodels)
        {
            var response = new ResponseMessage();
            //var user = HttpContext.Current.Session["user"] as User;
            var entity = new ModelProcess();
            var processentities = new List<ModelProcess>();
            foreach (var item in viewmodels)
            {
                if (item.ID != null && item.ID > 0)
                {
                    entity = new ModelProcess();
                    Mapper.Map(item, entity);
                    entity.UpdatedBy = 1;
                    entity.UpdatedDate = DateTime.Now;
                    _unit.Repository<ModelProcess>().Update(entity);
                    processentities.Add(entity);
                 }

                else
                {
                    entity = Mapper.Map<ModelProcess>(item);
                    entity.AddedBy = 1;
                    entity.AddedDate = DateTime.Now;
                    entity.Status = true;
                    _unit.Repository<ModelProcess>().Add(entity);
                    processentities.Add(entity);
                }
                
            }

            try
            {
                _unit.Commit();
                response.Message = "Process Have been Saved Successfully";
                response.MessageType = MessageType.Success;
            }
            catch (Exception ex)
            {
                response.Message = "Failed" + ex.Message;
                response.MessageType = MessageType.Failed;
            }
            return response;
        }

        public ResponseMessage DeleteById(long? id)
        {
            var response = new ResponseMessage();
            var data = _unit.Repository<ModelProcess>().Get((long)id);

            if (data.ID != null && data.ID > 0)
            {
                data.Status = false;
                data.UpdatedBy = 1;
                data.UpdatedDate = DateTime.Now;
                _unit.Repository<ModelProcess>().Update(data);
                _unit.Commit();
                response.Message = "Procee Have been Deleted Successfully";
                response.MessageType = MessageType.Success;
                return response;
            }

            else
            {
                response.Message = "There is no Item to Delete!!";
                response.MessageType = MessageType.Failed;
                return response;
            }

        }
    }
}
