﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using WMRP_DAL;
using WMRP.Helper;
using WMRP.Service.IServices;
using WMRP.Infrastructures.Repositories;

namespace WMRP.Service.IServices
{
    public class AccountService:IAccountService
    {
         private readonly Unit _unit;


         public AccountService(Unit unit)
        {
            _unit = unit;
        }


        #region USER

        public int AddUser(User user)
        {
            var usr = HttpContext.Current.Session["user"] as User;
            try
            {
                user.AddedBy = usr==null?0:usr.Id;
                user.AddedDate = DateTime.Now;
                _unit.UserRepository.Add(user);
                _unit.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public List<User> GetUsers()
        {
            var model = _unit.UserRepository.GetAll();
            return model.ToList();
        }

        public User GetUser(long id)
        {
            var model = _unit.UserRepository.Get(id);
            return model;
        }

        public List<User> GetUsersByInchargeId(string employeeId)
        {
            var model = _unit.UserRepository.GetUsersByInchargeId(employeeId);
            return model;
        }

        #endregion

        #region ROLE

        public int AddRole(Role model)
        {
            var user = HttpContext.Current.Session["user"] as User;
            try
            {
                model.AddedBy = user.Id;
                model.AddedDate = DateTime.Now;
                _unit.RoleRepository.Add(model);
                _unit.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public List<Role> GetRoles()
        {
            var model = _unit.RoleRepository.GetAll();
            return model.ToList();
        }

        public Role GetRole(long id)
        {
            var model = _unit.RoleRepository.Get(id);
            return model;
        }

        public User LoginAuthorize(string userName, string password)
        {
            try
            {
                User user = _unit.LoginRepository.GetLogin(userName, password);
                if (user != null)
                {
                    string roleName = user.Role;
                    var ticket = new FormsAuthenticationTicket(
                        1, // Ticket version
                        Convert.ToString(user.EmployeeId), // Username associated with ticket
                        DateTime.Now, // Date/time issued
                        DateTime.Now.AddMinutes(60), // Date/time to expire
                        true, // "true" for a persistent user cookie
                        roleName, // User-data, in this case the roles
                        FormsAuthentication.FormsCookiePath); // Path cookie valid for


                    // Encrypt the cookie using the machine key for secure transport,
                    string hash = FormsAuthentication.Encrypt(ticket);
                    var cookie = new HttpCookie(
                        FormsAuthentication.FormsCookieName, // Name of auth cookie
                        hash); // Hashed ticket

                    // Set the cookie's expiration time to the tickets expiration time
                    if (ticket.IsPersistent) cookie.Expires = ticket.Expiration;

                    // Add the cookie to the list for outgoing response
                    HttpContext.Current.Response.Cookies.Add(cookie);
                    return user;
                }
                return null;
            }
            catch (Exception e)
            {
                Logger.SaveLogger(e.Message, "AuthorizeUserForLogin");
                throw;
            }
        }

        public List<Permission> GetUserPermissions(string employeeId)
        {
            List<Permission> permissions = _unit.PermissionRepository.Find(i => i.EmployeeId == employeeId && i.IsAccessable == true).ToList();
            return permissions;
        }

        public string SavePermissions(List<string> permissionList, string employeeId)
        {
            try
            {
                var permissions = new List<Permission>();
                foreach (var p in permissionList)
                {
                    var stringSprintAr = p.Split(',');
                    string controllerName = stringSprintAr[0];
                    var actionAndDescription = stringSprintAr[1].Contains('-')
                        ? stringSprintAr[1].Split('-')
                        : new[] { stringSprintAr[1], "No Description" };
                    string action = actionAndDescription[0];
                    string description = actionAndDescription[1];
                    controllerName = controllerName.Replace("Controller", "");
                    var permission = new Permission
                    {
                        ActionName = action,
                        ControllerName = controllerName,
                        EmployeeId = employeeId,
                        Description = description,
                        IsAccessable = true

                    };
                    permissions.Add(permission);
                }
                var userPreviousPermissions = _unit.PermissionRepository.Find(i => i.EmployeeId == employeeId);
                foreach (var permission in permissions)
                {
                    var index = userPreviousPermissions.FindIndex(
                        i =>
                            i.ControllerName.Equals(permission.ControllerName) &&
                            i.ActionName.Equals(permission.ActionName));
                    if (index >= 0)
                    {
                        userPreviousPermissions[index].IsAccessable = true;
                        userPreviousPermissions[index].Description = permission.Description;
                        _unit.PermissionRepository.Update(userPreviousPermissions[index]);
                    }
                    else
                    {
                        _unit.PermissionRepository.Add(permission);
                    }
                }
                _unit.Commit();

                foreach (var previousPermission in userPreviousPermissions)
                {
                    var prevIndex =
                        permissions.FindIndex(
                            i =>
                                i.ControllerName.Equals(previousPermission.ControllerName) &&
                                i.ActionName.Equals(previousPermission.ActionName));
                    if (prevIndex < 0)
                    {
                        previousPermission.IsAccessable = false;
                        _unit.PermissionRepository.Update(previousPermission);
                        _unit.Commit();
                    }
                }
                return "Success";
            }
            catch (Exception exception)
            {
                return exception.Message;
            }

        }

        #endregion
    }
}