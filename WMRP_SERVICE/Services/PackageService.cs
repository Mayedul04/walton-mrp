﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using WMRP.Helper;
using WMRP.Infrastructures.Repositories;
using WMRP.IServices;
using WMRP.Service.Dto;
using WMRP_DAL;

namespace WMRP.Services
{
    public class PackageService : IPackageService
    {
        private readonly WMRPEntities _context;
        private readonly Unit _unit;

        public PackageService()
        {
            _context = new WMRPEntities();
            _unit = new Unit(_context);
        }

        public List<ProductModelViewModel> GetProductModelsByProduct(long id)
        {
            List<ProductModelViewModel> modelList = new List<ProductModelViewModel>();

            var models = _unit.Repository<ProductModel>().GetAll();
            modelList = Mapper.Map<List<ProductModelViewModel>>(models);
            modelList = modelList.Where(x => x.ProductId == id).ToList();

            return modelList;

        }

        public List<ProductViewModel> GetAllProducts()
        {
            List<ProductViewModel> productsList = new List<ProductViewModel>();

            var products = _unit.Repository<Product>().GetAll();
            productsList = Mapper.Map<List<ProductViewModel>>(products);

            return productsList;
        }

        public List<PackageConfigurationViewModel> GetAllPackages()
        {
            List<PackageConfigurationViewModel> packageList = new List<PackageConfigurationViewModel>();

            var products = _unit.Repository<PackageConfiguration>().GetAll();
            packageList = Mapper.Map<List<PackageConfigurationViewModel>>(products);

            return packageList;
        }

        public ResponseMessage AddPackage(PackageConfigurationViewModel packageConfigurationViewModels)
        {
            var response = new ResponseMessage();
            var entity = new PackageConfiguration();


            var packagedetails = new List<PackageDetail>();

            var detail = new PackageDetail();

            if (packageConfigurationViewModels.PackageDetails != null &&
                packageConfigurationViewModels.PackageDetails.Count > 0)
            {
                foreach (var item in packageConfigurationViewModels.PackageDetails)
                {
                    if (item.ID > 0)
                    {
                        detail = new PackageDetail();
                        detail = _unit.Repository<PackageDetail>().Get(item.ID);
                        //single.ExpectedStartDate = DateTime.Parse(item.ExpectedStartDateText);
                        Mapper.Map(item, detail);
                        _unit.Repository<PackageDetail>().Update(detail);
                        packagedetails.Add(detail);
                    }
                    else
                    {
                        detail = Mapper.Map<PackageDetail>(item);
                        //single.ExpectedStartDate = DateTime.Parse(item.ExpectedStartDateText);
                        _unit.Repository<PackageDetail>().Add(detail);
                        packagedetails.Add(detail);
                    }

                }
            }


            try
            {
                if (packageConfigurationViewModels.ID > 0)
                {

                    Mapper.Map(packageConfigurationViewModels, entity);
                    entity.Status = true;
                    entity.PackageDetails = packagedetails;
                    _unit.Repository<PackageConfiguration>().Update(entity);
                    _unit.Commit();
                    response.Message = "Package Configuration Has been Updated Successfully";
                    response.MessageType = MessageType.Success;
                    return response;
                }

                else
                {
                    entity = Mapper.Map<PackageConfiguration>(packageConfigurationViewModels);
                    entity.AddedBy = 1;
                    entity.AddedDate = DateTime.Now;
                    entity.Status = true;
                    entity.PackageDetails = packagedetails;
                    _unit.Repository<PackageConfiguration>().Add(entity);
                    _unit.Commit();
                    response.Message = "Package Configuration Has been Added Successfully";
                    response.MessageType = MessageType.Success;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Message = "Failed" + ex.Message;
                response.MessageType = MessageType.Failed;
                return response;
            }
        }

        public ResponseMessage Delete(long? id)
        {
            var response = new ResponseMessage();
            var packageDetails = _unit.Repository<PackageDetail>().Get((long)id);

            if (packageDetails.ID > 0)
            {
                //packageDetails.Status = false;
                //packageDetails.UpdatedBy = 1;
                //packageDetails.UpdatedDate = DateTime.Now;
                _unit.Repository<PackageDetail>().Remove(packageDetails);
                _unit.Commit();
                response.Message = "Package Detail Has been Deleted Successfully";
                response.MessageType = MessageType.Success;
                return response;
            }

            else
            {
                response.Message = "There is no Item to Delete!!";
                response.MessageType = MessageType.Failed;
                return response;
            }

        }

        public List<PackageConfigurationViewModel> GetPackageListForAutoFill(string prefix)
        {
            prefix = prefix.ToLower();
            var data = _unit.Repository<PackageConfiguration>().GetAll()
                .Where(x => (x.PackageName.ToLower().Contains(prefix)))
                .OrderBy(x => x.PackageName)
                .Select(x => new PackageConfigurationViewModel { ID = x.ID, PackageName = x.PackageName })
                .Take(10)
                .ToList();
            return data;
        }

        public List<PackageDetailViewModel> GetDetailsByPackageId(long id)
        {
            List<PackageDetailViewModel> packageDetails = new List<PackageDetailViewModel>();
            var detailList = _unit.Repository<PackageDetail>().GetAll().Where(x => x.PackageId == id && x.PackageConfiguration.Status == true).ToList();
            packageDetails = Mapper.Map<List<PackageDetailViewModel>>(detailList);
            return packageDetails;

        }
        public List<MOrderModelViewModel> GetPackageDetailsByMOId(long id, long? moid)
        {
            List<MOrderModelViewModel> packageDetails = new List<MOrderModelViewModel>();
            var mordermodels = _unit.Repository<MOrderModel>().GetAll().Where(x => x.PackageID == id && x.MOId == moid).ToList();
            packageDetails = Mapper.Map<List<MOrderModelViewModel>>(mordermodels);
            return packageDetails;
        }
    }
}
