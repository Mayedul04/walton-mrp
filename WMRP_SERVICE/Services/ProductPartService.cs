﻿using System.Data.Entity.Core.Metadata.Edm;
using System.Web;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml.FormulaParsing.Excel.Functions.RefAndLookup;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using Oracle.ManagedDataAccess.Client;
using WMRP.Helper;
using WMRP.Infrastructures.Repositories;
using WMRP_DAL;
using WMRP.Service.Services;
using WMRP.Service.IServices;
using WMRP.Service.Dto;
using OfficeOpenXml;
using WMRP.Service;

using System.Data;

namespace WMRP.Service.Services
{
    public class ProductPartService : IProductPartService
    {
        private readonly WMRPEntities _context;
        private readonly Unit _unit;

        public ProductPartService()
        {
            _context = new WMRPEntities();
            _unit = new Unit(_context);
        }

        #region for Product Parts

        public ProductPartViewModel GetProductPart(long ?id)
        {
            var productPart = _unit.Repository<ProductPart>().Get((long)id);
            return Mapper.Map<ProductPartViewModel>(productPart); 
        }

        public List<ProductPartViewModel> GetAllProductParts()
        {
            List<ProductPartViewModel> productPartList = new List<ProductPartViewModel>();
            var productParts = _unit.Repository<ProductPart>().GetAll();

            productPartList = Mapper.Map<List<ProductPartViewModel>>(productParts);

            return productPartList;            

        }

        public List<CommercialReportViewModel> GetCommercialReport(List<ProductPart> productParts)
        {
            List<CommercialReportViewModel> commercialList = new List<CommercialReportViewModel>();

            foreach (var part in productParts)
            {
                var productPart = GetProductPart(part.ID);
                productPart = Mapper.Map<ProductPartViewModel>(productPart);
                var bomStock = _unit.StockRepository.GetBomStockByPartID(productPart.ID);
                var stockShortage = 0L;
                //var currentStock = bomStock.CurrentStock;

                //if (productPart.RequiredQty > currentStock)
                //{
                //    stockShortage = (long)(productPart.RequiredQty - currentStock);
                //}

                CommercialReportViewModel commercialReport = new CommercialReportViewModel();

                commercialReport.ID = productPart.ID;
                commercialReport.Name = productPart.Name;
                commercialReport.ProductModelID = productPart.ProductModelID;
                commercialReport.ProductModelName = productPart.ProductModelName;
                commercialReport.ManufacturerId = productPart.ManufacturerId;
                commercialReport.ManufacturerName = productPart.ManufacturerName;
                commercialReport.PurchaseAmount = Convert.ToInt32(stockShortage);

                commercialList.Add(commercialReport);
            }
            return commercialList;
        }

        public List<ProductPartViewModel> GetAllProductParts(long pid)
        {
            List<ProductPartViewModel> productPartList = new List<ProductPartViewModel>();


            var productParts = _unit.Repository<ProductPart>().GetAll();
            productPartList = Mapper.Map<List<ProductPartViewModel>>(productParts);

            return productPartList;
        }
        public List<ProductPartViewModel> GetAllProductPartsByModel(long id)
        {
            List<ProductPartViewModel> productPartList = new List<ProductPartViewModel>();
             
            var productParts = _unit.Repository<ProductPart>().GetAll();
            productParts = productParts.Where(x => x.ProductModelID == id).ToList();
            productPartList = Mapper.Map<List<ProductPartViewModel>>(productParts);

            return productPartList;
        }
 
        public ResponseMessage AddProductPart(List<ProductPartViewModel>productPartViewModels)
        {
            var response = new ResponseMessage();         
            var entity = new ProductPart();

            if (productPartViewModels.Any())
            {
                foreach (var item in productPartViewModels)
                {
                    try
                    {
                        if (item.ID > 0)
                        {
                            entity = new ProductPart();
                            Mapper.Map(item, entity);
                            entity.UpdatedBy = 1;
                            entity.UpdatedDate = DateTime.Now;
                            entity.Status = true;

                            _unit.Repository<ProductPart>().Update(entity);
                            _unit.Commit();

                            response.Message = "Product Part Has been Updated Successfully";
                            response.MessageType = MessageType.Success;

                            //return response;
                        }

                        else
                        {
                            entity = Mapper.Map<ProductPart>(item);
                            entity.AddedBy = 1;
                            entity.AddedDate = DateTime.Now;
                            entity.Status = true;

                            _unit.Repository<ProductPart>().Add(entity);
                            _unit.Commit();

                            response.Message = "Product Part Has been Added Successfully";
                            response.MessageType = MessageType.Success;

                           // return response;
                        }


                    }
                    catch (Exception ex)
                    {
                        response.Message = "Failed" + ex.Message;
                        response.MessageType = MessageType.Failed;
                        //return response;
                    }
                }
               
                
            }
            return response;

        }

        public ResponseMessage DeleteProductPart(long id)
        {
            var response = new ResponseMessage();
            try
            {
                if (id != null && id > 0)
                {
                    var productPart = _unit.Repository<ProductPart>().Get(id);
                    productPart.Status = false;
                    _unit.Repository<ProductPart>().Update(productPart);
                    
                    _unit.Commit();

                    response.Message = "Product Part Has been Deleted Successfully";
                    response.MessageType = MessageType.Success;
                    return response;
                }
                else
                {
                    response.Message = "Invalid Id";
                    response.MessageType = MessageType.Failed;

                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Message = "Failed" + ex.Message;
                response.MessageType = MessageType.Failed;
                return response;
            }
        }

        public List<ProductPartViewModel> GetAllChild(long? id)
        {
            List<ProductPartViewModel> childProductPartList = new List<ProductPartViewModel>();

            var childProductList = _unit.Repository<ProductPart>().GetAll().Where(x => x.ParentID == id);
            childProductPartList = Mapper.Map<List<ProductPartViewModel>>(childProductList);

            return childProductPartList;
        }

        public string UpdateBomFromOracle()
        {
            var response = new ResponseMessage();      
            List<ProductPart> productParts = _unit.Repository<ProductPart>().GetAll();
            List<ProductPart> entityProductPart = new List<ProductPart>();
            var rowAffected = 0;
            foreach (var part in productParts)
            {
                string bomQuery = "SELECT  MSI.CREATION_DATE," + "MSI.ORGANIZATION_ID," + "MSI.INVENTORY_ITEM_ID ," +
                                  "MSI.SEGMENT1 " + "||'.'||" + "MSI.SEGMENT2" + "||'.'||" + "MSI.SEGMENT3 ITEM_CODE," +
                                  "MSI.DESCRIPTION ITEM_NAME," + " MSI.PRIMARY_UOM_CODE UOM," + "MCB.SEGMENT1," +
                                  " MCB.SEGMENT2" +
                                  " FROM MTL_SYSTEM_ITEMS MSI," + " MTL_ITEM_CATEGORIES MIC," + "MTL_CATEGORIES_B MCB" +
                                  " WHERE" +
                                  "  MSI.INVENTORY_ITEM_STATUS_CODE='Active' AND MCB.SEGMENT1 = 'RAW MATERIAL'  AND MSI.ORGANIZATION_ID=646 " +
                                  "AND MSI.DESCRIPTION = '" + part.Description + "' and  rownum = 1";

                var connection = OracleDatabaseConnection.GetOldConnection();
                OracleDataReader oracleDataReader = null;
                OracleCommand oracleCommand = new OracleCommand(bomQuery, connection) { CommandType = CommandType.Text };

                try
                {
                    connection.Open();
                    oracleDataReader = oracleCommand.ExecuteReader();
                    if (oracleDataReader.HasRows)
                    {
                        while (oracleDataReader.Read())
                        {
                            part.InventoryItemId = oracleDataReader.IsDBNull(2) ? 0 : oracleDataReader.GetInt64(2);

                        }
                        rowAffected++;
                    }
                    oracleDataReader.Close();
                    connection.Close();
                    oracleCommand.Dispose();
                    
                    //response.Message = "Product Part Has been Updated Successfully";
                    //response.MessageType = MessageType.Success;
                }
                catch (Exception e)
                {
                    response.Message = "Failed" + e.Message;
                    response.MessageType = MessageType.Failed;
                }
            }
            _unit.Repository<ProductPart>().UpdateRange(productParts);
            _unit.Commit();

            //return "Total Rows Affected: " + rowAffected;
             return "Total Rows Affected: " + rowAffected;
            
           
        }


        #endregion

        #region Manufacturer

        public List<ManufacturerViewModel> GetAllManufacturers()
        {
            List<ManufacturerViewModel> manufacturerList = new List<ManufacturerViewModel>();

            var manufacturers =  _unit.Repository<Manufacturer>().GetAll().ToList();

            manufacturerList = Mapper.Map<List<ManufacturerViewModel>>(manufacturers);

            return manufacturerList;

        }


        #endregion


        public void AddBOMFile(HttpPostedFileBase bomFile)
        {
            string ExcuteMsg = string.Empty;
            //int NumberOfColume = 0;            
            //HttpPostedFileBase file = Request.Files["ProductExcel"];
            HttpPostedFileBase file = bomFile;
            //Extaintion Check
            if (bomFile.FileName.EndsWith("xls") || bomFile.FileName.EndsWith("xlsx") ||
                bomFile.FileName.EndsWith("XLS") ||
                bomFile.FileName.EndsWith("XLSX"))
            {
                //Null Exp Check
                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));                 

                    using (var package = new ExcelPackage(file.InputStream))
                    {
                        var currentSheet = package.Workbook.Worksheets;
                        var workSheet = currentSheet.First();
                        var noOfCol = workSheet.Dimension.End.Column;
                        var noOfRow = workSheet.Dimension.End.Row;

                        ProductPart part = new ProductPart();
                        
                        List<ProductPart> partList = new List<ProductPart>();

                        for (int rowIterator = 3; rowIterator <= noOfRow; rowIterator++)
                        {
                            part = new ProductPart();

                            part.YwxPartNo = workSheet.Cells[rowIterator, 1].Value == null
                                ? null
                                : workSheet.Cells[rowIterator, 1].Value.ToString();

                            part.Name = workSheet.Cells[rowIterator, 2].Value == null
                             ? null
                             : workSheet.Cells[rowIterator, 2].Value.ToString();

                            part.ChineseName = workSheet.Cells[rowIterator, 3].Value == null
                             ? null
                             : workSheet.Cells[rowIterator, 3].Value.ToString();

                            part.ProductModelID = Convert.ToInt32(workSheet.Cells[rowIterator, 4].Value) == 0
                             ? 0
                             : Convert.ToInt32(workSheet.Cells[rowIterator, 4].Value);

                            part.Description = workSheet.Cells[rowIterator, 5].Value == null
                             ? null
                             : workSheet.Cells[rowIterator, 5].Value.ToString();

                            part.RequiredQty = Convert.ToInt32(workSheet.Cells[rowIterator, 6].Value) == 0
                                ? 0
                                : Convert.ToInt32(workSheet.Cells[rowIterator, 6].Value);

                            //part.ManufacturerId = Convert.ToInt32(workSheet.Cells[rowIterator, 7].Value) == 0
                            //    ? 0
                            //    : Convert.ToInt32(workSheet.Cells[rowIterator, 7].Value);


                            if (workSheet.Cells[rowIterator, 7].Value == null)
                            {
                                part.ManufacturerId = null;
                            }
                            else
                            {
                                part.ManufacturerId = Convert.ToInt32(workSheet.Cells[rowIterator, 7].Value);

                            }

                            part.ManufacturePartNo = workSheet.Cells[rowIterator, 8].Value == null
                             ? null
                             : workSheet.Cells[rowIterator, 8].Value.ToString();

                           
                            
                            part.MPQ = Convert.ToInt32(workSheet.Cells[rowIterator, 9].Value) == 0
                                 ? 0
                                 : Convert.ToInt32(workSheet.Cells[rowIterator, 9].Value);

                            part.MOQ = Convert.ToInt32(workSheet.Cells[rowIterator, 10].Value) == 0
                                ? 0
                                : Convert.ToInt32(workSheet.Cells[rowIterator, 10].Value);

                            //part.ParentID = Convert.ToInt32(workSheet.Cells[rowIterator, 12].Value) == 0
                            //    ? 0
                            //    : Convert.ToInt32(workSheet.Cells[rowIterator, 12].Value);
                            //part.PartsType = Convert.ToInt32(workSheet.Cells[rowIterator, 13].Value) == 0
                            //   ? 0
                            //   : Convert.ToInt32(workSheet.Cells[rowIterator, 13].Value);
                            if (workSheet.Cells[rowIterator, 12].Value == null)
                            {
                                part.ParentID = null;
                            }
                            else
                            {
                                part.ParentID = Convert.ToInt32(workSheet.Cells[rowIterator, 12].Value);

                            }
                            if (workSheet.Cells[rowIterator, 13].Value == null)
                            {
                                part.PartsType = null;
                            }
                            else
                            {
                                part.PartsType = Convert.ToInt32(workSheet.Cells[rowIterator, 13].Value);

                            }
                            //part.AddedBy = 1;
                            //part.AddedDate = DateTime.Now;
                            //part.Status = true;

                            partList.Add(part);

                            
                           
                            //entity.PartsType = 2;
                            //entity.ParentID = 0;
                                                     
                            
                        }
                        //_unit.Repository<ProductPart>().AddRange(partList);
                        //_unit.Commit();


                        foreach (var entity in partList)
                        {
                            entity.AddedBy = 1;
                            entity.AddedDate = DateTime.Now;
                            entity.Status = true;

                            _unit.Repository<ProductPart>().Add(entity);
                            _unit.Commit();
                        }
                    }

                }
            }
            else
            {
                //ViewBag.Error = "File type is incorrect <br>";
            }
        }
    }
}
