﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMRP.Helper;
using WMRP.HELPER;
using WMRP.Infrastructures.Repositories;
using WMRP.Service.IServices;
using WMRP.Service.Dto;
using WMRP_DAL;

namespace WMRP.Service.IServices
{
    public class BasicService : IBasicServices
    {
        private readonly WMRPEntities _context;
        private readonly CellPhoneProjectEntities _ccontext;
        private readonly Unit _unit;
        private readonly CellPhoneUnit _cunit;


        public BasicService()
        {
            _context = new WMRPEntities();
            _unit = new Unit(_context);
            _ccontext = new CellPhoneProjectEntities();
            _cunit = new CellPhoneUnit(_ccontext);
        }

        #region Manufacturer

        public List<ManufacturerViewModel> GetAllManufacturer()
        {
            List<ManufacturerViewModel> manufacturerList = new List<ManufacturerViewModel>();
            var data = _unit.Repository<Manufacturer>().GetAll();
            manufacturerList = Mapper.Map<List<ManufacturerViewModel>>(data);
            return manufacturerList;
        }

        #endregion

        #region ProductGroup

        public List<ProductGroupViewModel> GetProductGroups()
        {
            List<ProductGroupViewModel> groupList = new List<ProductGroupViewModel>();
            var data = _unit.Repository<ProductGroup>().GetAll();
            groupList = Mapper.Map<List<ProductGroupViewModel>>(data);
            return groupList;
        }

        #endregion

        #region Manufacture Types

        public List<ManufactureTypeViewModel> GetManufactureTypes()
        {
            List<ManufactureTypeViewModel> manufactureList = new List<ManufactureTypeViewModel>();
            var data = _unit.Repository<ManufactureType>().GetAll();
            manufactureList = Mapper.Map<List<ManufactureTypeViewModel>>(data);
            return manufactureList;
        }

        #endregion

        public List<EnumDto> GetPartTypes()
        {
            var enumtypes = Enum.GetValues(typeof(PartsType)).Cast<PartsType>()
                .Select(t => new EnumDto
                {
                    ID = ((int)t),
                    Name = EnumDisplayName.GetDisplayName((PartsType)t)
                });

            return enumtypes.ToList();
        }
        public List<EnumDto> GetOrderStatusTypes()
        {
            var enumtypes = Enum.GetValues(typeof(OrderStatus)).Cast<OrderStatus>()
                .Select(t => new EnumDto
                {
                    ID = ((int)t),
                    Name = EnumDisplayName.GetDisplayName((OrderStatus)t)
                });

            return enumtypes.ToList();
        }
        public List<EnumDto> GetStockTypes()
        {
            var enumtypes = Enum.GetValues(typeof(StockType)).Cast<StockType>()
                .Select(t => new EnumDto
                {
                    ID = ((int)t),
                    Name = EnumDisplayName.GetDisplayName((StockType)t)
                });

            return enumtypes.ToList();
        }

        public List<DateTime> HolidayList()
        {
            var holidays = _cunit.Repository<GovernmentHolidayTable>().GetAll().Select(x => DateTime.Parse(x.HolidayDate.ToString())).ToList();
            return holidays;
        }
       
    }
}
