﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMRP.Infrastructures.Repositories;
using WMRP.Service.Dto;
using WMRP_DAL;
using AutoMapper;
using WMRP.Service.IServices;
using WMRP.Helper;

namespace WMRP.Service.Services
{
    public class ProductModelService : IProductModelService
    {
        private readonly WMRPEntities _context;
        private readonly Unit _unit;

        public ProductModelService()
        {
            _context = new WMRPEntities();
            _unit = new Unit(_context);
        }


        #region Products
        public List<ProductViewModel> GetAllProducts()
        {
            List<ProductViewModel> productList = new List<ProductViewModel>();

            var data = _unit.Repository<Product>().GetAll();

            productList = Mapper.Map<List<ProductViewModel>>(data);

            return productList;

        }

        public List<ProductViewModel> GetAllProductByGroup(long? id)
        {
            List<ProductViewModel> productList = new List<ProductViewModel>();
            var data = _unit.Repository<Product>().GetAll();
            productList = Mapper.Map<List<ProductViewModel>>(data);

            if (id != null)
            {
                productList = productList.Where(x => x.GroupId == id).ToList();
            }

            return productList;

        }

        public ProductViewModel GetProduct(long? id)
        {
            var data = _unit.Repository<Product>().Get((long)id);
            return Mapper.Map<ProductViewModel>(data);
        }
        #endregion



        #region Product Model
        public List<ProductModelViewModel> GetModels(long? pid)
        {
            List<ProductModelViewModel> productModels = new List<ProductModelViewModel>();

            var data = _unit.Repository<ProductModel>().GetAll();
            data = data.Where(x => x.Status == true || x.Status == null).ToList();
            productModels = Mapper.Map<List<ProductModelViewModel>>(data);
            if (pid != null)
                productModels = productModels.Where(x => x.ProductGroupId == pid).ToList();

            return productModels;
        }

        public ProductModelViewModel GetModel(long? id)
        {
            var data = _unit.Repository<ProductModel>().Get((long)id);

            return Mapper.Map<ProductModelViewModel>(data);
        }
        public ResponseMessage AddModel(ProductModelViewModel productmodelViewModel)
        {
            var response = new ResponseMessage();
            //var user = HttpContext.Current.Session["user"] as User;
            var entity = new ProductModel();
            try
            {
                if (productmodelViewModel.ID != null && productmodelViewModel.ID > 0)
                {

                    Mapper.Map(productmodelViewModel, entity);
                    entity.UpdatedBy = 1;
                    entity.UpdatedDate = DateTime.Now;
                    _unit.Repository<ProductModel>().Update(entity);
                    _unit.Commit();
                    response.Message = "Model Has been Updated Successfully";
                    response.MessageType = MessageType.Success;
                    return response;
                }

                else
                {
                    entity = Mapper.Map<ProductModel>(productmodelViewModel);
                    //Mapper.Map(productViewModel, entity);
                    entity.AddedBy = 1;
                    entity.AddedDate = DateTime.Now;
                    _unit.Repository<ProductModel>().Add(entity);
                    _unit.Commit();
                    response.Message = "Model Has been Added Successfully";
                    response.MessageType = MessageType.Success;
                    return response;
                }


            }
            catch (Exception ex)
            {
                response.Message = "Failed" + ex.Message;
                response.MessageType = MessageType.Failed;
                return response;
            }
        }
        public ResponseMessage DeleteProductModel(long? id)
        {
            //var product = new Product();
            var response = new ResponseMessage();
            try
            {
                if (id != null || id > 0)
                {
                    var model = _unit.Repository<ProductModel>().Get((long)id);
                    model.Status = false;
                    _unit.Repository<ProductModel>().Update(model);
                    _unit.Commit();
                    response.Message = "Model Has been Deleted Successfully";
                    response.MessageType = MessageType.Success;
                    return response;
                }
                else
                {
                    response.Message = "Invalid Id";
                    response.MessageType = MessageType.Failed;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Message = "Failed" + ex.Message;
                response.MessageType = MessageType.Failed;
                return response;
            }
        }

        public List<ProductModelViewModel> GetModelListForAutoFill(string prefix, long? prid)
        {
            List<ProductModelViewModel> modelList = new List<ProductModelViewModel>();
            prefix = prefix.ToLower();
            var data = _unit.Repository<ProductModel>().GetAll().Where(x => x.Status == true);
            if (prid != null && prid > 0)
            {
                data = data.Where(x => x.ProductId == prid);
            }
            data = data
               .Where(x => x.ModelName.ToLower().Contains(prefix))
               .OrderBy(x => x.ModelName)
               .Take(10)
               .ToList();
            modelList = Mapper.Map<List<ProductModelViewModel>>(data);
            return modelList;
        }
        #endregion



        #region Product Group

        public List<ProductGroupViewModel> GetAllProductGroups()
        {
            List<ProductGroupViewModel> groups = new List<ProductGroupViewModel>();

            var data = _unit.Repository<ProductGroup>().GetAll();
            groups = Mapper.Map<List<ProductGroupViewModel>>(data);

            return groups;
        }




        #endregion

    }
}
