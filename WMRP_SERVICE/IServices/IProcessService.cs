﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMRP.Service.Dto;
using WMRP_DAL;
using WMRP.Helper;


namespace WMRP.Service.IServices
{
    public interface IProcessService
    {
        List<ModelProcessViewModel> GetAllProcess(long? modelid);

        ResponseMessage AddProcess(List<ModelProcessViewModel> viewmodels);
        ResponseMessage DeleteById(long? id);

        ModelProcessViewModel GetProcess(long? id);

        List<ProcessViewModel> GetAllBasicProcess();
    }
}
