﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WMRP.Service;
using WMRP.Helper;
using WMRP.Service.Dto;
using WMRP.Service.Services;
using WMRP_DAL;

namespace WMRP.Service.IServices
{
    public interface IProductPartService
    {
        List<ProductPartViewModel> GetAllProductParts();

        List<CommercialReportViewModel> GetCommercialReport(List<ProductPart> productParts);

        ResponseMessage AddProductPart(List<ProductPartViewModel> productPartViewModels);

        ProductPartViewModel GetProductPart (long? id);

        ResponseMessage DeleteProductPart(long id);

        List<ManufacturerViewModel> GetAllManufacturers();

        List<ProductPartViewModel> GetAllProductPartsByModel(long id);

        void AddBOMFile(HttpPostedFileBase bomFile);

        List<ProductPartViewModel> GetAllChild(long? id);

        string UpdateBomFromOracle();
    }
}
