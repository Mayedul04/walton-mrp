﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WMRP_DAL;

namespace WMRP.Service.IServices
{
    public interface IAccountService
    {
        


        #region USERS

        int AddUser(User user);
        List<User> GetUsers();
        User GetUser(long id);
        List<User> GetUsersByInchargeId(string employeeId);

        #endregion

        #region ROLE

        int AddRole(Role model);
        List<Role> GetRoles();
        Role GetRole(long id);

        #endregion

        #region User
        User LoginAuthorize(string userName, string password);
        #endregion

        List<Permission> GetUserPermissions(string employeeId);
        string SavePermissions(List<string> permissionList, string employeeId);
    }
}