﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMRP.Service.Dto;
using WMRP_DAL;
using WMRP.Helper;
using WMRP.Service.Services;

namespace WMRP.Service.IServices
{
    public interface IProductModelService
    {
        List<ProductViewModel> GetAllProducts();

        List<ProductViewModel> GetAllProductByGroup(long?id);
        ProductViewModel GetProduct(long? id);

        ResponseMessage AddModel(ProductModelViewModel productmodelViewModel);
        ResponseMessage DeleteProductModel(long ?id);

        List<ProductModelViewModel> GetModelListForAutoFill(string prefix, long? prid);
        List<ProductModelViewModel> GetModels(long? id);

        List<ProductGroupViewModel> GetAllProductGroups();

        ProductModelViewModel GetModel(long? id);

    }
}
