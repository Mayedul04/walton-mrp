﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMRP.Service.Dto;

namespace WMRP.Service.IServices
{
    public interface IBasicServices
    {
        List<ManufacturerViewModel> GetAllManufacturer();
        List<ManufactureTypeViewModel> GetManufactureTypes();
        List<EnumDto> GetPartTypes();
        List<EnumDto> GetOrderStatusTypes();
        List<DateTime> HolidayList();
    }
}
