﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMRP.Helper;
using WMRP.Service.Dto;

namespace WMRP.IServices
{
    public interface IPackageService
    {
        //List<PackageDetailViewModel> GetProductModelsByPackage(long id);
        List<ProductModelViewModel> GetProductModelsByProduct(long id);
        ResponseMessage AddPackage(PackageConfigurationViewModel packageConfigurationViewModels);
        ResponseMessage Delete(long? id);

        List<PackageConfigurationViewModel> GetPackageListForAutoFill(string prefix);

        List<ProductViewModel> GetAllProducts();

        List<PackageDetailViewModel> GetDetailsByPackageId(long id);
        List<MOrderModelViewModel> GetPackageDetailsByMOId(long id, long? moid);

        List<PackageConfigurationViewModel> GetAllPackages();
    }
}
