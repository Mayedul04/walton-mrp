﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMRP.Service.Services;
using WMRP.Helper;
using WMRP.Service.Dto;

namespace WMRP.Service.IServices
{
    public interface IManufactureOrderService
    {
        ManufactureOrderViewModel GetManufactureOrder(long? id);
        List<ManufactureOrderViewModel> GetAllOrders();
        DashboardViewModel GetAllTypeOfStatus();
        ResponseMessage AddMOrder(ManufactureOrderViewModel viewmodel);
        List<MOrderRoutingViewModel> GetProductRoutings(long mid, DateTime sdate);
        List<MOrderBOMViewModel> GetMorderBOM(long mid, int qty, DateTime expstartdate, long? moid);
        ManufactureOrderViewModel GetManufacturOrder(long? id);
        List<ManufactureOrderViewModel> GetAllOrders(int? status);
        List<DashboardViewModel> GetWorkstationCapacity();
        List<DashboardViewModel> GetModelWisePartsType();
        List<DashboardViewModel> GetAllModelBomStock();

    }
}
