﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMRP.Helper;
using WMRP.Service.Dto;
using WMRP.Service.Services;

namespace WMRP.Service.IServices
{
    public interface IWorkStationService
    {
        List<WorkStationViewModel> GetAllWorkStation();

        ResponseMessage AddWorkStation(WorkStationViewModel workStataionViewModel);

        WorkStationViewModel GetWorkStation(long? id);

        ResponseMessage DeleteWorkStation(long id);
        List<WorkStationDetailViewModel> GetModelStationProcess(long modelid);
        List<WorkStationDetailViewModel> GetAllWorkStationDetails(long modelid);
        ResponseMessage AddWorkStationDetails(List<WorkStationDetailViewModel> viewmodels);
    }
}
