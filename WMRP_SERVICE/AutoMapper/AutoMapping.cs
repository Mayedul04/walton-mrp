﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using WMRP_DAL;
using WMRP.Service.Dto;
using WMRP.HELPER;
using WMRP.Helper;
using WMRP.Service;

namespace WMRP.Service.AutoMapper
{
    public class AutoMapping : Profile
    {
        protected override void Configure()
        {
            base.Configure();

            CreateMap<BOMStock, BOMStockViewModel>()
                .ForMember(d => d.ProductPartName, o => o.MapFrom(s => s.PartsID > 0 ? s.ProductPart.ChineseName : ""));
            CreateMap<BOMStockViewModel, BOMStock>();

            CreateMap<BOMStockLog, BOMStockLogViewModel>();
            CreateMap<BOMStockLogViewModel, BOMStockLog>();

            CreateMap<CommercialOrderType, CommercialOrderTypeViewModel>();
            CreateMap<CommercialOrderTypeViewModel, CommercialOrderType>();


            CreateMap<ManufactureType, ManufactureTypeViewModel>()
                .ForMember(d => d.ManufactureOrders, o => o.Ignore());
            CreateMap<ManufactureTypeViewModel, ManufactureType>();

            CreateMap<ProductGroup, ProductGroupViewModel>()
                .ForMember(d => d.Products, o => o.Ignore());
            CreateMap<ProductGroupViewModel, ProductGroup>()
                   .ForMember(d => d.Products, o => o.Ignore());

            CreateMap<Product, ProductViewModel>()
             .ForMember(d => d.IsMainProductOfGroup, o => o.MapFrom(s => s.IsMainProductOfGroup == null ? false : s.IsMainProductOfGroup))
             .ForMember(d => d.ProductGroupName, o => o.MapFrom(s => s.GroupId > 0 ? s.ProductGroup.Name : ""))
             .ForMember(d => d.ProductModels, o => o.Ignore());
            CreateMap<ProductViewModel, Product>();

            //product model
            CreateMap<ProductModel, ProductModelViewModel>()
                .ForMember(d => d.ProductGroupId, o => o.MapFrom(s => s.ProductId > 0 ? s.Product.GroupId : 0))
                .ForMember(d => d.ProductName, o => o.MapFrom(s => s.ProductId > 0 ? s.Product.Name : ""))
                .ForMember(d => d.ModelProcesses, o => o.Ignore())
                .ForMember(d => d.ProductParts, o => o.Ignore())
                .ForMember(d => d.PackageDetails, o => o.Ignore());
            CreateMap<ProductModelViewModel, ProductModel>()
                .ForMember(d => d.ModelProcesses, o => o.Ignore())
                .ForMember(d => d.ProductParts, o => o.Ignore())
                .ForMember(d => d.PackageDetails, o => o.Ignore());


            CreateMap<Process, ProcessViewModel>()
                 .ForMember(d => d.ProcessLevelName, o => o.MapFrom(s => s.ProcessLevel > 0 ? EnumDisplayName.GetDisplayName((ProcessLevel)s.ProcessLevel) : ""))
                 .ForMember(d => d.ModelProcesses, o => o.Ignore());
            CreateMap<ProcessViewModel, Process>()
                 .ForMember(d => d.ModelProcesses, o => o.Ignore());

            CreateMap<ModelProcess, ModelProcessViewModel>()
                .ForMember(d => d.ProcessName, o => o.MapFrom(s => s.ProcessID > 0 ? s.Process.Name : ""))
                .ForMember(d => d.IsMainProcess, o => o.MapFrom(s => s.IsMainProcess != null ? s.IsMainProcess : false))
                .ForMember(d => d.ProductPartName, o => o.MapFrom(s => s.PartsID > 0 ? s.ProductPart.ChineseName : ""))
                .ForMember(d => d.Sequence, o => o.MapFrom(s => s.Sequence > 0 ? s.Sequence : 1))
                .ForMember(d => d.ProductModelName, o => o.MapFrom(s => s.ProductModelID > 0 ? s.ProductModel.ModelName : ""))
                .ForMember(d => d.ProductionLogs, o => o.Ignore())
                .ForMember(d => d.WorkStationDetails, o => o.Ignore());
            CreateMap<ModelProcessViewModel, ModelProcess>()
                .ForMember(d => d.ProductionLogs, o => o.Ignore())
                .ForMember(d => d.WorkStationDetails, o => o.Ignore());


            CreateMap<Manufacturer, ManufacturerViewModel>();
            CreateMap<ManufacturerViewModel, Manufacturer>()
                .ForMember(d => d.ProductParts, o => o.Ignore());

            CreateMap<ProductPart, ProductPartViewModel>()
                .ForMember(d => d.ManufacturerName, o => o.MapFrom(s => s.ManufacturerId > 0 ? s.Manufacturer.Name : ""))
                .ForMember(d => d.PartsParentName, o => o.MapFrom(s => s.ParentID > 0 ? s.ProductPart1.ChineseName : ""))
                .ForMember(d => d.ProductModelName, o => o.MapFrom(s => s.ProductModelID > 0 ? s.ProductModel.ModelName : ""))
                .ForMember(d => d.InventoryItemId, o => o.MapFrom(s => s.InventoryItemId > 0 ? s.InventoryItemId : 0))
                .ForMember(d => d.PartsTypeName, o => o.MapFrom(s => s.PartsType > 0 ? EnumDisplayName.GetDisplayName((PartsType)s.PartsType) : ""))
                .ForMember(d => d.BOMStocks, o => o.Ignore())
                .ForMember(d => d.MOrderBOMs, o => o.Ignore())
                .ForMember(d => d.ModelProcesses, o => o.Ignore())
                .ForMember(d => d.ProductParts1, o => o.Ignore());
            CreateMap<ProductPartViewModel, ProductPart>()
                .ForMember(d => d.BOMStocks, o => o.Ignore())
                .ForMember(d => d.MOrderBOMs, o => o.Ignore())
                .ForMember(d => d.ModelProcesses, o => o.Ignore())
                .ForMember(d => d.ProductParts1, o => o.Ignore());


            //CreateMap<ProductPart, CommercialReportViewModel>()
            //    .ForMember(d => d.ManufacturerName, o => o.MapFrom(s => s.ManufacturerId > 0 ? s.Manufacturer.Name : ""))
            //    .ForMember(d => d.ProductModelName, o => o.MapFrom(s => s.ProductModelID > 0 ? s.ProductModel.ModelName : ""))
            //    .ForMember(d => d.ProductModelName, o => o.MapFrom(s => s.ProductModelID > 0 ? s.ProductModel.ModelName : ""));
            //CreateMap<CommercialReportViewModel, ProductPart>();
               

            CreateMap<WorkSatation, WorkStationViewModel>();
            CreateMap<WorkStationViewModel, WorkSatation>()
                .ForMember(d => d.MOrderRoutings, o => o.Ignore())
                .ForMember(d => d.WorkStationDetails, o => o.Ignore());

            CreateMap<WorkStationDetail, WorkStationDetailViewModel>()
                .ForMember(d => d.WorkSatationName, o => o.MapFrom(s => s.WorkStationID > 0 ? s.WorkSatation.Name : ""))
                .ForMember(d => d.ModelProcessName, o => o.MapFrom(s => s.ModelProcessID > 0 ? s.ModelProcess.Process.Name : ""))
                .ForMember(d => d.WorkSatationCapacityType, o => o.MapFrom(s => s.ModelProcessID > 0 ? EnumDisplayName.GetDisplayName((CapacityType) s.WorkSatation.CapacityType ) : ""));

            CreateMap<WorkStationDetailViewModel, WorkStationDetail>();

            CreateMap<ManufactureOrder, ManufactureOrderViewModel>()
                .ForMember(d => d.ProductModelID, o => o.MapFrom(s => s.ManufactureType == 2 ? s.MOrderModels.FirstOrDefault().ProductModelID : 0))
                .ForMember(d => d.ProductModelName, o => o.MapFrom(s => s.ManufactureType == 2 ? s.MOrderModels.FirstOrDefault().ProductModel.ModelName : ""))
                .ForMember(d => d.ProductID, o => o.MapFrom(s => s.ManufactureType == 2 ? s.MOrderModels.FirstOrDefault().ProductModel.ProductId : null))
                .ForMember(d => d.PackageID, o => o.MapFrom(s => s.ManufactureType == 1 ? s.MOrderModels.FirstOrDefault().PackageID : null))
                .ForMember(d => d.PackageName, o => o.MapFrom(s => s.ManufactureType == 1 ? s.MOrderModels.FirstOrDefault().PackageConfiguration.PackageName : ""))
                .ForMember(d => d.ExpectedStartDateText, o => o.MapFrom(s => s.ExpectedStartDate != null ? DateTime.Parse(s.ExpectedStartDate.ToString()).ToString("dd/MM/yyyy") : DateTime.Today.ToString("dd/MM/yyyy")))
                .ForMember(d => d.ExpectedEndDateText, o => o.MapFrom(s => s.ExpectedEndDate != null ? DateTime.Parse(s.ExpectedEndDate.ToString()).ToString("dd/MM/yyyy") : DateTime.Today.ToString("dd/MM/yyyy")))
                .ForMember(d => d.CalculatedStartDateText, o => o.MapFrom(s => s.CalculatedStartDate != null ? DateTime.Parse(s.CalculatedStartDate.ToString()).ToString("dd/MM/yyyy") : DateTime.Today.ToString("dd/MM/yyyy")))
                .ForMember(d => d.CalculatedCompletionDateText, o => o.MapFrom(s => s.CalculatedCompletionDate != null ? DateTime.Parse(s.CalculatedCompletionDate.ToString()).ToString("dd/MM/yyyy") : DateTime.Today.ToString("dd/MM/yyyy")))
                .ForMember(d => d.StatusName, o => o.MapFrom(s => s.Status > -1 ? EnumDisplayName.GetDisplayName((OrderStatus)s.Status) : ""))
                .ForMember(d => d.ColorCode, o => o.MapFrom(s => s.ID > 0 ? "f0f" : ""))
                .ForMember(d => d.BDColor, o => o.MapFrom(s => s.ID > 0 ? s.BGColor : null)) 
                .ForMember(d => d.ProductionLogs, o => o.Ignore())
                .ForMember(d => d.MOrderBOMs, o => o.Ignore())
                .ForMember(d => d.MOrderRoutings, o => o.Ignore());

            CreateMap<ManufactureOrderViewModel, ManufactureOrder>()
                 .ForMember(d => d.MOrderModels, o => o.Ignore())
                 .ForMember(d => d.ProductionLogs, o => o.Ignore())
                 .ForMember(d => d.MOrderBOMs, o => o.Ignore())
                 .ForMember(d => d.MOrderRoutings, o => o.Ignore());

            CreateMap<MOrderBOM, MOrderBOMViewModel>()
                .ForMember(d => d.ManufactureOrderTitle, o => o.MapFrom(s => s.MOId > 0 ? s.ManufactureOrder.Title : ""))
                .ForMember(d => d.PartsName, o => o.MapFrom(s => s.PartsID > 0 ? s.ProductPart.Name : ""))
                .ForMember(d => d.ParentID, o => o.MapFrom(s => s.PartsID > 0 ? s.ProductPart.ParentID : null))
                .ForMember(d => d.ParentName, o => o.MapFrom(s => s.PartsID > 0 ? s.ProductPart.ProductPart1.Name : ""))
                .ForMember(d => d.RequiredQty, o => o.MapFrom(s => s.DraftedQty > 0 ? s.DraftedQty : s.SubmittedQty))
                .ForMember(d => d.PartsCode, o => o.MapFrom(s => s.PartsID > 0 ? s.ProductPart.YwxPartNo : ""))
                .ForMember(d => d.Dimension, o => o.MapFrom(s => s.PartsID > 0 ? s.ProductPart.Description : ""))
                .ForMember(d => d.RequiredQty, o => o.MapFrom(s => s.PartsID > 0 ? s.ProductPart.RequiredQty : 1))
                .ForMember(d => d.ManufacturerName, o => o.MapFrom(s => s.ManfacturerId > 0 ? s.ProductPart.Manufacturer.Name : ""))
                .ForMember(d => d.ExpectedProductionStartDateText, o => o.MapFrom(s => s.ExpectedProductionStartDate != null ? DateTime.Parse(s.ExpectedProductionStartDate.ToString()).ToString("dd/MM/yyyy") : DateTime.Today.ToString("dd/MM/yyyy")));
            CreateMap<MOrderBOMViewModel, MOrderBOM>();

            CreateMap<MOrderRouting, MOrderRoutingViewModel>()
                // .ForMember(d => d.ManufactureOrderTitle, o => o.MapFrom(s => s.MOId > 0 ? s.ManufactureOrder.Title : ""))
                // .ForMember(d => d.ProcessName, o => o.MapFrom(s => s.ProcessId > 0 ? s.WorkSatation.Process.Name : ""))
                //.ForMember(d => d.WorkStationName, o => o.MapFrom(s => s.WorkStationId > 0 ? s.WorkSatation.Name : ""))
                //.ForMember(d => d.Capacity, o => o.MapFrom(s => s.WorkStationId > 0 ? s.WorkSatation : 1))
                //.ForMember(d => d.Sequence, o => o.MapFrom(s => s.WorkStationId > 0 ? s.WorkSatation.Process.Sequence : 0))
                //.ForMember(d => d.OldModelChangeOvertime, o => o.MapFrom(s => s.WorkStationId > 0 ? s.WorkSatation.Process.OldModelChangeOvertime : 0))
                //.ForMember(d => d.NewModelChangeOvertime, o => o.MapFrom(s => s.WorkStationId > 0 ? s.WorkSatation.Process.NewModelChangeOvertime : 0))
                .ForMember(d => d.ExpectedStartDateText, o => o.MapFrom(s => s.ExpectedStartDate != null ? DateTime.Parse(s.ExpectedStartDate.ToString()).ToShortDateString() : DateTime.Today.ToShortDateString()))
                .ForMember(d => d.ExpectedEndDateText, o => o.MapFrom(s => s.ExpectedStartDate != null ? DateTime.Parse(s.ExpectedStartDate.ToString()).AddDays(+(int)(s.ExpectedFinishingTime / 8)).ToShortDateString() : DateTime.Today.ToShortDateString()));
            CreateMap<MOrderRoutingViewModel, MOrderRouting>();

            CreateMap<OrderMaster, OrderMasterViewModel>()
                .ForMember(d => d.OrderDetails, o => o.Ignore());
            CreateMap<OrderMasterViewModel, OrderMaster>()
                .ForMember(d => d.OrderDetails, o => o.Ignore());

            CreateMap<OrderDetail, OrderDetailViewModel>()
                .ForMember(d => d.OrderMaster, o => o.MapFrom(s => s.OrderMasterID > 0 ? s.OrderMaster.Title : ""));
            CreateMap<OrderDetailViewModel, OrderDetail>();

            CreateMap<PackageConfiguration, PackageConfigurationViewModel>()
                .ForMember(d => d.MOrderModels, o => o.Ignore())
                .ForMember(d => d.PackageDetails, o => o.Ignore());
            CreateMap<PackageConfigurationViewModel, PackageConfiguration>()
                .ForMember(d => d.MOrderModels, o => o.Ignore())
                .ForMember(d => d.PackageDetails, o => o.Ignore());

            CreateMap<PackageDetail, PackageDetailViewModel>()
                .ForMember(d => d.PackageConfigurationName, o => o.MapFrom(s => s.PackageId > 0 ? s.PackageConfiguration.PackageName : ""))
                .ForMember(d => d.ProductModelName, o => o.MapFrom(s => s.ProductModelID > 0 ? (s.ProductModel.ModelName + " (" + s.ProductModel.Product.Name + ")") : ""))
                .ForMember(d => d.ProductID, o => o.MapFrom(s => s.ProductModelID > 0 ? s.ProductModel.ProductId : 0))
                .ForMember(d => d.Models, o => o.Ignore());
            CreateMap<PackageDetailViewModel, PackageDetail>();

            CreateMap<ProductionLog, ProductionLogViewModel>()
                .ForMember(d => d.ManufactureOrderName, o => o.MapFrom(s => s.ManufactureOrderID > 0 ? s.ManufactureOrder.Title : ""))
                .ForMember(d => d.ProcessName, o => o.MapFrom(s => s.ModelProcessID > 0 ? s.ModelProcess.Process.Name : ""))
                .ForMember(d => d.ProductionWastages, o => o.Ignore());
            CreateMap<ProductionLogViewModel, ProductionLog>()
                .ForMember(d => d.ProductionWastages, o => o.Ignore());

            CreateMap<ProductionWastage, ProductionWastageViewModel>()
                .ForMember(d => d.ProductionLogName, o => o.MapFrom(s => s.ProductionLogID > 0 ? s.ProductionLog.ManufactureOrder.Title : ""));
            CreateMap<ProductionWastageViewModel, ProductionWastage>();

            CreateMap<MOrderModel, MOrderModelViewModel>()
                .ForMember(d => d.ManufactureOrderName, o => o.MapFrom(s => s.MOId > 0 ? s.ManufactureOrder.Title : ""))
                .ForMember(d => d.PackageConfigurationName, o => o.MapFrom(s => s.PackageID > 0 ? s.PackageConfiguration.PackageName : ""))
                .ForMember(d => d.ProductModelName, o => o.MapFrom(s => s.ProductModelID > 0 ? (s.ProductModel.ModelName + " (" + s.ProductModel.Product.Name + ")") : ""))
                .ForMember(d => d.ProductId, o => o.MapFrom(s => s.ProductModelID > 0 ? s.ProductModel.ProductId : 0));
            CreateMap<MOrderModelViewModel, MOrderModel>();
        }
    }
}