﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace WMRP.App_Start.AutoMapperBootstrapper
{
    public class AutoMapperBootstrapper
    {
        public static void BootStrapAutoMaps()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new WMRP.Service.AutoMapper.AutoMapping());
            });
        }

        
    }
}