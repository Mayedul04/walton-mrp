﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WMRP.Infrastructures.IRepositories;
using WMRP.Infrastructures.Repositories;
using WMRP.IServices;
using WMRP.Service.IServices;
using System.Reflection;
using WMRP.App_Start.AutoMapperBootstrapper;
using System.Web.Http;
using WMRP.Service.Services;
using WMRP.Services;


namespace WMRP
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            DependencyInjection();
            AutoMapperBootstrapper.BootStrapAutoMaps();
            
        }
        private void DependencyInjection()
        {
            Container container = new Container();
            container.Options.DefaultLifestyle = new WebRequestLifestyle();
            container.Register<IUnit, Unit>(Lifestyle.Transient);
            container.Register<IHomeService, HomeService>(Lifestyle.Transient);
            container.Register<IAccountService, AccountService>(Lifestyle.Transient);
             container.Register<IBasicServices, BasicService>(Lifestyle.Transient);
            container.Register<IProductModelService, ProductModelService>(Lifestyle.Transient);

            container.Register<IProcessService, MRPProcessService>(Lifestyle.Transient);


            container.Register<IProductPartService, ProductPartService>(Lifestyle.Transient);

            container.Register<IWorkStationService, WorkStationService>(Lifestyle.Transient);
            container.Register<IManufactureOrderService,ManufactureOrderService>(Lifestyle.Transient);

            container.Register<IPackageService, PackageService>(Lifestyle.Transient);
           

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

        }

        
    }
}
