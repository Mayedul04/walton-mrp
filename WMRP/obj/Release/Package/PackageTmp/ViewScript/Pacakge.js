var app = angular.module("WMRP", []);
app.controller("Packagectrl", function ($scope, $http, $location, AutoSearch, $filter) {
    //debugger;
    $scope.PackageNames = {};
    $scope.PackageConfiguration = {};
    $scope.Packages = [{ Model: "Hello" }];
    $scope.Packages.pop();

    //var host = 'http://' + $location.host();
    //var port = $location.port();
    //if (port)
    //    baseurl = host + ':' + port;
    //else
    //    baseurl = host;
    
    var baseurl;
    var host = '';
    var port = $location.port();
    var hostInfo = $location.host();
    if (hostInfo === "localhost") {
        host = 'http://' + hostInfo + ':' + port;
    } else {
        host = 'http://' + hostInfo + '/WMRP';
    }
    baseurl = host;

    $scope.GetAllLoadedData = function () {
        $scope.LoadProductData();
    };

    $scope.AddNewPackage = function () {
        $scope.IsDuplicate = false;
        $scope.PackageDto = {};
        $scope.PackageDto.PackageConfigurationName = $scope.PackageName;
        $scope.PackageDto.ProductID = null;
        
        $.each($scope.Packages, function (key, value) {
            if ($scope.PackageDto.ProductID == value.ProductID) {
                $scope.IsDuplicate = true;
            } else {
                $scope.IsDuplicate = false;
            }

        });

        if ($scope.IsDuplicate == false) {
            $scope.Packages.push($scope.PackageDto);
            //$scope.Reset();
        } else {
            $('#successModal').modal('show');
            $('#successModalText').text("This row already exist!");
            //$scope.Reset();
        }
    };

    $scope.SaveData = function () {
        //debugger
        $scope.PackageConfiguration.ID = document.getElementById("pacakgeId_").value;
        $scope.PackageConfiguration.PackageName = $scope.PackageName;
        $.each($scope.Packages, function (key, value) {
            var packageId = $scope.PackageNames[$scope.PackageName];
            $scope.Packages[key].PackageId = packageId;
            $scope.Packages[key].Models = {};
        });
        $scope.PackageConfiguration.PackageDetails = $scope.Packages;

        $http({
            method: "post",
            url: baseurl + "/Package/InsertData",
            datatype: "json",
            data: JSON.stringify($scope.PackageConfiguration)
        }).then(function (response) {
            $scope.Reset();
            $('#successModal').modal('show');
            $('#successModalText').text(response.data.Message);

        });
    };

    $scope.Reset = function () {
        document.getElementById("pacakgeId_").value = 0;
        document.getElementById("pacakgeDetailsId_").value = 0;
        $scope.PackageName = "";
        $scope.Packages = [{}];
    }

    $scope.RemovePackageDetails = function (item) {
        
        if (item.ID > 0) {
            document.getElementById("pacakgeDetailsId_").value = item.ID;
        } else
            $scope.Packages.splice($scope.Packages.indexOf(item), 1);
    };
    $scope.Delete = function () {
        var id = document.getElementById("pacakgeDetailsId_").value;
        if (id > 0) {
            $http({
                method: "post",
                url: baseurl + "/Package/DeletePackage?id=" + id,
                datatype: "json"
            }).then(function (response) {
                
                $scope.Packages.splice(0);
                $scope.GetConfigByPackage();
                $('#successModal').modal('show');
                $('#successModalText').text(response.data.Message);
                
            })
        }
    };
    
    $scope.GetConfigByPackage = function () {
        //debugger;
        if ($scope.PackageNames[$scope.PackageName] > 0) {
            var packageId = $scope.PackageNames[$scope.PackageName];
            var el = angular.element('#selectedmodel');
            el.blur();
            document.getElementById("pacakgeId_").value = packageId;
            $http({
                method: "get",
                url: baseurl + "/Package/GetProductDetailsByPackageId?id=" + packageId
            }).then(function (response) {
                $scope.Packages.splice(0);
                $.each(response.data, function (key, value) {
                    $scope.PackageDto = {};
                    $scope.PackageDto.ID = value.ID;
                    $scope.PackageDto.PackageId = value.PackageId;
                    $scope.PackageDto.ProductID = value.ProductID;
                    $.when($scope.LoadModelData(key, value.ProductID)).done(function () {
                        $scope.PackageDto.ProductModelID = value.ProductModelID;
                    });
                    $scope.PackageDto.Quantity = value.Quantity;
                    $scope.PackageDto.PackageConfigurationName = $scope.PackageName;
                    $scope.Packages.push($scope.PackageDto);
                });
            }, function () {
                alert("Error Occur");
            });
        } else {
            $scope.Packages.splice(0);
        }
    };
    
    $scope.LoadProductData = function () {
        $http({
            method: "get",
            url: baseurl + "/ProductModel/GetAllProductByGroup?id=" + 2
        }).then(function (response) {
                        $scope.Products = response.data;
        }, function () {
            alert("Error Occur");
        })
    };

    $scope.LoadModelData = function (index, productId) {
        
        
        $http({
            method: "get",
            url: baseurl + "/Package/GetAllModelByProduct?id=" + productId
        }).then(function (response) {
            
            $scope.Packages[index].Models = response.data;
            
        }, function () {
            alert("Error Occur");
        })
    };

    ///AUTO Complete
    $scope.selectedItems = null;

    $scope.searchItem = function (term) {
        AutoSearch.searchItem(term).then(function (items) {
            $scope.PackageNames = items;
            //console.log(JSON.stringify(items));
        });
    };
    //////////////

});
app.directive('keyboardPoster', function ($parse, $timeout) {
    var DELAY_TIME_BEFORE_POSTING = 0;
    return function (scope, elem, attrs) {

        var element = angular.element(elem)[0];
        var currentTimeout = null;

        element.oninput = function () {
            var model = $parse(attrs.postFunction);
            var poster = model(scope);
            if (currentTimeout) {
                $timeout.cancel(currentTimeout)
            }
            currentTimeout = $timeout(function () {
                poster(angular.element(element).val());
            }, DELAY_TIME_BEFORE_POSTING)
        };
    }
});

app.service('AutoSearch', function ($q, $http, $location) {

    this.searchItem = function (item) {
        //var host = 'http://' + $location.host();
        //var port = $location.port();
        //if (port)
        //    baseurl = host + ':' + port;
        //else
        //    baseurl = host;
        
        var baseurl;
        var host = '';
        var port = $location.port();
        var hostInfo = $location.host();
        if (hostInfo === "localhost") {
            host = 'http://' + hostInfo + ':' + port;
        } else {
            host = 'http://' + hostInfo + '/WMRP';
        }
        baseurl = host;

        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: baseurl + '/Package/GetAllPackagesForAutoFill?prefix=' + item
        }).then(function (items) {
            var _items = {};
            var items = items.data;

            for (var i = 0, len = items.length; i < len; i++) {
                _items[items[i].PackageName] = items[i].ID;
                //_items[items[i]] = items[i];
                //_items[items[i].name] = items[i].name;
                //console.log(_items[items[i].name]);
            }
            deferred.resolve(_items);

        }, function () {
            deferred.reject(arguments);
        });
        return deferred.promise;
    };
})