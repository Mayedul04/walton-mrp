﻿//angular.module("WMRP", []);


//var WMRP = angular.module("WMRP", ['ui.router', 'angularUtils.directives.dirPagination']);


var app = angular.module("WMRP", []);


app.controller("ProductModelCtrl", function ($scope, $http, $location, $q) {
    //var host = 'http://' + $location.host();
    //var port = $location.port();
    //if (port)
    //    baseurl = host + ':' + port;
    //else
    //    baseurl = host;
    var baseurl;
    var host = '';
    var port = $location.port();
    var hostInfo = $location.host();
    if (hostInfo === "localhost") {
        host = 'http://' + hostInfo + ':' + port;
    } else {
        host = 'http://' + hostInfo + '/WMRP';
    }
    baseurl = host;
    
    $scope.GetAllLoadedData = function () {
        var models = document.location.href.split("/");
        var id = parseInt(models[models.length - 1]);
        if (id > 0) {
            $scope.UpdateData(id);
        }
        else
            $scope.LoadProductGroupData();

    };

    $scope.LoadProductGroupData = function () {
        
        $http({
            method: "get",
            url: baseurl + "/ProductModel/GetAllProductGroups"
        }).then(function (response) {
            //console.log(JSON.stringify(response.data));
            //console.log(response.data);
            debugger;
            $scope.Groups = response.data;
            //var parts = document.location.href.split("/");
            //var id = parts[parts.length - 1];
            //if (parts.length > 5) {
            //    $scope.UpdateData(id);
            //}

        }, function () {
            alert("Error Occur");
        })
    };

    $scope.LoadProductData = function () {

        $http({
            method: "get",
            url: baseurl + "/ProductModel/GetAllProductByGroup?id=" + $scope.ProductGroupId
        }).then(function (response) {
            //console.log(JSON.stringify(response.data));
            //console.log(response.data);
            $scope.Products = response.data;
            //var parts = document.location.href.split("/");
            //var id = parts[parts.length - 1];
            //if (parts.length > 5) {
            //    $scope.UpdateData(id);
            //}

        }, function () {
            alert("Error Occur");
        })
    };

    $scope.InsertData = function () {
        debugger
        $scope.ModelDto = {};
        $scope.ModelDto.ID = document.getElementById("ModelID_").value;
        $scope.ModelDto.ModelName = $scope.Name;
        $scope.ModelDto.ProductId = $scope.ProductId;
        //$scope.ModelDto.GroupId = $scope.ProductGroupId;

        $http({
            method: "post",
            url: baseurl + "/ProductModel/InsertData",
            datatype: "json",
            data: JSON.stringify($scope.ModelDto)
        }).then(function (response) {

            $('#successModal').modal('show');
            $('#successModalText').text(response.data.Message);
            $scope.Reset();

        })

    };
    $scope.Reset = function () {
        $scope.ID = 0;
        $scope.Name = "";
        $scope.ProductId = 0;
        $scope.ProductGroupId = 0;
    };

    $scope.UpdateData = function (id) {
        $http({
            method: "get",
            url: baseurl + "/ProductModel/GetModel?id=" + id
        }).then(function (dto) {
            //console.log(JSON.stringify(dto.data));
            document.getElementById("ModelID_").value = dto.data.ID;
            
            $.when($scope.LoadProductGroupData()).done(function () {
                $scope.ProductGroupId = dto.data.ProductGroupId;
                    console.log('Success:' + $scope.ProductGroupId);
            });

            $.when($scope.LoadProductData()).done(function () {
                $scope.ProductId = dto.data.ProductId;
            });

            $scope.Name = dto.data.ModelName;


        }, function () {
            alert("Error Occur");
        })
    };

    $scope.Delete = function () {
        debugger
        var id = document.getElementById("hiddenModelId").value;
        $http({
            method: "post",
            url: baseurl + "/ProductModel/DeleteModel?id=" + id,
            datatype: "json"
        }).then(function (response) {
            $('#successModal').modal('show');
            $('#successModalText').text(response.data.Message);
            $scope.LoadAllModel();
        })
    };
    $scope.SetId = function (id) {

        document.getElementById("hiddenModelId").value = id;
    }

    $scope.LoadAllModel = function () {
        debugger;
        $http({
            method: "get",
            url: baseurl + "/ProductModel/GetAllModel"
        }).then(function (response) {
            console.log(JSON.stringify(response.data));
            //console.log(response.data);
            debugger;
            $scope.Models = response.data;
            //var parts = document.location.href.split("/");
            //var id = parts[parts.length - 1];
            //if (parts.length > 5) {
            //    $scope.UpdateData(id);
            //}
            
            //angular.element(document).ready(function () {
            //    dTable = $('#user_table');
            //    dTable.DataTable();
            //});

        }, function () {
            alert("Error Occur");
        })

    };
})