﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WMRP
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // Your specialized route
            //routes.MapRoute(
            //    name: "Page",
            //    url:  "{controller}/{action}"
            //);
           
               // defaults: new { controller = "controller", action = "index" }
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            );
            // Everything else will hit Home/Index which serves up the root angular app page
            //routes.MapRoute(
            //    name: "ngDefault",
            //    url: "{*anything}", // THIS IS THE MAGIC!!!!
            //    defaults: new { controller = "Home", action = "Index" }
            //);
        }
    }
}
