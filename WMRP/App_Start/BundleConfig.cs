﻿using System.Web;
using System.Web.Optimization;

namespace WMRP
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                    "~/Scripts/jquery-{version}.js",
                    "~/Scripts/jquery-migrate-1.0.0.js"));

            bundles.Add(new ScriptBundle("~/bundles/amChartJS").Include(
                    "~/Scripts/am_core.js",    
                    "~/Scripts/am_charts.js",                    
                    "~/Scripts/am_animated.js"
                   ));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));


            //_CustomLayout.JS

            bundles.Add(new ScriptBundle("~/bundles/coreJS").Include(
                      "~/assets/global/plugins/jquery.min.js",
                       "~/assets/global/plugins/bootstrap/js/bootstrap.min.js",
                       "~/assets/global/plugins/js.cookie.min.js",
                       "~/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js",
                       "~/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js",
                       "~/assets/global/plugins/jquery.blockui.min.js",
                       "~/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js",
                       "~/assets/global/plugins/select2/js/select2.full.min.js",
                       "~/assets/global/scripts/app.min.js",
                       "~/assets/pages/scripts/components-select2.min.js",
                       "~/assets/layouts/layout4/scripts/layout.min.js",
                       "~/assets/layouts/layout4/scripts/demo.min.js",
                       "~/assets/layouts/global/scripts/quick-sidebar.min.js"
                       //"~/assets/global/plugins/jquery-validation/js/jquery.validate.min.js",
                       //"~/assets/pages/scripts/form-validation.min.js"

               ));
            bundles.Add(new ScriptBundle("~/bundles/dateTimeJS").Include(
                      "~/assets/global/plugins/moment.min.js",
                       "~/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js",
                       "~/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js",
                       "~/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js",
                       "~/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js",
                       "~/assets/global/plugins/clockface/js/clockface.js",
                        "~/assets/pages/scripts/components-date-time-pickers.min.js",
                       "~/assets/global/plugins/fullcalendar/fullcalendar.min.js"
               ));
            bundles.Add(new ScriptBundle("~/bundles/tableJS").Include(
                      "~/assets/global/scripts/datatable.js",
                       "~/assets/global/plugins/datatables/datatables.min.js",
                       "~/assets/pages/scripts/table-datatables-responsive.min.js",
                       "~/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js",
                       "~/assets/pages/scripts/table-datatables-editable.min.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/chartJS").Include(
                    "~/assets/global/plugins/morris/morris.min.js",
                    "~/assets/global/plugins/amcharts/amcharts/amcharts.js",
                    "~/assets/global/plugins/amcharts/amcharts/serial.js",
                    "~/assets/pages/scripts/dashboard.min.js",
                    "~/assets/global/plugins/flot/jquery.flot.min.js",
                    "~/assets/global/plugins/flot/jquery.flot.categories.min.js",
                    "~/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js",
                    "~/assets/global/plugins/jquery.sparkline.min.js"
                
              ));


            //_CustomLayout.CSS

            bundles.Add(new StyleBundle("~/bundles/coreCss").Include(
                "~/assets/global/plugins/bootstrap/css/bootstrap.min.css", 
                "~/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css",
                "~/assets/global/plugins/simple-line-icons/simple-line-icons.min.css",
                "~/assets/global/css/components.min.css",
                "~/assets/global/css/plugins.min.css",
                "~/assets/layouts/layout4/css/layout.min.css",
                "~/assets/layouts/layout4/css/themes/light.min.css",
                "~/assets/layouts/layout4/css/custom.min.css",
                "~/assets/global/plugins/morris/morris.css",
                "~/assets/global/plugins/jqvmap/jqvmap/jqvmap.css",
                "~/assets/global/plugins/select2/css/select2.min.css",
                "~/assets/global/plugins/select2/css/select2-bootstrap.min.css",
                "~/assets/global/plugins/fancybox/source/jquery.fancybox.css",
                "~/assets/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css" 
               ));
            bundles.Add(new StyleBundle("~/bundles/dateCss").Include(
                "~/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css",
                "~/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css",
                "~/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css",
                "~/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css",
                "~/assets/global/plugins/clockface/css/clockface.css"

               ));

            bundles.Add(new StyleBundle("~/bundles/tableCss").Include(
               "~/assets/global/plugins/datatables/datatables.min.css",
               "~/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css"

              ));

            bundles.Add(new StyleBundle("~/bundles/calenderCss").Include(
              "~/assets/global/plugins/fullcalendar/fullcalendar.min.css" 

             ));

            bundles.Add(new StyleBundle("~/bundles/fileUploadCss").Include(
            "~/assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css",
            "~/assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css"

            ));

           
        }
    }
}
