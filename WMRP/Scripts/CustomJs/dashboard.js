﻿function generate_gauge_for_all_kpi() {
    var url = 'OverAllWeightedScore'; //@Url.Action("OverAllWeightedScore","Home");
    $.post(url, {condition:"team"}, function (result) {
        Highcharts.chart('container', {

            chart: {
                type: 'gauge',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false,
                title: false,
                height: 300,
                width: 300
            },
            exporting: { enabled: false },
            title: {
                text: 'Team KPI'
            },
            
            pane: {
                startAngle: -150,
                endAngle: 150,
                background: [{
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, '#FFF'],
                            [1, '#333']
                        ]
                    },
                    borderWidth: 0,
                    outerRadius: '109%'
                }, {
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, '#333'],
                            [1, '#FFF']
                        ]
                    },
                    borderWidth: 1,
                    outerRadius: '107%'
                }, {
                    // default background
                }, {
                    backgroundColor: '#DDD',
                    borderWidth: 0,
                    outerRadius: '105%',
                    innerRadius: '103%'
                }]
            },

            // the value axis
            yAxis: {
                min: 0,
                max: 100,

                minorTickInterval: 'auto',
                minorTickWidth: 1,
                minorTickLength: 10,
                minorTickPosition: 'inside',
                minorTickColor: '#666',

                tickPixelInterval: 30,
                tickWidth: 2,
                tickPosition: 'inside',
                tickLength: 10,
                tickColor: '#666',
                labels: {
                    step: 2,
                    rotation: 'auto'
                },
                title: {
                    text: 'kpi'
                },
                plotBands: [
                {
                    from: 0,
                    to: 33,
                    color: '#f44941', //red 
                    PlotLine: 'Bad'
                }, {
                    from: 33,
                    to: 60,
                    color: '#f49d41' // orange
                },
                {
                    from: 60,
                    to: 80,
                    color: '#f4d641' // green
                }, {
                    from: 80,
                    to: 100,
                    color: '#61f441' // green
                }]
            },

            series: [{
                name: 'Average Weighted Score',
                data: [result],
                tooltip: {
                    valueSuffix: ''
                }
            }]

        }, function (chart) {
            if (!chart.renderer.forExport) { }
        });
    });
    
}

function generate_gauge_for_my_kpi() {
    var url = 'OverAllWeightedScore'; //@Url.Action("OverAllWeightedScore","Home");
    $.post(url, {condition:"self"} ,function (result) {
        Highcharts.chart('container1', {

            chart: {
                type: 'gauge',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false,
                height: 300,
                width: 300
            },
            exporting: { enabled: false },
            title: {
                text: 'Self KPI'
            },

            pane: {
                startAngle: -150,
                endAngle: 150,
                background: [{
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, '#FFF'],
                            [1, '#333']
                        ]
                    },
                    borderWidth: 0,
                    outerRadius: '109%'
                }, {
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, '#333'],
                            [1, '#FFF']
                        ]
                    },
                    borderWidth: 1,
                    outerRadius: '107%'
                }, {
                    // default background
                }, {
                    backgroundColor: '#DDD',
                    borderWidth: 0,
                    outerRadius: '105%',
                    innerRadius: '103%'
                }]
            },

            // the value axis
            yAxis: {
                min: 0,
                max: 100,

                minorTickInterval: 'auto',
                minorTickWidth: 1,
                minorTickLength: 10,
                minorTickPosition: 'inside',
                minorTickColor: '#666',

                tickPixelInterval: 30,
                tickWidth: 2,
                tickPosition: 'inside',
                tickLength: 10,
                tickColor: '#666',
                labels: {
                    step: 2,
                    rotation: 'auto'
                },
                title: {
                    text: 'kpi'
                },
                plotBands: [
                {
                    from: 0,
                    to: 33,
                    color: '#f44941', //red 
                    PlotLine: 'Bad'
                }, {
                    from: 33,
                    to: 60,
                    color: '#f49d41' // orange
                },
                {
                    from: 60,
                    to: 80,
                    color: '#f4d641' // green
                }, {
                    from: 80,
                    to: 100,
                    color: '#61f441' // green
                }]
            },

            series: [{
                name: 'Average Weighted Score',
                data: [result]
                //tooltip: {
                //    valueSuffix: ''
                //}
            }]

        }, function (chart) {
            if (!chart.renderer.forExport) { }
        });
    });
}

function generate_kpi_gauge_by_user(req_url) {
    var url = 'GetEmployeesKpiWithGauge';
    $.post(url, function (result) {
        console.log(result);
        var cc = 0;
        var html = '';
        debugger;
        //var testo = JSON.parse(result);
        var employees = [];
        for (var i in result) {

            var item = result[i];

            employees.push({
                "AssignedTo": item.AssignedTo,
                "EmployeeId": item.EmployeeId,
                "WeightedScore": item.WeightedScore
            });
        }
        complete_table_with_gauge(employees, req_url);
        //$.each(result, function (key, item) {
        //    cc = cc + 1;
        //    var tdId = 'gg_' + cc;
        //    var link = '@Html.ActionLink("Detail", "GetTrand", new { controller = "Home",kpiId = "--1"}, new { target = "_blank" })';
        //    link = link.replace('--1', item.KpiId);
        //    html += '<tr>';
        //    html += '<td>' + item.AssignedTo + '</td>';
        //    html += '<td>' + item.EmployeeId + '</td>';
        //    html += '<td data-gg="' + item.WeightedScore + '"><div id="' + tdId + '" style="min-width: 310px; max-width: 400px; height: 300px; margin: 0 auto"></div></td>';
        //    //html += '<td><div id="'+tdId+'" style="min-width: 310px; max-width: 400px; height: 300px; margin: 0 auto"></div></td>';
        //    html += '<td></td>';
        //    //html += '<td>' + item.TotalActual + '</td>';
        //    //html += '<td>' + arrow + textColor + ' ' + item.InPercent + '%</i></td>';
        //    //html += '<td><i class="fa fa-hourglass-half"></i></td>';
        //    //html += '<td data-sparkline="' + item.Trend + '"></td>';
        //    //html += '<td><i class="fa fa-bar-chart">' + link + '</i></td>';
        //    html += '</tr>';
        //});
        $('.kpi_gauge_by_user_body').html(html);

    });
}

function complete_table_with_gauge(data, req_url) {
    debugger;

    $.each(data, function (key, item) {
        var link = req_url;
        link = link.replace('--1', +item.EmployeeId);
        var row = '<tr class="active">' +
            '<td >' + item.AssignedTo + '</td> ' +
            '<td >' + item.EmployeeId + '</td>' +
            '<td style="max-width: 100px !important"><div id="chartdiv"> <div class="testchart" id="chartdiv' + key + '" ></div></div> </td>' +
            '<td>' + link + '</td>' +
            ' </tr>';

        $('#kpi_gauge_by_user_body').append(row);
        generate_gauge_chart(data[key], key);

    });

}



function generate_gauge_chart(data, key) {

    debugger;
    //chart
    var chart = AmCharts.makeChart("chartdiv" + key, {
        "type": "gauge",
        "theme": "light",
        "axes": [{
            "axisThickness": 1,
            "axisAlpha": 0.2,
            "tickAlpha": 0.2,
            "tickThickness": 0,
            "valueInterval": 100,
            "bands": [{
                    "color": "#ff0000",
                    "endValue": 33,
                    "startValue": 0
                }, {
                    "color": "#FF9608",
                    "endValue": 60,
                    "startValue": 33
                }, {
                    "color": "#EDFF08",
                    "endValue": 80,
                    "innerRadius": "95%",
                    "startValue": 60
                }, {
                    "color": "#71FF08",
                    "endValue": 100,
                    "innerRadius": "95%",
                    "startValue": 80
                }],

            "bottomTextYOffset": -100,
            "endValue": 100, 
            "bottomText": "0",
        }],
        "arrows": generateChartData(data),
        //"bottomText": "12",
        "export": {
            "enabled": true
        }
    });
}



function generateChartData(data) {
    debugger;

    var chartData = [];


    //$.each(data, function (index, itemData) {

    chartData.push({
        "value": data.WeightedScore
    });
    //chart.validateData();
    //});

    console.log(chartData);
    return chartData;
}


function get_kpi_data_in_time_range(employeeId, link) {
    $.post('KpiDetailByEmployee', { 'employeeId': employeeId }, function (result) {

        var html = '';
        $.each(result, function (key, item) {
            var percentage = parseInt(item.InPercent);
            var arrow = '';
            var textColor = '';
            if (percentage > 0) {
                arrow = '<i class="fa fa-long-arrow-up" style="font-size:24px;color:green"></i>';
                textColor = '<i style="color:green">';
            }
            else if (percentage === 0) {
                arrow = '<i class="fa fa-arrows-v" style="font-size:24px;color:grey"></i>';
                textColor = '<i style="color:grey">';
            } else {
                arrow = '<i class="fa fa-long-arrow-down" style="font-size:24px;color:red"></i>';
                textColor = '<i style="color:red">';
            }
            //else if (percentage < 0 && item.Optimization === "Minimize") {
            //    arrow = '<i class="fa fa-long-arrow-up" style="font-size:24px;color:green"></i>';
            //    textColor = '<i style="color:green">';
            //}
            //else if (percentage > 0 && item.Optimization === "Minimize") {
            //    arrow = '<i class="fa fa-long-arrow-down" style="font-size:24px;color:red"></i>';
            //    textColor = '<i style="color:red">';
            //}
            var lnk = link;
            lnk = lnk.replace('--1', item.AssignId);
            html += '<tr>';
            html += '<td hidden="hidden">' + item.KpiId + '</td>';
            html += '<td>' + item.KpiName + '</td>';
            html += '<td>' + item.Frequency + '</td>';
            html += '<td>' + item.Optimization + '</td>';
            html += '<td>' + item.AssinedBy + '</td>';
            html += '<td>' + item.AssinedTo + '</td>';
            html += '<td>' + item.EmployeeId + '</td>';
            html += '<td>' + item.TotalTarget + '</td>';
            html += '<td>' + item.TotalActual + '</td>';
            html += '<td>' + arrow + textColor + ' ' + item.InPercent + '%</i></td>';
            //html += '<td><i class="fa fa-hourglass-half"></i></td>';
            html += '<td data-sparkline="' + item.Trend + '"></td>';
            html += '<td>' + lnk + '</td>';
            html += '</tr>';
        });
        $('.tbody').html(html);
        Highcharts.SparkLine = function (a, b, c) {
            var hasRenderToArg = typeof a === 'string' || a.nodeName,
                options = arguments[hasRenderToArg ? 1 : 0],
                defaultOptions = {
                    chart: {
                        renderTo: (options.chart && options.chart.renderTo) || this,
                        backgroundColor: null,
                        borderWidth: 0,
                        type: 'area',
                        margin: [2, 0, 2, 0],
                        width: 80,
                        height: 20,
                        style: {
                            overflow: 'visible'
                        },

                        // small optimalization, saves 1-2 ms each sparkline
                        skipClone: true
                    },
                    exporting: { enabled: false },
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false
                    },
                    xAxis: {
                        labels: {
                            enabled: false
                        },
                        title: {
                            text: null
                        },
                        startOnTick: false,
                        endOnTick: false,
                        tickPositions: []
                    },
                    yAxis: {
                        endOnTick: false,
                        startOnTick: false,
                        labels: {
                            enabled: false
                        },
                        title: {
                            text: null
                        },
                        tickPositions: [0]
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        backgroundColor: null,
                        borderWidth: 0,
                        shadow: false,
                        useHTML: true,
                        hideDelay: 0,
                        shared: true,
                        padding: 0,
                        positioner: function (w, h, point) {
                            return { x: point.plotX - w / 2, y: point.plotY - h };
                        }
                    },
                    plotOptions: {
                        series: {
                            animation: false,
                            lineWidth: 1,
                            shadow: false,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            marker: {
                                radius: 1,
                                states: {
                                    hover: {
                                        radius: 2
                                    }
                                }
                            },
                            fillOpacity: 0.25
                        },
                        column: {
                            negativeColor: '#910000',
                            borderColor: 'silver'
                        }
                    }
                };

            options = Highcharts.merge(defaultOptions, options);

            return hasRenderToArg ?
                new Highcharts.Chart(a, options, c) :
                new Highcharts.Chart(options, b);
        };

        var start = +new Date(),
            $tds = $('td[data-sparkline]'),
            fullLen = $tds.length,
            n = 0;

        // Creating 153 sparkline charts is quite fast in modern browsers, but IE8 and mobile
        // can take some seconds, so we split the input into chunks and apply them in timeouts
        // in order avoid locking up the browser process and allow interaction.
        function doChunk() {
            var time = +new Date(),
                i,
                len = $tds.length,
                $td,
                stringdata,
                arr,
                data,
                chart;

            for (i = 0; i < len; i += 1) {
                $td = $($tds[i]);
                stringdata = $td.data('sparkline');
                arr = stringdata.split('; ');
                data = $.map(arr[0].split(', '), parseFloat);
                chart = {};

                if (arr[1]) {
                    chart.type = arr[1];
                }
                $td.highcharts('SparkLine', {
                    series: [{
                        data: data,
                        pointStart: 1
                    }],
                    tooltip: {
                        headerFormat: '<span style="font-size: 10px">' + $td.parent().find('th').html() + ', Q{point.x}:</span><br/>',
                        pointFormat: 'Actual : <b>{point.y}</b>'
                    },
                    chart: chart
                });

                n += 1;

                // If the process takes too much time, run a timeout to allow interaction with the browser
                if (new Date() - time > 500) {
                    $tds.splice(0, i + 1);
                    setTimeout(doChunk, 0);
                    break;
                }

                // Print a feedback on the performance
                if (n === fullLen) {
                    $('#result').html('Generated ' + fullLen + ' sparklines in ' + (new Date() - start) + ' ms');
                }
            }
        }
        doChunk();
    });



}
