﻿using System.Deployment.Internal.Isolation;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WMRP.Helper;
using WMRP.Service.Dto;
using WMRP_DAL;
using WMRP.Service;
using WMRP.Service.IServices;

namespace WMRP.Controllers
{
    public class ProductPartController : Controller
    {
        private readonly IProductPartService _productPartService;
        private readonly IProductModelService _productService;
        private readonly IBasicServices _basicService;
        //
        // GET: /ProductPart/
        public ProductPartController(IProductPartService productPartService, IProductModelService productService, IBasicServices basicService)
        {
            _productPartService = productPartService;
            _productService = productService;
            _basicService = basicService;
        }

        public ActionResult Index()
        {
            return View(_productPartService.GetAllProductParts());
        }

        public ActionResult AddProductPart(long? id)
        {
            return View();
        }

        public ActionResult DeleteProductPart(long id)
        {
            _productPartService.DeleteProductPart(id);

            return RedirectToAction("Index");
        }

        public JsonResult InsertData(List<ProductPartViewModel> viewModels )
        {
            var response = new ResponseMessage();
            response = _productPartService.AddProductPart(viewModels);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getAllPartsByProductId(long pid)
        {
            List<ProductPartViewModel> data = _productPartService.GetAllProductParts();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPartType()
        {
            var enums = _basicService.GetPartTypes();
            return Json(enums, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetBOMByModelId(long id)
        {
            var data = _productPartService.GetAllProductPartsByModel(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllManufacturer()
        {
            var manufacturers = _productPartService.GetAllManufacturers();

            return Json(manufacturers, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetChild(long?id)
        {
            var childs = _productPartService.GetAllChild(id);

            return Json(childs, JsonRequestBehavior.AllowGet);
        }
     

        public ActionResult BomUploader()
        {
            return View();
        }

        [HttpPost]
        public ActionResult BomUploader(HttpPostedFileBase bomFile)
        {
            _productPartService.AddBOMFile(bomFile);
            return RedirectToAction("Index");
        }

        public ActionResult UpdateFromOracle()
        {
            string message;
            message = _productPartService.UpdateBomFromOracle();

            return Content(message);
        }
	}


}