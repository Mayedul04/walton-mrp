﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WMRP.Service;
using WMRP.Models;
using WMRP.Service.Dto;
using WMRP_DAL;
using WMRP.Service.IServices;
using WMRP.Service;
using WMRP.Helper;

namespace WMRP.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductModelService _productService;
        private readonly IBasicServices _basicService;

        public ProductController(IBasicServices basicService, IProductModelService productService)
        {
            _basicService = basicService;
            _productService = productService;
        }
        //
        // GET: /Product/
        public ActionResult Index()
        {            
            return View();
        }

        public JsonResult GetAllProducts()
        {
            List<ProductViewModel> data = _productService.GetAllProducts();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllProductGroups()
        {
            List<ProductGroupViewModel> productGroups = _productService.GetAllProductGroups();
            return Json(productGroups, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllProductModel(long?groupId)
        {
            List<ProductModelViewModel> productModels = _productService.GetModels(groupId);
            return Json(productModels, JsonRequestBehavior.AllowGet);
        }

        
        public JsonResult GetAllProductModelForAutoFill(string prefix,long? productid)
        {
            var result = _productService.GetModelListForAutoFill(prefix, productid); 
            //var data = result.Select(r => new { ID = r.ID, ModelName = r.ModelName }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        
        //public JsonResult GetProductJsonData(DataTablesAjaxRequestModel model)
        //{
        //    var jsonData = new PotentialStudentListModel().GetStudentJsonData(model);

        //    return Json(jsonData, JsonRequestBehavior.AllowGet);

        //}

        public JsonResult GetProductById(long Id)
        {
            var dto = _productService.GetProduct(Id);
            return Json(dto, JsonRequestBehavior.AllowGet);
        }

        //public JsonResult GetAllProductGroups()
        //{
        //    List<ProductGroup> productGroups = _productGroupService.GetProductGroups();
        //    return Json(productGroups, JsonRequestBehavior.AllowGet);
        //}
       
        public ActionResult AddProduct()
        {
            return View();
        }

        [HttpPost]
        public JsonResult InsertData(ProductModelViewModel productViewModel)
        {
            var response = new ResponseMessage();
            response = _productService.AddModel(productViewModel);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteProduct(long id)
        {
            _productService.DeleteProductModel(id);
            return RedirectToAction("Index");
        }

        
	}
}