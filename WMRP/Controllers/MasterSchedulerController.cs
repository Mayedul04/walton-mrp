﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WMRP.Service.IServices;
using WMRP.Service.Dto;



namespace WMRP.Controllers
{
    public class MasterSchedulerController : Controller
    {
        private readonly IManufactureOrderService _manufacturerOrderService;
        //
        // GET: /MasterScheduler/
        public MasterSchedulerController(IManufactureOrderService manufacturerOrderService)
        {
            _manufacturerOrderService = manufacturerOrderService;
        }
        public ActionResult Index()
        {
            return View();
        }

         public JsonResult GetEvents()
         {
             var events = _manufacturerOrderService.GetAllOrders().ToList();
             //var dateList = new List<DateTime>();
             //foreach (var item in events)
             //{
             //    dateList.Add(item.ExpectedStartDate);
             //    dateList.Add(item.ExpectedEndDate);
             //    dateList.Add(item.CalculatedCompletionDate);
             //}
             return new JsonResult { Data = events, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        [HttpPost]
        public JsonResult SaveEvent(ManufactureOrderViewModel manufactureOrderViewModel)
        {
            var response = _manufacturerOrderService.AddMOrder(manufactureOrderViewModel);
            return new JsonResult { Data = new { status = response.Message } };

        }

        

    }
}
