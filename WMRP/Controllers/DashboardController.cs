﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WMRP.Service.Dto;
using WMRP.Service.IServices;

namespace WMRP.Controllers
{
    public class DashboardController : Controller
    {
        private readonly IManufactureOrderService _forecastService;

        public DashboardController(IManufactureOrderService forecastService)
        {
            _forecastService = forecastService;
        }
        //
        // GET: /Dashboard/
        public ActionResult Index()
        {
           return View( _forecastService.GetAllTypeOfStatus());
        }
        
        public JsonResult GetWorkstationCapacity()
        {
            var data = _forecastService.GetWorkstationCapacity();
            
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetModelPartsType()
        {
            
            var data = _forecastService.GetModelWisePartsType();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetModelBomStock()
        {
            var data = _forecastService.GetAllModelBomStock();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        
	}
}