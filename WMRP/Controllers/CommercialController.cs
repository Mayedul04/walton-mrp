﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WMRP.Service.IServices;
using WMRP_DAL;

namespace WMRP.Controllers
{
    public class CommercialController : Controller
    {
        private readonly IProductPartService _productPartService;

        public CommercialController(IProductPartService productPartService)
        {
            _productPartService = productPartService;
        }
        //
        // GET: /Commercial/
        public ActionResult Index(List<ProductPart> productParts)
        {
            return View(_productPartService.GetCommercialReport(productParts));
        }
	}
}