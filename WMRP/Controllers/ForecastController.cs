﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WMRP.Models;
using WMRP.Service.Dto;
using WMRP_DAL;
using WMRP.Service.IServices;
using WMRP.Service;
using WMRP.Helper;
using System.Globalization;


namespace WMRP.Controllers
{
    public class ForecastController : Controller
    {
        //
        // GET: /Forecast/
        private readonly IManufactureOrderService _forecastService;
        private readonly IProductModelService _productService;
        private readonly IBasicServices _basicService;

        public ForecastController(IManufactureOrderService forecastService, IProductModelService productService, IBasicServices basicService)
        {
            _forecastService = forecastService;
            _productService = productService;
            _basicService = basicService;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Add()
        {
            return View();
        }

        public JsonResult getAllOrders(int? status)
        {
            List<ManufactureOrderViewModel> data = _forecastService.GetAllOrders(status);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getManufactureTypes()
        {
            List<ManufactureTypeViewModel> data = _basicService.GetManufactureTypes();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getProductModels(long? prid)
        {
            List<ProductModelViewModel> data = _productService.GetModels(prid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllProcess(long pid, string startdate)
        {
            DateTime FromDate = DateTime.MinValue;
            DateTime.TryParseExact(startdate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            List<MOrderRoutingViewModel> data = _forecastService.GetProductRoutings(pid, FromDate);
            return Json(data, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetProductBOM(long pmid, int qty, string startdate,long? moid)
        {
            DateTime FromDate = DateTime.MinValue;
            DateTime.TryParseExact(startdate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            List<MOrderBOMViewModel> data = _forecastService.GetMorderBOM(pmid, qty, FromDate, moid);
            return Json(data, JsonRequestBehavior.AllowGet);

        }

        public JsonResult InsertData(ManufactureOrderViewModel viewmodel)
        {
            var response = new ResponseMessage();
            response = _forecastService.AddMOrder(viewmodel);
            return Json(response, JsonRequestBehavior.AllowGet);
            
        }

        [HttpPost]
        public JsonResult InsertBasicData(ManufactureOrderViewModel viewmodel)
        {
            var response = new ResponseMessage();
            response = _forecastService.AddMOrder(viewmodel);
            //return Json(response, JsonRequestBehavior.AllowGet);
            return Json(new { Success = response.MessageType, Message = response.Message, ReturnedID = ((MesssageType)response.MessageType == MesssageType.Success ? response.ReturnId : 0)}, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult InsertBOMData(ManufactureOrderViewModel viewmodel)
        {
            var response = new ResponseMessage();
            response = _forecastService.AddMOrder(viewmodel);
            return Json(response, JsonRequestBehavior.AllowGet);
            
        }
        [HttpPost]
        public JsonResult InsertRountingData(ManufactureOrderViewModel viewmodel)
        {
            var response = new ResponseMessage();
            response = _forecastService.AddMOrder(viewmodel);
            return Json(response, JsonRequestBehavior.AllowGet);
            
        }
        [HttpPost]
        public JsonResult InsertDecisionData(ManufactureOrderViewModel viewmodel)
        {
            var response = new ResponseMessage();
            response = _forecastService.AddMOrder(viewmodel);
            return Json(response, JsonRequestBehavior.AllowGet);
            //return Json(new { Success = response.MessageType, Message = response.Message, ReturnedID = ((MesssageType)response.MessageType == MesssageType.Success ? response.ReturnId : 0)}, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDataById(long Id)
        {
            var dto = _forecastService.GetManufacturOrder(Id);
            return Json(dto, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetexpectedProductionDate(List<DateTime> vmodeldata)
        {
            // var dates = vmodeldata.SelectMany(MOrderBOMViewModel.ExpectedProductionStartDate).ToArray();
            return Json(GetMaxValue.GetMaxDate(vmodeldata), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetHolidayList()
        {
            List<DateTime> data = _basicService.HolidayList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
	}
}