﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WMRP.Helper;
using WMRP.IServices;
using WMRP.Service.Dto;

namespace WMRP.Controllers
{
    public class PackageController : Controller
    {
        private readonly IPackageService  _packageService;

        public PackageController(IPackageService packageService)
        {
            _packageService = packageService;
        }
        //
        // GET: /PacakgeController/
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult InsertData(PackageConfigurationViewModel packageConfigurationViewModel)
        {
            var response = new ResponseMessage();
            response = _packageService.AddPackage(packageConfigurationViewModel);

            return Json(response, JsonRequestBehavior.AllowGet);
         }
        public JsonResult GetProductDetailsByPackageId(long id,long? moid)
        {
           if(moid!=null && moid>0)
                return Json(_packageService.GetPackageDetailsByMOId(id, moid), JsonRequestBehavior.AllowGet);
            else
                return Json(_packageService.GetDetailsByPackageId(id), JsonRequestBehavior.AllowGet);
           
        }

        public JsonResult GetAllModelByProduct(long id)
        {
            var data = _packageService.GetProductModelsByProduct(id);

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProducts()
        {
            var products = _packageService.GetAllProducts();

            return Json(products, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeletePackage(long id)
        {
            var response = new ResponseMessage();

            response = _packageService.Delete(id);

            return Json(response, JsonRequestBehavior.AllowGet);

        }

         public JsonResult GetAllPackagesForAutoFill(string prefix)
        {
            var result = _packageService.GetPackageListForAutoFill(prefix);

            var data = result.Select(r => new { ID = r.ID, PackageName = r.PackageName }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

         public JsonResult GetAllPackages()
         {
             var products = _packageService.GetAllPackages();

             return Json(products, JsonRequestBehavior.AllowGet);
         }



        
	}
}