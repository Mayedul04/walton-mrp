﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WMRP.Helper;
using WMRP.Service;
using WMRP.Service.Dto;
using WMRP.Service.IServices;

namespace WMRP.Controllers
{
    public class WorkStationController : Controller
    {
        private readonly IWorkStationService _workStationService;

        public WorkStationController(IWorkStationService workStationService)
        {
            _workStationService = workStationService;
        }
        //
        // GET: /WorkStation/
        public ActionResult Index()
        {
           // return View(_workStationService.GetAllWorkStation());
            return View();
        }

        public ActionResult AddWorkStation(long?id)
        {
            if (id > 0)
            {
                var editableWorkStation = _workStationService.GetWorkStation(id);

                return View(editableWorkStation);
            }
            else
                return View();
        }

        [HttpPost]
        public ActionResult AddWorkStation(WorkStationViewModel workStationViewModel)
        {
            var response = new ResponseMessage();

            response = _workStationService.AddWorkStation(workStationViewModel);

            ViewBag.ResponseMessage = response.Message.ToString();

            return RedirectToAction("index");
        }

        public ActionResult DeleteWorkStation(long id)
        {
            _workStationService.DeleteWorkStation(id);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult AddWorkStationDetails(List<WorkStationDetailViewModel> viewmodels)
        {
            var response = new ResponseMessage();
            response = _workStationService.AddWorkStationDetails(viewmodels);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetModelStationProcessdata(long modelid)
        {
            List<WorkStationDetailViewModel> data = _workStationService.GetAllWorkStationDetails(modelid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
	}
}