﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WMRP_DAL;
using WMRP.Service.IServices;

namespace WMRP.Controllers
{
    public class UserController : Controller
    {
        private readonly IAccountService _accountService;

        public UserController(IAccountService accountService)
        {
            _accountService = accountService;
        }
        //
        // GET: /User/
        [Description("User List")]
        public ActionResult Index()
        {
            var model = _accountService.GetUsers();
            return View(model);
        }
        [Description("NA")]
        public ActionResult Create()
        {

            ViewBag.Role = _accountService.GetRoles();
            var inchargeListItems = new List<SelectListItem> {new SelectListItem {Value = "", Text = "Select Incharge"}};
            var inchargeList = _accountService.GetUsers()
                .Where(i => i.IsActive && i.IsIncharge).ToList();
            if (inchargeList.Any()) {
                inchargeListItems.AddRange(inchargeList.Select(incharge => new SelectListItem {Value = incharge.EmployeeId, Text = incharge.Name}));
            }
            ViewBag.Incharges = inchargeListItems;
            return View();
        }

        [HttpPost]
        public ActionResult Create(User model)
        {
            _accountService.AddUser(model);
            return RedirectToAction("Create");
        }

        [HttpGet]
        [AllowAnonymous]
        [Description("NA")]
        public ActionResult Edit(long id)
        {
            User user = _accountService.GetUser(id);
            ViewBag.Role = _accountService.GetRoles();
            var inchargeListItems = new List<SelectListItem> { new SelectListItem { Value = "", Text = "Select Incharge" } };
            var inchargeList = _accountService.GetUsers()
                .Where(i => i.IsActive && i.IsIncharge).ToList();
            if (inchargeList.Any())
            {
                inchargeListItems.AddRange(inchargeList.Select(incharge => new SelectListItem { Value = incharge.EmployeeId, Text = incharge.Name }));
            }
            ViewBag.Incharges = inchargeListItems;
            return View(user);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(User user)
        {
            return RedirectToAction("Index");
        }

      

        [Description("Role List")]
        public ActionResult Roles()
        {
            var model = _accountService.GetRoles();
            return View(model);
        }

        [Description("NA")]
        public ActionResult CreateRole()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateRole(Role model)
        {
            _accountService.AddRole(model);
            return RedirectToAction("CreateRole");
        }
	}
}