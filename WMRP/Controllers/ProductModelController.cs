﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WMRP.Helper;
using WMRP.Service.Dto;
using WMRP.Service.IServices;

namespace WMRP.Controllers
{
    public class ProductModelController : Controller
    {
        private readonly IProductModelService _productModelService;

        public ProductModelController(IProductModelService productModelService)
        {
            _productModelService = productModelService;
        }
        //
        // GET: /ProductModel/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddProductModel()
        {
            return View();
        }

        public JsonResult GetAllModel()
        {
            List<ProductModelViewModel> models = _productModelService.GetModels(null);

            return Json(models, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetModel(long id)
        {
            var dto = _productModelService.GetModel(id);

            return Json(dto, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult InsertData(ProductModelViewModel viewmodel)
        {
            var response = new ResponseMessage();
            response = _productModelService.AddModel(viewmodel);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllProductGroups()
        {
            List<ProductGroupViewModel> productGroups = _productModelService.GetAllProductGroups();
            return Json(productGroups, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllProductByGroup(long?id)
        {
            List<ProductViewModel> data = _productModelService.GetAllProductByGroup(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteModel(long ? id)
        {
            var response = new ResponseMessage();
            response = _productModelService.DeleteProductModel(id);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        
	}
}