﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WMRP.Models;
using WMRP.Service.Dto;
using WMRP_DAL;
using WMRP.Service.IServices;
using WMRP.Service;
using WMRP.Helper;

namespace WMRP.Controllers
{
    public class ProcessController : Controller
    {
        //
        // GET: /Process/

        private readonly IProductModelService _productService;
        private readonly IProcessService _processService;

        public ProcessController(IProcessService processService, IProductModelService productService)
        {
            _processService = processService;
            _productService = productService;
        }
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetAllProcess(long? modelid)
        {
            List<ModelProcessViewModel> data = _processService.GetAllProcess(modelid);
            return Json(data, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetAllBasicProcess()
        {
            List<ProcessViewModel> data = _processService.GetAllBasicProcess();
            return Json(data, JsonRequestBehavior.AllowGet);

        }

        public ActionResult Add()
        {
              return View();
        }

        public JsonResult GetDataById(long Id)
        {
            var dto = _processService.GetProcess(Id);
            return Json(dto, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult InsertData(List<ModelProcessViewModel> viewmodels)
        {
            var response = new ResponseMessage();
            response = _processService.AddProcess(viewmodels);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(long id)
        {
            _processService.DeleteById(id);
            return RedirectToAction("Index");
        }
    }
}