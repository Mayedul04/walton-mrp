﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMRP.Models
{
    public class PermissionViewModel
    {
        public string Controller { get; set; }
        public List<PermissionActionViewModel> ActionsActionViewModels { get; set; }
    }
}