﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMRP.Models
{
    public class PermissionActionViewModel
    {
        public string ActionName { get; set; }
        public bool IsChecked { get; set; }
    }
}