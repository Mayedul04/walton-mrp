﻿var app = angular.module('WMRP', ['datatables']);
app.controller('ModelCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnBuilder',
    function ($scope, $http, DTOptionsBuilder, DTColumnBuilder) {
        $scope.dtColumns = [
            DTColumnBuilder.newColumn("ID", "ID"),
            DTColumnBuilder.newColumn("ProductName", "Product Name"),
            DTColumnBuilder.newColumn("ModelName", "Model Name")
        ]

        $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
            url: "/ProductModel/GetAllModel",
            type: "POST"
        })
        .withPaginationType('full_numbers')
        .withDisplayLength(10);

    }])