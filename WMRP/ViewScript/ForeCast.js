﻿var app = angular.module("WMRP", ["checklist-model"]);



app.controller("MOrderCtrl", function ($scope, $http, dateFilter, $location, AutoSearch, $q) {
    var baseurl;

    var host = '';
    var port = $location.port();
    var hostInfo = $location.host();
    if (hostInfo === "localhost") {
        host = 'http://' + hostInfo + ':' + port;
    } else {
        host = 'http://' + hostInfo + '/WMRP';
    }
    baseurl = host;
    $scope.lateLoader = 0;

    $scope.ProductVisible = true;
    $scope.ProductModels = {};
    $scope.Holidays = [];
    $scope.selected = { products: [] };
    //$scope.FinalStartDate = new Date();

    $scope.MOrderModels = [{ PackageID: 'Hello', ProductModelId: 'world' }];
    $scope.MOrderModels.pop();

    $scope.ChartData = [{}];
    $scope.ChartData.pop();
    //var vm = this;
    $scope.ExpectedStartDate = dateFilter(new Date(), 'dd/MM/yyyy');
    $scope.ExpectedEndDate = dateFilter(new Date(), 'dd/MM/yyyy');
    $scope.FinalStartDate = new Date(9998, 12, 31);


    //$scope.DateParse = function (inputdate)
    //{
    //   // debugger
    //   dateFilter(inputdate, 'MM/dd/yyyy');
    //};

    ///// Configure Area
    $scope.GetAllInitializations = function () {

        $scope.GetManufactureTypes();
        $scope.GetHolidays();
        var parts = document.location.href.split("/");
        var id = parseInt(parts[parts.length - 1]);
        if (id > 0) {
            $scope.UpdateData(id);
        }

    };
    //$scope.getDate = function () {
    //    this.myDate = new Date();
    //    this.isOpen = false;
    //};
    $scope.GetManufactureTypes = function () {

        $http({
            method: "GET",
            url: baseurl + "/ForeCast/getManufactureTypes"
        }).then(function (response) {
            $scope.ManufactureTypes = response.data;
            console.log(JSON.stringify($scope.ManufactureTypes));
        }, function () {
            alert("Error Occur");
        });
    };



    $scope.GetHolidays = function () {

        $http({
            method: "GET",
            url: baseurl + "/Forecast/GetHolidayList",
            dataType: 'json',
        }).then(function (response) {
            $scope.Holidays = response.data;
            //$.each(response.data, function (key, value) {
            //     $scope.Holidays.push(new Date(parseInt(value.replace('/Date(', '').replace(')/', ''))));
            //});
           // console.log(JSON.stringify($scope.Holidays));
        }, function () {
            alert("Error Occur");
        })
    };

    $scope.GetPackages = function () {

        $http({
            method: "get",
            url: baseurl + "/Package/GetAllPackages"
        }).then(function (response) {
            $scope.Packages = response.data;
        }, function () {
            alert("Error Occur");
        })
    };

    $scope.GetPackageProducts = function () {

        var id = $scope.PackageID;
        if (id > 0) {
            $http({
                method: "get",
                url: baseurl + "/Package/GetProductDetailsByPackageId?id=" + id + '&moid=' + $scope.ID
            }).then(function (response) {
                // debugger
                $scope.PackageProducts = response.data;
            }, function () {
                alert("Error Occur");
            });
        }
        else {
            $('#successModal').modal('show');
            $('#successModalText').text('Please select a Package!');
        }
    };

    $scope.GetAllProduct = function () {

        $http({
            method: "get",
            url: baseurl + "/ProductModel/GetAllProductByGroup?id=2"
        }).then(function (response) {
            $scope.Products = response.data;
        }, function () {
            alert("Error Occur");
        })
    };
    ////// End of Configure Area

    ////On Change Methods
    $scope.SelectModel = function () {
        var modelid = 0;
        //debugger;
        if ($scope.ProductModels[$scope.ModelName] > 0) {
            modelid = $scope.ProductModels[$scope.ModelName];
            $scope.ModelId = modelid;
            $scope.selectedModelID = modelid;
            var el = angular.element('#selectedmodel');
            el.blur();
        }

        if (modelid > 0) {
            //debugger
            $scope.GetBOMdata(modelid);
            //$scope.LoadProductProcess(modelid, $scope.MaxDate)
        }
        //else
        //    alert("No Product Model have been select");
    };

    $scope.GetBOMdata = function (modelid) {
        var deferred = $q.defer();
        //debugger;
        var qty = $scope.ExpectedProductionVolume;
        var sdate = dateFilter($scope.ExpectedStartDate, 'dd/MM/yyyy');
        var productionstartdate;
        if (qty > 0 && sdate != null) {
            $http({
                method: 'GET',
                url: baseurl + "/Forecast/GetProductBOM?pmid=" + modelid + '&qty=' + qty + '&startdate=' + sdate + '&moid=' + $scope.ID
            }).then(function (response) {
                $scope.ProductBOM = response.data;
                deferred.resolve($scope.ProductBOM);
                //$scope.GetMaxDate();$scope.MaxDate
                $scope.LoadProductProcess($scope.selectedModelID, sdate);
                $scope.lateLoader = 1;
            }, function () {
                $scope.lateLoader = 0;
                deferred.reject(arguments);
            });
        }
        return deferred.promise;
    }

    $scope.ProductVisibility = function (ctrl) {

        $scope.ProductVisible = true;
        if (ctrl == 2) {
            $scope.GetAllProduct();
            $scope.ProductVisible = false;
        }
        else
            $scope.GetPackages();

    }

    $scope.checkAllProduct = function () {
        $scope.selected.products = angular.copy($scope.PackageProducts);
    };
    $scope.uncheckAllProduct = function () {
        $scope.selected.products = [];
    };

    $scope.LoadProductProcess = function (id, productionstartdate) {

        $scope.CheckBOMShortage();
        //debugger
        $http({
            method: "get",
            url: baseurl + "/ForeCast/GetAllProcess?pid=" + id + '&startdate=' + productionstartdate + '&moid=' + $scope.ID
        }).then(function (response) {

            //console.log(JSON.stringify(response.data));
            $scope.ProductRoutings = response.data;
            $.each($scope.ProductRoutings, function (key, value) {
                if (value.Selected == true) {
                    $scope.Chartrow = {};
                    $scope.Chartrow.name = value.WorkStationName;
                    $scope.Chartrow.pname = value.ProcessName;
                    $scope.Chartrow.fromDate = dateFilter(new Date(value.ExpectedStartDateText), 'dd/MM/yyyy');
                    $scope.Chartrow.toDate = dateFilter(new Date(value.ExpectedEndDateText), 'dd/MM/yyyy');
                    $scope.Chartrow.color = "#" + ((1 << 24) * Math.random() | 0).toString(16);
                    $scope.ChartData.push($scope.Chartrow);
                    $scope.Chartrow = {};
                }
            });
            loadGanttChart($scope.ChartData);
        }, function () {
            alert("Error Occur");
        })
    };

    var GetNextAvailableDateAfterHoliday = function (datetocheck) {
       // debugger
       // var temp1 = new Date(datetocheck);
        var temp1 = "/Date(" + datetocheck + ")/";
        var availdate = datetocheck;
        var realdate = new Date(datetocheck);
        if ($scope.Holidays.indexOf(temp1) !== -1) {
            availdate = realdate.setDate(realdate.getDate() + 1);
            GetNextAvailableDateAfterHoliday(availdate);
        }
        return availdate;
    }
    var HolidayCount = function (startdate, daycount) {
        var count = 0;
        var jsondate = startdate;
        startdate = new Date();
        for (var i = 1; i < daycount; i++) {
            //debugger
            // var temp = dateFilter(startdate.setDate(startdate.getDate() + 1), 'MM/dd/yyyy');
            var temp = "/Date(" + jsondate.setDate(jsondate.getDate() + 1) + ")/";
            if ($scope.Holidays.indexOf(temp) != -1)
                count++;
        }
        return count;
    }

    /////// End of Change Methods

    ///CRUD
    $scope.InsertData = function () {

        ///ManufactureType==1  Package Mode
        if (document.getElementById("MorderID_").value == 0) {
            if ($scope.ManufactureType == 1) {

                $.each($scope.PackageProducts, function (key, value) {
                    $scope.SelectedModelDto = {};
                    $scope.SelectedModelDto.PackageID = $scope.PackageID;
                    $scope.SelectedModelDto.ProductModelId = value.ProductModelID;

                    var product = value;
                    var selected = false;
                    $.each($scope.selected.products, function (key, value) {
                        if (product == $scope.selected.products[key]) {
                            selected = true;
                            return false;
                        }
                    });
                    $scope.SelectedModelDto.Selected = selected;
                    $scope.MOrderModels.push($scope.SelectedModelDto);
                });
            }
            else {
                $scope.SelectedModelDto = {};
                $scope.SelectedModelDto.ProductModelId = $scope.ModelId;
                $scope.MOrderModels.push($scope.SelectedModelDto);
            }
        }

        $scope.BGColor = "#" + ((1 << 24) * Math.random() | 0).toString(16);
        $scope.MOderDto = {};
        $scope.MOderDto.Title = $scope.Title;
        $scope.MOderDto.BGColor = $scope.BGColor;
        $scope.MOderDto.ID = document.getElementById("MorderID_").value;
        $scope.MOderDto.ManufactureType = $scope.ManufactureType;
        $scope.MOderDto.ExpectedProductionVolume = $scope.ExpectedProductionVolume;
        $scope.MOderDto.ProductId = $scope.ProductId;
        $scope.MOderDto.ExpectedStartDateText = dateFilter($scope.ExpectedStartDate, 'dd/MM/yyyy');
        $scope.MOderDto.ExpectedEndDateText = dateFilter($scope.ExpectedEndDate, 'dd/MM/yyyy');

        $scope.MOderDto.MOrderBOMs = $scope.ProductBOM;
        $scope.MOderDto.MOrderRoutings = $scope.ProductRoutings;
        $scope.MOderDto.MOrderModels = $scope.MOrderModels;

        $scope.MOderDto.CalculatedStartDateText = $scope.FinalStartDate;
        $scope.MOderDto.CalculatedCompletionDateText = $scope.FinalDate;
        $scope.MOderDto.Status = $scope.Status;
        //$scope.ProcessDto.ID = document.getElementById("ProsID_").value;
        $http({
            method: "post",
            url: baseurl + "/ForeCast/InsertData",
            datatype: "json",
            data: JSON.stringify($scope.MOderDto)
        }).then(function (response) {
            document.getElementById("MorderID_").value = response.data.ReturnId;
            $('#successModal').modal('show');
            $('#successModalText').text(response.data.Message);
            debugger
            if ($scope.ManufactureType == 2)
                $scope.GetBOMdata($scope.selectedModelID);
            else {
                $.each($scope.selected.products, function (key, value) {
                    $scope.selectedModelID = value.ProductModelID;
                    $scope.GetBOMdata($scope.selectedModelID);
                });
            }

        })
    }

    $scope.InsertBOM = function () {

        $scope.MOderDto = {};
        $scope.MOderDto.ID = document.getElementById("MorderID_").value;
        $scope.MOderDto.Title = $scope.Title;
        $scope.MOderDto.BGColor = $scope.BGColor;
        $scope.MOderDto.ManufactureType = $scope.ManufactureType;
        $scope.MOderDto.ExpectedProductionVolume = $scope.ExpectedProductionVolume;
        $scope.MOderDto.ProductId = $scope.ProductId;
        $scope.MOderDto.ExpectedStartDateText = dateFilter($scope.ExpectedStartDate, 'dd/MM/yyyy');
        $scope.MOderDto.ExpectedEndDateText = dateFilter($scope.ExpectedEndDate, 'dd/MM/yyyy');
        $scope.MOderDto.CalculatedStartDateText = $scope.FinalStartDate;
        $scope.MOderDto.CalculatedCompletionDateText = $scope.FinalDate;
        $scope.MOderDto.Status = $scope.Status;

        $scope.MOderDto.MOrderBOMs = $scope.ProductBOM;


        $http({
            method: "post",
            url: baseurl + "/ForeCast/InsertData",
            datatype: "json",
            data: JSON.stringify($scope.MOderDto)
        }).then(function (response) {
            
            $('#successModal').modal('show');
            $('#successModalText').text(response.data.Message);


        })
    }

    $scope.InsertRoutings = function () {

        $scope.MOderDto = {};
        $scope.MOderDto.ID = document.getElementById("MorderID_").value;
        $scope.MOderDto.Title = $scope.Title;
        $scope.MOderDto.BGColor = $scope.BGColor;
        $scope.MOderDto.ManufactureType = $scope.ManufactureType;
        $scope.MOderDto.ExpectedProductionVolume = $scope.ExpectedProductionVolume;
        $scope.MOderDto.ProductId = $scope.ProductId;
        $scope.MOderDto.ExpectedStartDateText = dateFilter($scope.ExpectedStartDate, 'dd/MM/yyyy');
        $scope.MOderDto.ExpectedEndDateText = dateFilter($scope.ExpectedEndDate, 'dd/MM/yyyy');
        $scope.MOderDto.CalculatedStartDateText = $scope.FinalStartDate;
        $scope.MOderDto.CalculatedCompletionDateText = $scope.FinalDate;
        $scope.MOderDto.Status = $scope.Status;

        $scope.MOderDto.MOrderRoutings = $scope.ProductRoutings;


        $http({
            method: "post",
            url: baseurl + "/ForeCast/InsertData",
            datatype: "json",
            data: JSON.stringify($scope.MOderDto)
        }).then(function (response) {

            $('#successModal').modal('show');
            $('#successModalText').text(response.data.Message);
        })
    }

    $scope.UpdateData = function (id) {

        $http({
            method: "get",
            url: baseurl + "/ForeCast/GetDataById?Id=" + id
        }).then(function (dto) {
            //console.log(JSON.stringify(dto.data));

            document.getElementById("MorderID_").value = dto.data.ID;
            //debugger
            $scope.ManufactureType = dto.data.ManufactureType;
            $scope.ProductVisibility($scope.ManufactureType);

            $scope.ID = dto.data.ID;
            $scope.Title = dto.data.Title;
            $scope.BGColor = dto.data.BGColor;
            $scope.ExpectedProductionVolume = dto.data.ExpectedProductionVolume;
            $scope.ExpectedStartDate = dateFilter(dto.data.ExpectedStartDateText, 'dd/MM/yyyy');
            $scope.ExpectedEndDate = dateFilter(dto.data.ExpectedEndDateText, 'dd/MM/yyyy');
            $scope.FinalDate = dateFilter(dto.data.CalculatedCompletionDateText, 'dd/MM/yyyy');
            $scope.FinalStartDate = dateFilter(dto.data.CalculatedStartDateText, 'dd/MM/yyyy');
            $scope.Status = dto.data.Status;


            if ($scope.ManufactureType == 2) {
                $scope.ProductId = dto.data.ProductID;
                $.when($scope.searchItem('')).done(function () {
                    $scope.ModelName = dto.data.ProductModelName;
                    $scope.GetBOMdata(dto.data.ProductModelID);
                    $scope.selectedModelID = dto.data.ProductModelID;
                    //$scope.LoadProductProcess(value.ProductModelID, $scope.MaxDate);
                });
            }
            else {
                $scope.PackageID = dto.data.PackageID;
                if ($scope.PackageID > 0) {
                    $scope.GetPackageProducts();
                    $scope.selected.products = dto.data.MOrderModels;
                }
                $.each($scope.selected.products, function (key, value) {
                    if (value.Selected) {
                        $scope.selectedModelID = value.ProductModelID;
                        $scope.GetBOMdata(value.ProductModelID);
                    }
                });
            }

        }, function () {
            alert("Error Occur");
        })
    }

    $scope.GetMaxDate = function () {
        var max = $scope.ExpectedStartDate;
        $.each($scope.ProductBOM, function (key, value) {
            if (dateFilter(value.ExpectedProductionStartDateText, 'dd/MM/yyyy') > max)
                max = dateFilter(value.ExpectedProductionStartDateText, 'dd/MM/yyyy');
        });
        $scope.MaxDate = max;
    }


    $scope.CheckBOMShortage = function () {
        $.each($scope.ProductBOM, function (key, value) {
            if (value.Available == false) {
                $scope.BOMShortage = true;
                //$('#successModal').modal('show');
                //$('#successModalText').text('All BOM Stocks are available. You can proceed from Trial Production');
                return false;
            }
        });
    }

    $scope.SkipProcess = function (processtype) {
        $scope.SkippedProcess = [5, 6, 7, 8, 9, 10, 11];
        if ($scope.BOMShortage == false && $scope.SkippedProcess.indexOf(processtype) !== -1) {
            return true;
        }
        return false;
    }

    $scope.GetAllData = function () {
        var parts = document.location.href.split("/");
        var status = parts[parts.length - 1];
        $http({
            method: "get",
            url: baseurl + "/ForeCast/getAllOrders?status=" + status
        }).then(function (response) {
            //console.log(response.data);
            $scope.ManufactureOrders = response.data;
        }, function () {
            alert("Error Occur");
        })
    };

    $scope.GetNumberofDays = function (index, totalqty) {
        var oldstratdate = new Date($scope.ProductRoutings[index].ExpectedStartDateText);
        var oldenddate = new Date($scope.ProductRoutings[index].ExpectedEndDateText);
        $scope.tempdate = new Date(9998, 12, 31); // Default date to clear the previous date value
        var mod
        var exactchangeoverfrq
        var numberOfDaysToAdd
        var exptime = $scope.ProductRoutings[index].NewModelChangeOvertime;
        if ($scope.ProductRoutings[index].CapacityType == 1)  //capacity type=pcs/hr
        {

            if ($scope.ProductRoutings[index].ProcessType == 17) // 17 == Rework .. 15% of the quantity (Assume)
                exactchangeoverfrq = (totalqty * 0.15) / $scope.ProductRoutings[index].Capacity;
            else
                exactchangeoverfrq = totalqty / $scope.ProductRoutings[index].Capacity;
            if ((totalqty % $scope.ProductRoutings[index].Capacity) > 0.1)
                mod = 1;
            else
                mod = 0;
            $scope.ProductRoutings[index].NoOfChangeOver = parseInt(exactchangeoverfrq) + parseInt(mod);

            if ($scope.ProductRoutings[index].NoOfChangeOver > 0)
                exptime = exptime + ((exptime == 0 ? $scope.ProductRoutings[index].NoOfChangeOver : ($scope.ProductRoutings[index].NoOfChangeOver - 1)) * ($scope.ProductRoutings[index].OldModelChangeOvertime > 0 ? $scope.ProductRoutings[index].OldModelChangeOvertime : 1));
            else
                exptime = exptime + 0;
            if ($scope.ProductRoutings[index].ThresholdTime > 0)
                exptime = exptime + $scope.ProductRoutings[index].ThresholdTime;
            else
                exptime = exptime + 0;
            $scope.ProductRoutings[index].ExpectedFinishingTime = exptime;
            $scope.tempdate = new Date($scope.ProductRoutings[index].ExpectedStartDateText);

            //var exactday = exptime / 8;
            if ((exptime % 8) > 0.1)
                mod = 1;
            else
                mod = 0;
            numberOfDaysToAdd = parseInt(exptime / 8) + parseInt(mod);

        }
        else {
            $scope.tempdate = new Date($scope.ProductRoutings[index].ExpectedStartDateText);
            numberOfDaysToAdd = $scope.ProductRoutings[index].Capacity;
        }
        return numberOfDaysToAdd;
    }

    $scope.CalculateFinishingTime = function (index) {
        var mod;
        var numberOfDaysToAdd = 0;
        var tempstartdate = new Date(9998, 12, 31);
        var tempenddate = new Date(9998, 12, 31);
        var holidaycount = 0;
        var newdatenew = Date(9998, 12, 31);
        $scope.Chartrow = {};
        if ($scope.ProductRoutings[index].Selected == true) {

            if (new Date($scope.ProductRoutings[index].ExpectedStartDateText) < new Date($scope.FinalStartDate)) {
                $scope.FinalStartDate = dateFilter($scope.ProductRoutings[index].ExpectedStartDateText, 'dd/MM/yyyy');
                $.each($scope.ProductBOM, function (key, value) {
                    if ($scope.ProductBOM[key].Available == false) {
                        $scope.ProductBOM[key].ExpectedProductionStartDateText = $scope.FinalStartDate;
                    }
                });
            }


            debugger
            var key = index;
            var similaritycount = 0;
            while (key > 0 && ($scope.ProductRoutings[key].ProcessType == $scope.ProductRoutings[index].ProcessType))  //Skip the first row
            {
                key--;
                similaritycount++;
            }
            var totalexpectedproduction = $scope.ExpectedProductionVolume;
            if (similaritycount > 1) {
                var productiontillnow = 0;
                var allowedperline = 0;
                if ((totalexpectedproduction % similaritycount) > 0.1)
                    mod = 1;
                else
                    mod = 0;
                totalexpectedproduction = parseInt(totalexpectedproduction / similaritycount) + parseInt(mod);
                allowedperline = totalexpectedproduction;
                for (var i = key + 1; i < index; i++) {
                    //$scope.ChartData.splice($scope.ChartData.indexOf($scope.ProductRoutings[i].WorkStationName), 1);
                    $scope.ChartData.pop();
                }
                for (var i = key + 1; i <= index; i++) {
                    //angular.copy($scope.ProductRoutings[i], $scope.Backup);
                    if (totalexpectedproduction > 0) {
                        
                        if ($scope.ProductRoutings[i].Capacity > allowedperline)
                            numberOfDaysToAdd = $scope.GetNumberofDays(i, $scope.ProductRoutings[i].Capacity);
                        else
                            numberOfDaysToAdd = $scope.GetNumberofDays(i, totalexpectedproduction);
                        tempstartdate = ($scope.ProductRoutings[key + 1].ExpectedStartDate.toString().indexOf("Date") == -1 ? new Date($scope.ProductRoutings[key + 1].ExpectedStartDate) : new Date(parseInt($scope.ProductRoutings[key + 1].ExpectedStartDate.replace('/Date(', '').replace(')/', ''))));
                        holidaycount = HolidayCount(tempstartdate, numberOfDaysToAdd);
                        // To do not lost the startdate
                        tempstartdate = ($scope.ProductRoutings[key + 1].ExpectedStartDate.toString().indexOf("Date") == -1 ? new Date($scope.ProductRoutings[key + 1].ExpectedStartDate) : new Date(parseInt($scope.ProductRoutings[key + 1].ExpectedStartDate.replace('/Date(', '').replace(')/', ''))));
                        newdate = tempstartdate.setDate(tempstartdate.getDate() + numberOfDaysToAdd + holidaycount);
                        $scope.ProductRoutings[i].ExpectedStartDateText = $scope.ProductRoutings[key + 1].ExpectedStartDateText;
                        var dayafterholiday = GetNextAvailableDateAfterHoliday(newdate);
                        $scope.ProductRoutings[i].ExpectedEndDateText = dateFilter(dayafterholiday, 'dd/MM/yyyy');
                        $.each($scope.ProductRoutings, function (key, value) {
                            if (key > i) {
                                var expstart = dayafterholiday;
                                $scope.ProductRoutings[key].ExpectedStartDate = expstart;
                                $scope.ProductRoutings[key].ExpectedStartDateText = dateFilter(expstart, 'dd/MM/yyyy');
                            }
                        });

                        $scope.Chartrow.name = $scope.ProductRoutings[i].WorkStationName;
                        $scope.Chartrow.pname = $scope.ProductRoutings[i].ProcessName;
                        $scope.Chartrow.fromDate = dateFilter(($scope.ProductRoutings[key + 1].ExpectedStartDate.toString().indexOf("Date") == -1 ? new Date($scope.ProductRoutings[key + 1].ExpectedStartDate) : new Date(parseInt($scope.ProductRoutings[key + 1].ExpectedStartDate.replace('/Date(', '').replace(')/', '')))), 'yyyy-MM-dd');
                        $scope.Chartrow.toDate = dateFilter(dayafterholiday, 'yyyy-MM-dd');
                        $scope.Chartrow.color = "#" + ((1 << 24) * Math.random() | 0).toString(16);
                        $scope.ChartData.push($scope.Chartrow);
                        $scope.Chartrow = {};
                        $scope.FinalDate = dateFilter(dayafterholiday, 'dd/MM/yyyy');
                        productiontillnow = productiontillnow + ($scope.ProductRoutings[i].Capacity) * ($scope.ProductRoutings[i].NoOfChangeOver)
                        totalexpectedproduction = $scope.ExpectedProductionVolume - productiontillnow;

                    }
                    else {
                        $scope.ProductRoutings[i].Selected = false;
                    }
                }

            }
            else {
                //angular.copy($scope.ProductRoutings[index], $scope.Backup);
                numberOfDaysToAdd = $scope.GetNumberofDays(index, totalexpectedproduction);
               
                tempstartdate = ($scope.ProductRoutings[index].ExpectedStartDate.toString().indexOf("Date") == -1 ? new Date($scope.ProductRoutings[index].ExpectedStartDate) : new Date(parseInt($scope.ProductRoutings[index].ExpectedStartDate.replace('/Date(', '').replace(')/', ''))));
                holidaycount = HolidayCount(tempstartdate, numberOfDaysToAdd);
                tempstartdate = ($scope.ProductRoutings[index].ExpectedStartDate.toString().indexOf("Date") == -1 ? new Date($scope.ProductRoutings[index].ExpectedStartDate) : new Date(parseInt($scope.ProductRoutings[index].ExpectedStartDate.replace('/Date(', '').replace(')/', ''))));
                //var df=tempstartdate.getDate();
                newdate = tempstartdate.setDate(tempstartdate.getDate() + numberOfDaysToAdd + holidaycount);
                //var fgd = dateFilter(newdate, 'dd/MM/yyyy');
                $scope.ProductRoutings[index].ExpectedEndDate = newdate;
                var dayafterholiday=GetNextAvailableDateAfterHoliday(newdate);
                $scope.ProductRoutings[index].ExpectedEndDateText = dateFilter(dayafterholiday, 'dd/MM/yyyy');
                $.each($scope.ProductRoutings, function (key, value) {
                    if (key > index) {
                        var expstart = new Date(dayafterholiday);
                        $scope.ProductRoutings[key].ExpectedStartDate = expstart;
                        $scope.ProductRoutings[key].ExpectedStartDateText = dateFilter(expstart, 'dd/MM/yyyy');
                    }
                });

                $scope.FinalDate = dateFilter(dayafterholiday, 'dd/MM/yyyy');
                $scope.Chartrow.name = $scope.ProductRoutings[index].WorkStationName;
                $scope.Chartrow.pname = $scope.ProductRoutings[index].ProcessName;
                $scope.Chartrow.fromDate = dateFilter(($scope.ProductRoutings[index].ExpectedStartDate.toString().indexOf("Date") == -1 ? new Date($scope.ProductRoutings[index].ExpectedStartDate) : new Date(parseInt($scope.ProductRoutings[index].ExpectedStartDate.replace('/Date(', '').replace(')/', '')))), 'yyyy-MM-dd');
                $scope.Chartrow.toDate = dateFilter(dayafterholiday, 'yyyy-MM-dd');
                $scope.Chartrow.color = "#" + ((1 << 24) * Math.random() | 0).toString(16);
                $scope.ChartData.push($scope.Chartrow);

            }
            loadGanttChart($scope.ChartData);
        }
        else {

            var key = index;
            var similaritycount = 0;
            while (key > 0 && ($scope.ProductRoutings[key].ProcessType == $scope.ProductRoutings[index].ProcessType))  //Skip the first row
            {
                if ($scope.ProductRoutings[key].Selected == true)
                    similaritycount++;
                key--;
            }
            var totalexpectedproduction = $scope.ExpectedProductionVolume;
            if (similaritycount > 0) {
                var productiontillnow = 0;
                if ((totalexpectedproduction % similaritycount) > 0.1)
                    mod = 1;
                else
                    mod = 0;
                totalexpectedproduction = parseInt(totalexpectedproduction / similaritycount) + parseInt(mod);

                for (var i = key + 1; i <= index; i++) {
                    //$scope.ChartData.splice($scope.ChartData.indexOf($scope.ProductRoutings[i].WorkStationName), 1);
                    $scope.ChartData.pop();
                }
                for (var i = key + 1; i <= index; i++) {
                    if (totalexpectedproduction > 0) {
                        if ($scope.ProductRoutings[i].Capacity > allowedperline)
                            numberOfDaysToAdd = $scope.GetNumberofDays(i, $scope.ProductRoutings[i].Capacity);
                        else
                            numberOfDaysToAdd = $scope.GetNumberofDays(i, totalexpectedproduction);
                        tempenddate = ($scope.ProductRoutings[index].ExpectedEndDate.toString().indexOf("Date") == -1 ? new Date($scope.ProductRoutings[key + 1].ExpectedEndDate) : new Date(parseInt($scope.ProductRoutings[key + 1].ExpectedEndDate.replace('/Date(', '').replace(')/', ''))));
                            
                        holidaycount = HolidayCount(tempenddate, numberOfDaysToAdd);
                        tempenddate = ($scope.ProductRoutings[index].ExpectedEndDate.toString().indexOf("Date") == -1 ? new Date($scope.ProductRoutings[key + 1].ExpectedEndDate) : new Date(parseInt($scope.ProductRoutings[key + 1].ExpectedEndDate.replace('/Date(', '').replace(')/', ''))));
                        newdate = tempenddate.setDate(tempenddate.getDate() - numberOfDaysToAdd - holidaycount);
                        $scope.ProductRoutings[i].ExpectedStartDateText = $scope.ProductRoutings[key + 1].ExpectedStartDateText;
                        var dayafterholiday = GetNextAvailableDateAfterHoliday(newdate);
                        $scope.ProductRoutings[i].ExpectedEndDateText = dateFilter(dayafterholiday, 'dd/MM/yyyy');

                        $.each($scope.ProductRoutings, function (key, value) {
                            if (key > i) {

                                $scope.ProductRoutings[key].ExpectedStartDateText = dateFilter(dayafterholiday, 'dd/MM/yyyy');
                            }
                        });

                        //$scope.ChartData.splice($scope.ChartData.indexOf($scope.ProductRoutings[i].WorkStationName), 1);
                        if (i < index) {
                            $scope.Chartrow.name = $scope.ProductRoutings[i].WorkStationName;
                            $scope.Chartrow.pname = $scope.ProductRoutings[i].ProcessName;
                            $scope.Chartrow.fromDate = dateFilter(new Date($scope.ProductRoutings[i].ExpectedStartDateText), 'yyyy-MM-dd');
                            $scope.Chartrow.toDate = dateFilter(new Date($scope.ProductRoutings[i].ExpectedEndDateText), 'yyyy-MM-dd');
                            $scope.Chartrow.color = "#" + ((1 << 24) * Math.random() | 0).toString(16);
                            $scope.ChartData.push($scope.Chartrow);
                        }

                        $scope.FinalDate = dateFilter(dayafterholiday, 'dd/MM/yyyy');
                        productiontillnow = productiontillnow + ($scope.ProductRoutings[i].Capacity) * ($scope.ProductRoutings[i].NoOfChangeOver)
                        totalexpectedproduction = $scope.ExpectedProductionVolume - productiontillnow;
                    }
                    else {
                        $scope.ProductRoutings[i].Selected = false;
                    }
                }
            }
            else {

                numberOfDaysToAdd = $scope.GetNumberofDays(index, totalexpectedproduction);
                tempenddate = ($scope.ProductRoutings[index].ExpectedEndDate.toString().indexOf("Date") == -1 ? new Date($scope.ProductRoutings[index].ExpectedEndDate) : new Date(parseInt($scope.ProductRoutings[index].ExpectedEndDate.replace('/Date(', '').replace(')/', ''))));
                holidaycount = HolidayCount(tempenddate, numberOfDaysToAdd);
                tempenddate = ($scope.ProductRoutings[index].ExpectedEndDate.toString().indexOf("Date") == -1 ? new Date($scope.ProductRoutings[index].ExpectedEndDate) : new Date(parseInt($scope.ProductRoutings[index].ExpectedEndDate.replace('/Date(', '').replace(')/', ''))));
                newdate = tempenddate.setDate(tempenddate.getDate() - numberOfDaysToAdd - holidaycount);
                var dayafterholiday = GetNextAvailableDateAfterHoliday(newdate);
                $scope.ProductRoutings[index].ExpectedEndDateText = dateFilter(dayafterholiday, 'dd/MM/yyyy');
                $.each($scope.ProductRoutings, function (key, value) {
                    if (key > index) {
                        $scope.ProductRoutings[key].ExpectedStartDateText = dateFilter(dayafterholiday, 'dd/MM/yyyy');
                    }
                });
                $scope.ChartData.pop();


                $scope.FinalDate = dateFilter(dayafterholiday, 'dd/MM/yyyy');
            }
            loadGanttChart($scope.ChartData);
        }
    };


    /////////End of CRUD

    ///AUTP Complete Testing
    $scope.searchItem = function (term) {
        //debugger;
        AutoSearch.searchItem(term, $scope.ProductId).then(function (items) {
            $scope.ProductModels = items;

        });
    };
    //////////////

    // Themes begin

});

app.directive('keyboardPoster', function ($parse, $timeout) {
    var DELAY_TIME_BEFORE_POSTING = 0;
    return function (scope, elem, attrs) {

        var element = angular.element(elem)[0];
        var currentTimeout = null;

        element.oninput = function () {
            var model = $parse(attrs.postFunction);
            var poster = model(scope);
            if (currentTimeout) {
                $timeout.cancel(currentTimeout)
            }
            currentTimeout = $timeout(function () {
                poster(angular.element(element).val());
            }, DELAY_TIME_BEFORE_POSTING)
        };
    }
});

app.service('AutoSearch', function ($q, $http, $location) {

    this.searchItem = function (item, productid) {
        var baseurl;
        var host = '';
        var port = $location.port();
        var hostInfo = $location.host();
        if (hostInfo === "localhost") {
            host = 'http://' + hostInfo + ':' + port;
        } else {
            host = 'http://' + hostInfo + '/WMRP';
        }
        baseurl = host;

        var deferred = $q.defer();
        debugger;
        $http({
            method: 'GET',
            url: baseurl + '/Product/GetAllProductModelForAutoFill?prefix=' + item + '&productid=' + productid
        }).then(function (items) {
            var _items = {};
            var items = items.data;

            for (var i = 0, len = items.length; i < len; i++) {
                _items[items[i].ModelName] = items[i].ID;

            }
            console.log(items);
            deferred.resolve(_items);

        }, function () {
            deferred.reject(arguments);
        });
        return deferred.promise;
    };
});

app.directive('datePicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModel) {
            element.datetimepicker({
                minView: 2,
                format: 'dd/mm/yyyy',
                parseInputDate: function (data) {
                    if (data instanceof Date) {
                        return dateFilter(data, 'dd/MM/yyyy');
                    } else {
                        return dateFilter(new Date(), 'dd/MM/yyyy');
                    }
                },
                onSelect: function (date) {
                    scope.date = date;
                    scope.$apply();
                }
            });

            element.on("change", function (e) {
                $(this).datetimepicker('hide');

            });
        }
    };
});


var loadGanttChart = function (data) {
    if (chart) {
        chart.dispose();
    }
    var chart = am4core.create("chartdiv", am4charts.XYChart);
    chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

    chart.paddingRight = 10;
    chart.dateFormatter.inputDateFormat = "dd/MM/yyyy";

    var colorSet = new am4core.ColorSet();
    colorSet.saturation = 0.4;



    chart.data = data;

    var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "name";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.inversed = true;

    var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.dateFormatter.dateFormat = "dd/MM/yyyy";
    dateAxis.renderer.minGridDistance = 15;
    dateAxis.baseInterval = { count: 1, timeUnit: "day" };
    //dateAxis.max = new Date(2018, 0, 1, 24, 0, 0, 0).getTime();
    //dateAxis.strictMinMax = true;
    dateAxis.renderer.tooltipLocation = 0;

    var series1 = chart.series.push(new am4charts.ColumnSeries());
    series1.columns.template.width = am4core.percent(80);
    series1.columns.template.tooltipText = "{pname}: {openDateX} - {dateX}";

    series1.dataFields.openDateX = "fromDate";
    series1.dataFields.dateX = "toDate";
    series1.dataFields.categoryY = "name";
    series1.columns.template.propertyFields.fill = "color"; // get color from data
    series1.columns.template.propertyFields.stroke = "color";
    series1.columns.template.strokeOpacity = 1;

    chart.scrollbarX = new am4core.Scrollbar();
    var cellSize = 50;
    chart.events.on("datavalidated", function (ev) {

        // Get objects of interest
        var chart = ev.target;
        var categoryAxis = chart.yAxes.getIndex(0);

        // Calculate how we need to adjust chart height
        var adjustHeight = chart.data.length * cellSize - categoryAxis.pixelHeight;

        // get current chart height
        var targetHeight = chart.pixelHeight + adjustHeight;

        // Set it on chart's container
        chart.svgContainer.htmlElement.style.height = targetHeight + "px";
    });

}
