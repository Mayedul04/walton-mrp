﻿var app = angular.module("WMRP", []);

app.controller("ProductBOMCtrl", function ($scope, $http, dateFilter, $location, AutoSearch) {
    $scope.ProductParts = [{ Name: 'Hello', Code: 'world' }];
    $scope.ProductParts.pop();
    //var baseurl;
    //var host = 'http://' + $location.host();
    //var port = $location.port();
    //if (port)
    //    baseurl = host + ':' + port;
    //else
    //    baseurl = host;
    
    var baseurl;
    var host = '';
    var port = $location.port();
    var hostInfo = $location.host();
    if (hostInfo === "localhost") {
        host = 'http://' + hostInfo + ':' + port;
    } else {
        host = 'http://' + hostInfo + '/WMRP';
    }
    baseurl = host;
    
    $scope.GetAllLoadedData = function () {
        //debugger;
        var parts = document.location.href.split("/");
        var id = parseInt(parts[parts.length - 1]);
        //if (id > 0) {
        //    $scope.UpdateData(id);
        //}
        //else
        //debugger;
        $scope.LoadPartTypeData();
        $scope.LoadManufacturerData();

    };
    
  
    $scope.AddParts = function () {
        debugger;
        $scope.Parts = {};
        $scope.Parts.Name = $scope.PartName; 
        $scope.Parts.ComponentCode = $scope.PartCode;
        $scope.Parts.PartsType = $scope.PartTypeId;
        $scope.Parts.Dimention = $scope.PartDimension;
        $scope.Parts.Value = $scope.PartValue;
        $scope.Parts.ManufacturerId = $scope.ManufacturerId;
        //$scope.Parts.ModelID = $scope.ModelId;
        $scope.Parts.ProductModelID = $scope.artists[$scope.ModelId];
       
        $scope.ProductParts.push($scope.Parts);
        console.log(JSON.stringify($scope.ProductParts));
        
       // $scope.BOMTable.reload();
        //$scope.Reset();
    };
    $scope.LoadBOMByModel = function () {
        debugger;
        $scope.Id = $scope.artists[$scope.ModelId];

        if ($scope.Id > 0) {
            //$scope.ProductParts = {};
            $http({
                method: "get",
                url: baseurl + "/ProductPart/GetBOMByModelId?id=" + $scope.Id
            }).then(function(response) {
                //console.log(JSON.stringify(response.data));
                //debugger;
                $scope.ProductParts =response.data;
                //$scope.ProductParts.splice(0);
                //for (var key in response.data) {

                //    $scope.Parts = {};
                //    $scope.Parts.ID = response.data[key].ID;
                //    $scope.Parts.Name = response.data[key].Name;
                //    $scope.Parts.ComponentCode = response.data[key].ComponentCode;
                //    $scope.Parts.PartsType = response.data[key].PartsType;
                //    $scope.Parts.Dimention = response.data[key].Dimention;
                //    $scope.Parts.Value = response.data[key].Value;
                //    $scope.Parts.ManufacturerId = response.data[key].ManufacturerId;
                //    $scope.Parts.ModelID = $scope.ModelId;
                //    $scope.Parts.ProductModelID = $scope.artists[$scope.ModelId];

                //    $scope.ProductParts.push($scope.Parts);
                //}
                console.log(JSON.stringify($scope.ProductParts));
            }, function() {
                alert("Error Occur");
            })
        } else {
            $scope.ProductParts.splice(0);
        }
    };

    
    $scope.Reset = function () {
        $scope.PartName = "";
        $scope.PartCode = "";
        $scope.PartTypeId = 0;
        $scope.PartDimension = "";
        $scope.PartValue = "";
        $scope.ManufacturerId = 0;     
    };

    
    $scope.InsertData = function () {
        console.log(JSON.stringify($scope.ProductParts));
        debugger;
        $http({
            method: "post",
            url: baseurl + "/ProductPart/InsertData",
            datatype: "json",
            data: JSON.stringify($scope.ProductParts)
        }).then(function (response) {

            $('#successModal').modal('show');
            $('#successModalText').text(response.data.Message);
            $scope.Reset();

        })

    };

    $scope.LoadPartTypeData = function () {

        $http({
            method: "get",
            url: baseurl + "/ProductPart/GetPartType"
        }).then(function(response) {
            //debugger;
            $scope.Types = response.data;
          }, function() {
            alert("Error Occur");
        })

    };
    
    $scope.LoadManufacturerData = function () {

        $http({
            method: "get",
            url: baseurl + "/ProductPart/GetAllManufacturer"
        }).then(function (response) {
            //debugger;
            $scope.Manufacturers = response.data;
        }, function () {
            alert("Error Occur");
        })

    };

    $scope.LoadParentData = function () {
        debugger;
        
        //$http({
        //    method: "get",
        //    url: baseurl + "/ProductPart/GetParent?id=" + $scope.PartTypeId
        //}).then(function (response) {
        //    $scope.Parents = response.data;
        //}, function () {
        //    alert("Error Occur");
        //})
        
        $scope.ParentArray = {};
        if ($scope.PartTypeId == 1) {
            //$scope.Parents.push({ Name: "SFG" });
            //$scope.ParentArray.Name = "SFG";
            $scope.ParentArray.push("SFG");
            $scope.parents = ParentArray;
        } else {
            //$scope.ParentArray.Name = "NULL";
            //$scope.parents = ParentArray;
            //$scope.Parents.push({ Name: "NULL" });
            $scope.ParentArray.push("NULL");
            $scope.parents = ParentArray;
        }
    };

    
    ///AUTP Complete Testing
    $scope.selectedItems = null;
    $scope.artists = {};
    //$scope.pmodels = {};
    $scope.searchItem = function (term) {

        AutoSearch.searchItem(term).then(function (items) {
            $scope.artists = items;
            //console.log(JSON.stringify(items));
        });
        //console.log(JSON.stringify($scope.ProductModels));
    };

});

app.directive('keyboardPoster', function ($parse, $timeout) {
    var DELAY_TIME_BEFORE_POSTING = 0;
    return function (scope, elem, attrs) {

        var element = angular.element(elem)[0];
        var currentTimeout = null;

        element.oninput = function () {
            var model = $parse(attrs.postFunction);
            var poster = model(scope);
            if (currentTimeout) {
                $timeout.cancel(currentTimeout)
            }
            currentTimeout = $timeout(function () {
                poster(angular.element(element).val());
            }, DELAY_TIME_BEFORE_POSTING)
        };
    }
});

app.service('AutoSearch', function ($q, $http,$location) {

    this.searchItem = function (item) {
        var baseurl;
        var host = '';
        var port = $location.port();
        var hostInfo = $location.host();
        if (hostInfo === "localhost") {
            host = 'http://' + hostInfo + ':' + port;
        } else {
            host = 'http://' + hostInfo + '/WMRP';
        }
        baseurl = host;
        
        var deferred = $q.defer();
        
        $http({
            method: 'GET',
            //url: 'http://localhost:7989/Product/GetAllProductModelForAutoFill?prefix=' + item
            url: baseurl + '/Product/GetAllProductModelForAutoFill?prefix=' + item
        }).then(function (items) {
            var _items = {};
            var items = items.data;

            for (var i = 0, len = items.length; i < len; i++) {
                _items[items[i].ModelName] = items[i].ID;
                //_items[items[i]] = items[i];
                //_items[items[i].name] = items[i].name;
                //console.log(_items[items[i].name]);
            }
            deferred.resolve(_items);

        }, function () {
            deferred.reject(arguments);
        });
        return deferred.promise;
    };
})