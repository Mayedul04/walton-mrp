﻿var app = angular.module("WMRP", []);
app.controller("WorkstationCtrl", function ($scope, $http, $location, AutoSearch, $filter) {
    //debugger;
    //var host = 'http://' + $location.host();
    //var port = $location.port();
    //if (port)
    //    baseurl = host + ':' + port;
    //else
    //    baseurl = host;
    
    var baseurl;
    var host = '';
    var port = $location.port();
    var hostInfo = $location.host();
    if (hostInfo === "localhost") {
        host = 'http://' + hostInfo + ':' + port;
    } else {
        host = 'http://' + hostInfo + '/WMRP';
    }
    baseurl = host;
    
    $scope.ProductModels = {};

    $scope.SaveData = function () {
        $http({
            method: "post",
            url: baseurl + "/WorkStation/AddWorkStationDetails",
            datatype: "json",
            data: JSON.stringify($scope.ModelStationProcesses)
        }).then(function (response) {

            $('#successModal').modal('show');
            $('#successModalText').text(response.data.Message);
        });
    }

    $scope.GetModelSatationProcess = function () {

        if ($scope.ProductModels[$scope.ModelName] > 0) {
            var el = angular.element('#selectedmodel');
            el.blur();
            var modelid = $scope.ProductModels[$scope.ModelName];
            $http({
                method: "get",
                url: baseurl + "/WorkStation/GetModelStationProcessdata?modelid=" + modelid
            }).then(function (response) {
                debugger;
                $scope.ModelStationProcesses = response.data;
            }, function () {
                alert("Error Occur");
            });
        }
    }

    ///AUTO Complete
    $scope.selectedItems = null;

    $scope.searchItem = function (term) {
        AutoSearch.searchItem(term).then(function (items) {
            $scope.ProductModels = items;
            //console.log(JSON.stringify(items));
        });
    };
    //////////////

});
app.directive('keyboardPoster', function ($parse, $timeout) {
    var DELAY_TIME_BEFORE_POSTING = 0;
    return function (scope, elem, attrs) {

        var element = angular.element(elem)[0];
        var currentTimeout = null;


        element.oninput = function () {
            var model = $parse(attrs.postFunction);
            var poster = model(scope);

            if (currentTimeout) {
                $timeout.cancel(currentTimeout)
            }

            currentTimeout = $timeout(function () {
                poster(angular.element(element).val());
            }, DELAY_TIME_BEFORE_POSTING)
        };

    }
});

app.service('AutoSearch', function ($q, $http, $location) {

    this.searchItem = function (item) {
        //var host = 'http://' + $location.host();
        //var port = $location.port();
        //if (port)
        //    baseurl = host + ':' + port;
        //else
        //    baseurl = host;
        
        var baseurl;
        var host = '';
        var port = $location.port();
        var hostInfo = $location.host();
        if (hostInfo === "localhost") {
            host = 'http://' + hostInfo + ':' + port;
        } else {
            host = 'http://' + hostInfo + '/WMRP';
        }
        baseurl = host;

        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: baseurl + '/Product/GetAllProductModelForAutoFill?prefix=' + item
        }).then(function (items) {
            var _items = {};
            var items = items.data;

            for (var i = 0, len = items.length; i < len; i++) {
                _items[items[i].ModelName] = items[i].ID;

            }
            deferred.resolve(_items);

        }, function () {
            deferred.reject(arguments);
        });
        return deferred.promise;
    };
});