﻿var app = angular.module("WMRP", []);


app.controller("ProductCtrl", function ($scope, $http, $location) {
    //var host = 'http://' + $location.host();
    //var port = $location.port();
    //if (port)
    //    baseurl = host + ':' + port;
    //else
    //    baseurl = host;
    
    var baseurl;
    var host = '';
    var port = $location.port();
    var hostInfo = $location.host();
    if (hostInfo === "localhost") {
        host = 'http://' + hostInfo + ':' + port;
    } else {
        host = 'http://' + hostInfo + '/WMRP';
    }
    baseurl = host;

    $scope.GetAllData = function () {
        $http({
            method: "get",
            url: baseurl + "/Product/GetAllProducts"
        }).then(function (response) {
            console.log(response.data);
            $scope.Products = response.data;
        }, function () {
            alert("Error Occur");
        })
    };
    
    $scope.Delete = function () {
        debugger
        var id = document.getElementById("hiddenProductId").value;
        $http({
            method: "post",
            url: baseurl + "/Product/DeleteProduct?Id=" + id,
            datatype: "json"
        }).then(function (response) {
            $('#successModal').modal('show');
            $('#successModalText').text(response.data.Message);
            $scope.GetAllData();
        })
    };
    $scope.SetId = function (id) {
        
        document.getElementById("hiddenProductId").value = id;
    }


    $scope.InsertData = function() {
        debugger
        $scope.ProductDto = {};
        $scope.ProductDto.Name = $scope.ProductName;
        $scope.ProductDto.ModelID = $scope.ProductModelId;
        $scope.ProductDto.GroupId = $scope.ProductGroupId;
        $scope.ProductDto.IsMainProdduct = $scope.IsMainProdduct;
        $scope.ProductDto.LeadTime = $scope.LeadTime;
        $scope.ProductDto.Status = true;
        //$scope.ProductDto.Status.inActive = $scope.Status.inActive;

        $http({
            method: "post",
            url: baseurl + "/Product/InsertData",
            datatype: "json",
            data: JSON.stringify($scope.ProductDto)
        }).then(function(response) {

            $('#successModal').modal('show');
            $('#successModalText').text(response.data.Message);
            $scope.Reset();

        })

    };
    $scope.Reset = function() {
        $scope.ID = 0;
        $scope.ProductName = "";
        $scope.ProductModelId = 0;
        $scope.ProductGroupId = 0;
        $scope.IsMainProdduct = "";
        $scope.LeadTime = "";


    };

    $scope.GetAllLoadedData = function() {

        $scope.LoadProductGroupData();
        $scope.LoadProductModelData();
        
        var products = document.location.href.split("/");
        var id = parseInt(products[products.length - 1]);
        if (id > 0) {
            $scope.UpdateData(id);
        }
    };

    $scope.UpdateData = function(id) {

        $http({
            method: "get",
            url: baseurl + "/Product/GetProductById?Id=" + id
        }).then(function(dto) {
            console.log(JSON.stringify(dto.data));
            document.getElementById("hiddenProductId").value = dto.data.ID;
            
            $scope.ProductName = dto.data.Name;
            $scope.ProductModelId = dto.data.ModelID;
            $scope.ProductGroupId = dto.data.GroupId;
            $scope.IsMainProdduct = dto.data.IsMainProdduct;
            $scope.LeadTime = dto.data.LeadTime;
       
        }, function() {
            alert("Error Occur");
        })
    };

    $scope.LoadProductGroupData = function () {
        debugger;
        $http({
            method: "get",
            url: baseurl + "/Product/GetAllProductGroups"
        }).then(function (response) {
            console.log(JSON.stringify(response.data));
            //console.log(response.data);
            $scope.Groups = response.data;
            //var parts = document.location.href.split("/");
            //var id = parts[parts.length - 1];
            //if (parts.length > 5) {
            //    $scope.UpdateData(id);
            //}

        }, function () {
            alert("Error Occur");
        })
    };
    $scope.LoadProductModelData = function () {
        $http({
            method: "get",
            url: baseurl + "/Product/GetAllProductModel?groupId=" + $scope.ProductGroupId
        }).then(function (response) {
            console.log(JSON.stringify(response.data));
            //console.log(response.data);
            $scope.Models = response.data;
            var parts = document.location.href.split("/");
            //var id = parts[parts.length - 1];
            //if (parts.length > 5) {
            //    $scope.UpdateData(id);
            //}

        }, function () {
            alert("Error Occur");
        })
    };

    $scope.ProductGroups = [];
    $scope.SelecetedProductGroup = null;

    $scope.AfterSelectedProductGroup = function (selected) {
        debugger
        if (selected) {

            $scope.SelecetedProductGroup = selected.originalObject;

            $http({
                method: "get",
                url: baseurl + "/Product/GetAllProductGroups"
            }).then(function(response) {
                console.log(response.data);
                $scope.ProductGroups = response.data;
            }, function() {
                alert("Error Occur");
            })
        }
    };

})