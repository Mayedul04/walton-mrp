﻿var app = angular.module("WMRP", []);
app.controller("ProcessCtrl", function ($scope, $http, $location, AutoSearch, $filter) {
    //debugger;
    var baseurl;
    var host = '';
    var port = $location.port();
    var hostInfo = $location.host();
    if (hostInfo === "localhost") {
        host = 'http://' + hostInfo + ':' + port;
    } else {
        host = 'http://' + hostInfo + '/WMRP';
    }
    baseurl = host;
    
    //if (port)
    //    baseurl = host + ':' + port;
    //else
    //    baseurl = host;
    
    
    $scope.ProductModels = {};
    $scope.ModelProcesses = [{ Process: 'Hello', Model: 'world' }];
    $scope.ModelProcesses.pop();

    $scope.AddNewProcess = function() {
        //debugger
        $scope.IsDuplicate = false;
        if ($scope.ProductModels[$scope.ModelName]) {
            $scope.ProductModelID = $scope.ProductModels[$scope.ModelName];
            $scope.ProcessDto = {};
            $scope.ProcessDto.ProductModelID = $scope.ProductModelID;
            $scope.ProcessDto.ProductModelName = $scope.ModelName;
            $scope.ProcessDto.ProcessID = null;

            $.each($scope.ModelProcesses, function(key, value) {
                if ($scope.ProcessDto.ProcessID == value.ProcessID) {
                    $scope.IsDuplicate = true;
                } else {
                    $scope.IsDuplicate = false;
                }

            });

            if ($scope.IsDuplicate == false) {
                $scope.ModelProcesses.push($scope.ProcessDto);
                $scope.Reset();
            } else {
                $('#successModal').modal('show');
                $('#successModalText').text("This row already exist!");
                $scope.Reset();
            }
        }
    };

    $scope.RemoveProcess = function (item) {
        if (item.ID > 0) {
            document.getElementById("ModelProsID_").value = item.ID;
            //$scope.ModelProcesses.splice($scope.ModelProcesses.indexOf(item), 1);
        }
        else
            $scope.ModelProcesses.splice($scope.ModelProcesses.indexOf(item), 1);
    }

    $scope.ChackDuplicate = function(item) {
        $.each($scope.ModelProcesses, function(key, value) {
            if (key < ($scope.ModelProcesses.length - 1)) {
                if (item.ProcessID == value.ProcessID) {
                    $('#successModal').modal('show');
                    $('#successModalText').text("This process already exist!");
                    item.ProcessID = null;
                }
            }

        });
    };

    $scope.SaveData = function () {
        $http({
            method: "post",
            url: baseurl + "/Process/InsertData",
            datatype: "json",
            data: JSON.stringify($scope.ModelProcesses)
        }).then(function (response) {

            $('#successModal').modal('show');
            $('#successModalText').text(response.data.Message);
        });
    }

    $scope.Reset = function () {

        $scope.ProcessID = 0;
        $scope.ProcessName = "";
        $scope.IsMainProcess = "";
        $scope.Sequence = "";
        $scope.LeadTime = "";
        document.getElementById("ModelProsID_").value = 0;
    };

    $scope.GetAllBasicProcess = function () {
        $http({
            method: "get",
            url: baseurl + "/Process/GetAllBasicProcess"
        }).then(function (response) {

            $scope.processes = response.data;
        }, function () {
            alert("Error Occur");
        });
    }

    $scope.GetAllData = function () {
        if ($scope.ProductModels[$scope.ModelName] > 0) {
            var el = angular.element('#selectedmodel');
            el.blur();
            var modelid = $scope.ProductModels[$scope.ModelName];
            $http({
                method: "get",
                url: baseurl + "/Process/GetAllProcess?modelid=" + modelid
            }).then(function (response) {

                $scope.ModelProcesses = response.data;
            }, function () {
                alert("Error Occur");
            })
        }
    };


    $scope.Delete = function () {
        var id = document.getElementById("ModelProsID_").value;
        if (id > 0) {
            $http({
                method: "post",
                url: baseurl + "/Process/Delete?Id=" + id,
                datatype: "json"
            }).then(function (response) {
                $('#successModal').modal('show');
                $('#successModalText').text(response.data.Message);
                $scope.GetAllData();
            })
        }
    };
    $scope.LoadData = function(index) {
        document.getElementById("ModelProsID_").value = ($scope.ModelProcesses[index].ID ? $scope.ModelProcesses[index].ID : 0);
        $scope.ProcessID = $scope.ModelProcesses[index].ProcessID;
        $scope.ModelName = $scope.ModelProcesses[index].ProductModelName;
        $scope.IsMainProcess = $scope.ModelProcesses[index].IsMainProcess;
        $scope.Sequence = $scope.ModelProcesses[index].Sequence;
        $scope.LeadTime = $scope.ModelProcesses[index].LeadTime;
    };
    $scope.SetId = function (id) {
        debugger
        document.getElementById("ModelProsID_").value = id;
    }

    ///AUTP Complete
    $scope.selectedItems = null;

    $scope.searchItem = function (term) {
        AutoSearch.searchItem(term).then(function (items) {
            $scope.ProductModels = items;
            //console.log(JSON.stringify(items));
        });
    };
   

});
app.directive('keyboardPoster', function ($parse, $timeout) {
    var DELAY_TIME_BEFORE_POSTING = 0;
    return function (scope, elem, attrs) {

        var element = angular.element(elem)[0];
        var currentTimeout = null;

        element.oninput = function () {
            var model = $parse(attrs.postFunction);
            var poster = model(scope);
            if (currentTimeout) {
                $timeout.cancel(currentTimeout)
            }
            currentTimeout = $timeout(function () {
                poster(angular.element(element).val());
            }, DELAY_TIME_BEFORE_POSTING)
        };
    }
});

app.service('AutoSearch', function ($q, $http, $location) {

    this.searchItem = function (item) {
        //var host = 'http://' + $location.host();
        //var port = $location.port();
        //if (port)
        //    baseurl = host + ':' + port;
        //else
        //    baseurl = host;
        
        var baseurl;
        var host = '';
        var port = $location.port();
        var hostInfo = $location.host();
        if (hostInfo === "localhost") {
            host = 'http://' + hostInfo + ':' + port;
        } else {
            host = 'http://' + hostInfo + '/WMRP';
        }
        baseurl = host;
        
        var deferred = $q.defer();
        
        $http({
            method: 'GET',
            url: baseurl + '/Product/GetAllProductModelForAutoFill?prefix=' + item
        }).then(function (items) {
            var _items = {};
            var items = items.data;

            for (var i = 0, len = items.length; i < len; i++) {
                _items[items[i].ModelName] = items[i].ID;
                //_items[items[i]] = items[i];
                //_items[items[i].name] = items[i].name;
                //console.log(_items[items[i].name]);
            }
            deferred.resolve(_items);

        }, function () {
            deferred.reject(arguments);
        });
        return deferred.promise;
    };
});